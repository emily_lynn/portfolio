import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class MinimumStack<Item extends Comparable> implements Iterable<Item> {
    private int totMods = 0;    // total number of pop and push modifications to the stack
    Stack<Item> stack = new Stack();    // first item on the normal stack
    private Node min = null;    // first item on the stack of minimum elements; current element with the minimum value

    // helper linked list class
    private class Node {
        private Item item;
        private Node next;
    }

    public MinimumStack() {}

    // returns the number of items stored
    public int size() { return stack.size(); }

    // returns true iff empty
    public boolean isEmpty() { return stack.isEmpty(); }

    // push item onto stack
    public void push(Item item) {
        // When just starting a stack, min is initialized to the one and only element
        if (stack.size() == 0) {
            min = new Node();
            min.item = item;
        }
        // For already begun stacks, only put item on the minimum stack if it is less than or equal to the current min
        else if ((item.compareTo(min.item) < 0) || (item.compareTo(min.item)==0)) {
            Node oldMin = min;
            min = new Node();
            min.item = item;
            min.next = oldMin;
        }

        stack.push(item);
        totMods++;
    }

    // pop and return the top item
    public Item pop() throws NoSuchElementException {
        Item item = stack.pop();

        // if the item that is popped has the same value as the top of the min stack, remove that one as well
        if (item==min.item) {
            min = min.next;          // previous min is removed but not returned
        }

        totMods++;
        return item;                 // return the saved item
    }

    // returns the minimum item in constant time
    public Item minimum() throws NoSuchElementException {
        if (isEmpty()) throw new NoSuchElementException("An empty stack has no minimum");
        return min.item;
    }

    // returns new Iterator<Item> that iterates over items
    @Override
    public Iterator<Item> iterator() {
        return new Iterator<Item>()  {
            Iterator<Item> itr  = stack.iterator();
            private int numMods = totMods;  // numMods is the # of modifications when iterator is created

            public boolean hasNext () {return itr.hasNext(); }

            public void remove() { itr.remove(); }

            public Item next() throws ConcurrentModificationException { // returns the current item and iterates to the next one
                if ( numMods != totMods ){ throw new ConcurrentModificationException("Cannot call next() in iterator after stack is modified"); }
                return itr.next();
            }
        };
    }

    // Iterates through and prints each element in the stack
    private void printStack() {
        Iterator<Item> itr = iterator();
        StdOut.println("********");
        while(itr.hasNext()) {
            StdOut.println(itr.next()); // prints the current element before iterating to the next
        }
        StdOut.println("********");
        StdOut.println();
    }

    // Prints the current minimum of the stack
    private void printMin(){
        StdOut.println("--- " + minimum() + " ---");
        StdOut.println();
    }

    public static void main(String[] args) {
        //testIterator();
        //testIterator2();
        //test();
    }



    /************* UNIT TESTS **************/

    private static void testIterator(){
        MinimumStack stack = new MinimumStack();
        Iterator<String> itr = stack.iterator();
        stack.push("Hello");
        StdOut.println(itr.next()); // Test if proper exception thrown
    }

    private static void testIterator2(){
        MinimumStack stack = new MinimumStack();
        stack.push("World");
        Iterator<String> itr = stack.iterator();
        stack.pop();
        StdOut.println(itr.next()); // Test if proper exception thrown
    }

    private static void test(){
        MinimumStack stack = new MinimumStack();
        stack.printStack();
        //stack.pop(); // testing if correct exception thrown
        stack.push(4);
        stack.printMin();
        stack.printStack();
        stack.pop();
        stack.printStack();
        //stack.printMin(); // testing if correct exception thrown
        stack.push(3);
        stack.push(5);
        stack.printStack();
        stack.printMin();
        stack.push(3);
        stack.printMin();
        stack.printStack();
        stack.pop();
        stack.printStack();
        stack.printMin();
        stack.pop();
        stack.printMin();
        stack.printStack();
        stack.pop();
        stack.printStack();
        //stack.printMin(); // testing if correct exception is thrown
    }

}
