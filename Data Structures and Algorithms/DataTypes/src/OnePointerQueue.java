import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class OnePointerQueue<Item> implements Iterable<Item> {
    private int qSize = 0;    // number of elements in the queue
    private Node back = null; // back of the queue; back.next is the front of the queue
    private int totMods = 0;  // total number enqueue and dequeue modifications to the queue

    public OnePointerQueue() {}

    private class Node {      //each element in the queue is in a node
        Item item;
        Node next;
    }

    // returns the number of items stored
    public int size() {
        return qSize;
    }

    // returns true iff empty
    public boolean isEmpty() { return qSize == 0; }

    // enqueue item to "back"
    public void enqueue(Item item) {
        Node oldBack = back;
        back = new Node();
        back.item = item;
        if (oldBack == null) { back.next = back; } // A one item queue points back to itself
        else {
            back.next = oldBack.next;              //the back always points up to the front
            oldBack.next = back;
        }
        qSize++;
        totMods++;
    }

    // dequeue item from "front" and returns it
    public Item dequeue() throws NoSuchElementException {
        if (isEmpty()) { throw new NoSuchElementException("Cannot dequeue on an empty queue"); }

        Node oldFront = back.next;
        if (qSize > 1) {
            Node newFront = oldFront.next;
            back.next = newFront;
        }
        else {  // back.next points to itself and back itself needs to be deleted
            back = null;
        }
        qSize--;
        totMods++;
        return oldFront.item;
    }

    // returns new Iterator<Item> that iterates over items in queue
    @Override
    public Iterator<Item> iterator() {
        return new Iterator<Item>()  {
            private Node preCurrent = back;        // preCurrent is the node before the current node; necessary to avoid nullPointerException if queue is empty
            private boolean itrJustStarted = true; // true only for the first hasNext check, to allow preCurrent to equal the back at the start of iteration
            private int numMods = totMods;         // numMods is the # of modifications when iterator is created

            public boolean hasNext() {
                if (preCurrent == null) { return false; } // An empty queue does not have a next
                else if (preCurrent == back && itrJustStarted != true) { return false; } // the iterator has come back around to the back and reached the end of the queue
                return true;
            }

            public void remove() { throw new UnsupportedOperationException(); }

            public Item next() throws ConcurrentModificationException { // returns the current item and iterates to the next one
                if ( numMods != totMods ){ throw new ConcurrentModificationException("Cannot call next() in iterator after steque is modified"); }
                if ( !hasNext() ){ throw new NoSuchElementException("Cannot call next() after having iterated through the whole queue"); }
                itrJustStarted = false;
                Item currentItem = preCurrent.next.item;
                preCurrent = preCurrent.next;
                return currentItem;
            }
        };
    }

    // iterates through and prints each element in a queue
    private void printQ( ) { // necessary to pass queue in order to test modification exception
        Iterator<Item> itr = iterator();
        StdOut.println("********");
        while(itr.hasNext()) {
            Item currentItem = itr.next();
            StdOut.println(currentItem);
        }
        StdOut.println("********");
        StdOut.println();
    }

    // perform unit testing here
    public static void main(String[] args) {
        //testIterator();
        //testIterator2();
        //testEnqueue();
        //testDequeue();
    }


    /*********** UNIT TESTS ************/

    private static void testIterator(){
        OnePointerQueue q = new OnePointerQueue();
        Iterator<Integer> itr = q.iterator();
        q.enqueue(0);
        StdOut.println(itr.next()); // Test if proper exception thrown
    }

    private static void testIterator2(){
        OnePointerQueue q = new OnePointerQueue();
        q.enqueue(0);
        Iterator<Integer> itr = q.iterator();
        q.dequeue();
        StdOut.println(itr.next()); // Test if proper exception thrown
    }

    private static void testEnqueue() {
        OnePointerQueue q = new OnePointerQueue();
        q.printQ();
        q.printQ();
        q.enqueue(1);
        q.printQ();
    }

    private static void testDequeue() {
        OnePointerQueue q = new OnePointerQueue();
        q.printQ(); // Testing if throws proper exception
        q.dequeue(); // Testing if throws proper exception
        q.enqueue(1);
        q.printQ();
        q.dequeue();
        q.printQ();
        q.enqueue(1);
        q.printQ();
        q.enqueue(2);
        q.printQ();
        q.dequeue();
        q.printQ();
        q.dequeue();
        q.printQ();
        q.dequeue(); // Testing if throws proper exception
    }
}
