import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class Steque<Item> implements Iterable<Item> {

    private Node top = null;     // Allowed to push to and pop from the top
    private Node bottom = null;  // Allowed to enqueue at the bottom
    private int size = 0;        // number of items in the steque
    private int totMods = 0;     // total number of push, pop, and enqueue modifications to the steque

    private class Node {         // Each entry in the steque is in a node
        private Item item;
        private Node next;
    }

    public Steque() {}

    // returns the number of items stored
    public int size() {
        return size;
    }

    // returns true iff steque is empty
    public boolean isEmpty() { return size == 0; }

    // enqueues item to bottom of steque
    public void enqueue(Item item) {
        Node oldBottom = bottom;
        bottom = new Node();
        bottom.item = item;
        bottom.next = null;
        if (oldBottom != null) { oldBottom.next = bottom; } // Cannot assign a "next" to a null pointer
        if (top == null) { top = bottom; }                  // With only one entry both top and bottom point to the same node
        size++;
        totMods++;
    }

    // pushes item to top of steque
    public void push(Item item) {
        Node oldTop = top;
        top = new Node();
        top.item = item;
        top.next = oldTop;
        if (bottom == null) { bottom = top; }  // With only one entry both top and bottom point to the same node
        size++;
        totMods++;
    }

    // pops and returns top item
    public Item pop() throws NoSuchElementException {
        if (isEmpty()) { throw new NoSuchElementException("Cannot pop on empty steque"); }
        Item item = top.item;               // save item to return
        top = top.next;                     // delete top node
        if (top == null){ bottom = null; }  // Making sure bottom does not still point to the last item
        size--;
        totMods++;
        return item;                        // return saved item
    }

    // returns new Iterator<Item> that iterates over items in steque
    @Override
    public Iterator<Item> iterator() {
        return new Iterator<Item>()  {
            private Node current = top;
            private int numMods = totMods;  // numMods is the # of modifications when iterator is created

            public boolean hasNext () {return current != null; }

            public void remove() { throw new UnsupportedOperationException(); }

            public Item next() throws ConcurrentModificationException { // returns the current item and iterates to the next one
                if ( numMods != totMods ){ throw new ConcurrentModificationException("Cannot call next() in iterator after steque is modified"); }
                if ( !hasNext() ){ throw new NoSuchElementException("Cannot call next() after having iterated through the whole steque"); }
                Item currentItem = current.item;
                current = current.next;
                return currentItem;
            }
        };
    }

    // Iterates through and prints each element in the steque
    private void printSteque() {
        Iterator<Item> itr = iterator();
        StdOut.println("********");
        while(itr.hasNext()) {
            StdOut.println(itr.next()); // prints the current element before iterating to the next
        }
        StdOut.println("********");
        StdOut.println();
    }

    // perform unit testing here
    public static void main(String[] args) throws NoSuchElementException {
        //testIterator();
        //testIterator2();
        //testIterator3();
        //testPushPop();
        //testEnqueue();
        //testAll();
    }



    /************ UNIT TESTS *************/

    private static void testIterator(){
        Steque steque = new Steque();
        Iterator<Integer> itr = steque.iterator();
        steque.push(1);
        StdOut.println(itr.next()); // Test if proper exception thrown
    }

    private static void testIterator2(){
        Steque steque = new Steque();
        steque.push(0);
        steque.push(1);
        Iterator<Integer> itr = steque.iterator();
        steque.pop();
        StdOut.println(itr.next()); // Test if proper exception thrown
    }

    private static void testIterator3(){
        Steque steque = new Steque();
        steque.push(0);
        steque.push(1);
        Iterator<Integer> itr = steque.iterator();
        steque.enqueue(2);
        StdOut.println(itr.next()); // Test if proper exception thrown
    }

    private static void testPushPop() {
        Steque steque = new Steque();
        steque.printSteque();
        steque.push(0);
        steque.printSteque();
        steque.pop();
        steque.printSteque();
        steque.push(1);
        steque.printSteque();
        steque.push(2);
        steque.printSteque();
        steque.pop();
        steque.printSteque();
        steque.pop();
        steque.printSteque();
        steque.printSteque();
        //steque.pop(); // Testing if proper exception thrown
    }

    private static void testEnqueue() {
        Steque steque = new Steque();
        steque.printSteque();
        steque.enqueue(0);
        steque.printSteque();
        steque.pop();
        steque.printSteque();
        steque.enqueue(1);
        steque.printSteque();
        steque.enqueue(2);
        steque.printSteque();
        steque.pop();
        steque.printSteque();
        steque.pop();
        steque.printSteque();
        //steque.pop(); // testing if proper exception thrown
    }

    private static void testAll() {
        Steque steque = new Steque();
        steque.printSteque();
        steque.enqueue(0);
        steque.printSteque();
        steque.push(1);
        steque.printSteque();
        steque.push(2);
        steque.printSteque();
        steque.enqueue(-1);
        steque.printSteque();
        steque.pop();
        steque.printSteque();
        steque.pop();
        steque.printSteque();
        steque.pop();
        steque.printSteque();
        steque.pop();
        steque.printSteque();
        //steque.pop(); // testing if proper exception thrown
    }
}