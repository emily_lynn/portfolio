import java.math.BigInteger;

public class FindTheOne {
    private static void neo_up_naive() {
        StdRandom.setSeed(42);
        Neo_Up neo = new Neo_Up();
        try {
            int i = 0;
            while(neo.get(i) == 0) i++; // keep incrementing until
                                        // we find the first 1!
            neo.found1(i);
        } catch(Exception e) {
            StdOut.println("Caught exception: " + e);
        }
    }

    private static void neo_up_bad() {
        StdRandom.setSeed(42);
        Neo_Up neo = new Neo_Up();
        try {
            neo.found1(0);  // this is not the correct position!
            neo.found1(0);  // this is will throw an exception!
        } catch(Exception e) {
            StdOut.println("Caught exception: " + e);
        }
    }

    private static void neo_up() {
        StdRandom.setSeed(42);
        Neo_Up neo = new Neo_Up();
        try {
            int size = neo.size();
            int top = size-1;
            int bottom = 0;
            int prev = -1;
            int post = -2;
            /*
            int midBot = -1;
            int midBotVal = -4;
            int midTopVal = -5;
            int midTop = -2;
            int mid = -3; */
            while(true) {
                int midGuess = (top - bottom) / 2;
                int prevPos = bottom + (midGuess - 2);
                int postPos = bottom + (midGuess);
                prev = neo.get(prevPos);
                post = neo.get(postPos);
                if ((prev != post)) {  // 1 is either between or the post
                    int mid = neo.get(bottom + midGuess -1);
                    if (mid == 1) {
                        neo.found1(mid);
                        break;
                    } else {
                        neo.found1(postPos);
                        break;
                    }
                } else {  // both are either 1 or 0
                    if (prev == 0) { // need to check the top half of what we were looking at
                        bottom = postPos + 1;
                    }
                    if (prev == 1) { // need to check the bottom half of what we were looking at
                        top = postPos;
                    }
                }
            }

                /*
                mid = findMid(bottom, top);
                midTop = (findMid(mid, top));
                midTopVal = neo.get(midTop);
                midBot = (findMid(bottom, mid));
                midBotVal = neo.get(midBot);
                if (midTopVal != midBotVal){
                    bottom = midBot;
                    top = midTop;
                }
                else {
                    if ( midBotVal == 0) {
                        bottom = midTop;
                    }
                    else {
                        top = midBot;
                    }
                }*/
            //neo.found1(midBot);
            // do the tests here!
        } catch(Exception e) {
            StdOut.println("Caught exception: " + e);
        }
    }

    /*private static int find1(int bottom, int top){
        int mid = bottom + ((top - bottom)/2);
        if (mid == 1 && top == bottom) {
            return mid;
        }
        find1(bottom, top);
    }*/

    private static void neo_down() {
        StdRandom.setSeed(42);
        Neo_Down neo = new Neo_Down();
        try {
            // do the tests here!
        } catch(Exception e) {
            StdOut.println("Caught exception: " + e);
        }
    }

    private static void neo_left() {
        StdRandom.setSeed(42);
        Neo_Left neo = new Neo_Left();
        try {
            // do the tests here!
        } catch(Exception e) {
            StdOut.println("Caught exception: " + e);
        }
    }

    private static void neo_right() {
        StdRandom.setSeed(42);
        Neo_Right neo = new Neo_Right();
        try {
            // do the tests here!
        } catch(Exception e) {
            StdOut.println("Caught exception: " + e);
        }
    }

    public static void main(String[] args) {
        //neo_up_naive();
        //neo_up_bad();
        neo_up();
        //neo_down();
        //neo_left();
        //neo_right();
    }
}
