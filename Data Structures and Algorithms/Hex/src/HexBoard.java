/****************************************************************************
 *  This class manages an N-by-N hex game board .
 ****************************************************************************/

public class HexBoard {
    private WeightedQuickUnionUF unionMethod;
    //private Stopwatch stopwatch;
    private int[][] board;
    private int unsetTiles = 0;
    private int boardSize = 0;
    private int winner = 0;


    public HexBoard(int N) {
        if ( N < 1 ) {
            throw new java.lang.IllegalArgumentException("Size of board cannot be < or = to 0");
        }
        //stopwatch = new Stopwatch(); // start stopwatch
        boardSize = N;
        unsetTiles = N*N;
        board = new int[N][N];
        unionMethod = new WeightedQuickUnionUF((N*N)+4);  // includes 4 nodes at the end; one for each color side
        for (int i = 0; i < N; i++){
            for (int j = 0; j < N; j++){
                board[i][j] = 0; // 0 indicates an unset tile; all tiles start unset
            }
        }
    }

    private boolean isTouchingTeam(int row, int col, int player) {
        if (outOfBounds(row, col)) {
            throw new java.lang.IndexOutOfBoundsException("isTouchingTeam out of Range");
        }
        if (board[row][col] == player) {
            return true;
        }
        else {
            return false;
        }
    }

    private boolean outOfBounds(int row, int col){
        if ( (row > (boardSize-1)) || (col > (boardSize-1)) || (row < 0) || (col < 0) ) {
            return true;
        }
        else {
            return false;
        }
    }

    public int getPlayer(int row, int col) {
        if (outOfBounds(row, col)) {
            throw new java.lang.IndexOutOfBoundsException("getPlayer out of Range");
        }
        return board[row][col];
    }
    
    public boolean isSet(int row, int col) {
        if (outOfBounds(row, col)) {
            throw new java.lang.IndexOutOfBoundsException("isSet out of Range");
        }
        if (board[row][col] != 0){
            return true;
        }
        else {
            return false;
        }
    }

    public boolean isOnWinningPath(int row, int col) {
        if (outOfBounds(row, col)) {
            throw new java.lang.IndexOutOfBoundsException("isOnWinningPath out of Range");
        }
        if (winner == 1){
            if ( unionMethod.find(col + (boardSize*row)) == unionMethod.find(boardSize*boardSize) ){
                return true;
            }
            else {
                return false;
            }
        }
        else if (winner == 2){
            if ( unionMethod.find(col + (boardSize*row)) == unionMethod.find(boardSize*boardSize + 2) ){
                return true;
            }
            else {
                return false;
            }
        }
        else {
            return false;
        }
    }

    public void setTile(int row, int col, int player) {
        if (outOfBounds(row, col)) {
            throw new java.lang.IndexOutOfBoundsException("setTile out of Range");
        }
        if (isSet(row, col)) {
            throw new java.lang.IllegalArgumentException("Tile has already been set");
        }
        board[row][col] = player;
        unsetTiles = unsetTiles - 1;
        for (int i = (row-1); i < (row+2); i++){
            for (int j = (col-1); j < (col+2); j++){
                if ( !outOfBounds(i, j) ){
                    if ( !(i==(row-1) && (j==(col-1))) && !(i==(row+1) && (j==(col+1))) ) {
                        if (isTouchingTeam(i, j, player)) {
                            unionMethod.union(col + (boardSize * row), j + (boardSize * i));
                        }
                    }
                }
            }
        }
        // make unions to ends if appropriate
        if ( player == 1 ){ // red team is playing
            if ( col==0 ){
                unionMethod.union(col + (boardSize*row), (boardSize*boardSize) );
            }
            if ( col==(boardSize-1) ) {
                unionMethod.union(col + (boardSize*row), (boardSize*boardSize) + 1 );
            }
        }
        else if ( player == 2 ){ // blue team is playing
            if (row==0){
                unionMethod.union(col + (boardSize*row), (boardSize*boardSize) + 2 );
            }
            if (row==(boardSize-1)) {
                unionMethod.union(col + (boardSize*row), (boardSize*boardSize) + 3 );
            }
        }
    }

    public boolean hasPlayer1Won() {
        if (unionMethod.connected( (boardSize*boardSize),(boardSize*boardSize) + 1)) {
            winner = 1;
            //double time = stopwatch.elapsedTime(); // record elapsed time
            //StdOut.println("elapsed time = " + time);
            return true;
        }
        else {
            return false;
        }
    }

    public boolean hasPlayer2Won() {
        if (unionMethod.connected((boardSize*boardSize) + 2, (boardSize*boardSize) + 3)) {
            winner = 2;
            //double time = stopwatch.elapsedTime(); // record elapsed time
            //StdOut.println("elapsed time = " + time);
            return true;
        }
        else {
            return false;
        }
    }

    public int numberOfUnsetTiles() {
        return unsetTiles;
    }
}