/****************************************************************************
 *  Command: HexBoardStats N0 N1 T
 *
 *  This program takes the board sizes N0,N1 and game count T as a command-line
 *  arguments. Then, the program runs T games for each board size N where
 *  N0 <= N <= N1 and where each play randomly chooses an unset tile to set in
 *  order to estimate the probability that player 1 will win.
 ****************************************************************************/

public class HexBoardStats {
    private Stopwatch stopwatch;
    private double[] probArray;
    private int n0;
    private int n1;

    public HexBoardStats(int N0, int N1, int T) {
        stopwatch = new Stopwatch(); // start Stopwatch
        if ( (N0 < 1) || (N1 < N0) || (T < 1) ){
            throw new java.lang.IllegalArgumentException("N0, N1, or T illegal value");
        }
        n0 = N0;
        n1 = N1;
        probArray = new double[N1 + 1];
        for (int i = N0; i < (N1+1); i++) { // i is the size of the board
            double player1Wins = 0.0;
            for (int j = 0; j < T; j++) { // j is the number of times board has been played
                HexBoard board = new HexBoard(i);
                int player = 1;
                while ( (board.hasPlayer1Won() == false) && (board.hasPlayer2Won() == false) ) {
                    int row = StdRandom.uniform(0, i);
                    int col = StdRandom.uniform(0, i);
                    if ( !(board.isSet(row, col)) ) {
                        board.setTile(row, col, player);
                        player = (player % 2) + 1;
                    }
                }
                if (board.hasPlayer1Won()) {
                    player1Wins = player1Wins + 1;
                }
            }
            StdOut.println("player1Wins = " + player1Wins);
            probArray[i] = player1Wins/T;
        }
        StdOut.println("T = " + T);
        for (int N = N0; N < (N1+1); N++) {
            double P1 = getP1WinProbabilityEstimate(N);
            int P1Num = (int) (P1*T);
            double P2 = 1.0 - P1;
            int P2Num = (int) (P2*T);
            StdOut.println("N = " + N + ", P1 = " + P1 + " (" + P1Num + "), P2 = " + P2 + " (" + P2Num + ")" );
        }
        double time = stopwatch.elapsedTime(); // record elapsed time
        StdOut.println("elapsed time = " + time);
    }

    public double getP1WinProbabilityEstimate(int N){
        if ( (N < n0) || (N > n1) ) {
            throw new java.lang.IndexOutOfBoundsException("Out of Range of probability estimate array");
        }
        return probArray[N];
    }


    public static void main(String[] args) {
        int N0 = java.lang.Integer.parseInt(args[0]);
        int N1 = java.lang.Integer.parseInt(args[1]);
        int T = java.lang.Integer.parseInt(args[2]);
        HexBoardStats hexStats = new HexBoardStats(N0, N1, T);
    }
}
