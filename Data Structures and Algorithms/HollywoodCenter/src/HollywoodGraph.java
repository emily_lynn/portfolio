import java.util.NoSuchElementException;

/**
 * HollywoodGraph loads movie and actor data from a file, generates
 * a graph out of it, and then allows questions to be asked about
 * the data, such as:
 * - How many connected components exist?
 * - How many actors are connected to a particular actor?
 * - What is the average distance between all actors connected
 *   to a particular actor?
 * - What is the length of the shortest path between two actors?
 */
public class HollywoodGraph {
    //SymbolGraph symbolGraph;
    ActorSymbolGraph actorSymbolGraph; // Contains all of the actors connected to
    MyCCBFS myCCBFS;
    BreadthFirstPaths bfpInfo;
    Stack connectedComp;
    MyHollywoodActor myHollyActorArray[];
    //ST innerActorConnectST = new ST<String, String>();
    //ST actorConnectionST = new ST<String, ST<String, String>>();
    // first actor is actor A, which is connected to another symbol table where the first string is Actor B and the second string is the movie that connects them

    private class ActorSymbolGraph {
        private ST<String, Integer> st;  // string -> index
        private String[] keys;           // index  -> string
        private Graph G;

        private ActorSymbolGraph(String filename, String delimiter) {
            st = new ST<String, Integer>(); // values are the indices of the Graph G, the keys are the actors names

            // First pass builds the index by reading strings to associate
            // distinct strings with an index
            In in = new In(filename);
            // while (in.hasNextLine()) {
            while (!in.isEmpty()) {
                String[] a = in.readLine().split(delimiter);
                for (int i = 1; i < a.length; i++) { // changed starting index from 0 to 1
                    if (!st.contains(a[i])) {
                        int index = st.size();
                        st.put(a[i], index);
                        //actorConnectionST.put(a[i] , index);
                    }
                }
            }
            StdOut.println("Done reading " + filename);
            myHollyActorArray = new MyHollywoodActor[st.size()];

            // inverted index to get string keys in an array
            keys = new String[st.size()];
            for (String name : st.keys()) {
                keys[st.get(name)] = name;
            }

            // second pass builds the graph by connecting first actor vertex on each
            // line to all others
            G = new Graph(st.size());
            in = new In(filename);
            while (in.hasNextLine()) {
                String[] a = in.readLine().split(delimiter);
                for (int i = 1; i < a.length; i++) {
                    int v = st.get(a[i]);  // changed index of v from a[0] to a[i]
                    if (myHollyActorArray[v] == null) {
                        myHollyActorArray[v] = new MyHollywoodActor(a[i]);
                    }
                    myHollyActorArray[st.get(a[i])].movies.push(a[0]);
                    for (int j = i+1; j < a.length; j++) {
                        int w = st.get(a[j]);
                        G.addEdge(v, w);
                        //hollyActorArray[st.get(a[i])]

                        // add the actor and connecting movie to the actor's symbol table
                        /*
                        if (actorConnectionST.contains(v) == false) { // checks to see if the actor is already in the "actor database"; if not, add it
                            actorConnectionST.put(v, new ST<String, String>());
                        }
                        actorConnectionST.put(v, );
                         */
                    }
                }
            }
        }

        /**
         * Does the graph contain the vertex named <tt>s</tt>?
         * @param s the name of a vertex
         * @return <tt>true</tt> if <tt>s</tt> is the name of a vertex, and <tt>false</tt> otherwise
         */
        public boolean contains(String s) {
            return st.contains(s);
        }

        /**
         * Returns the integer associated with the vertex named <tt>s</tt>.
         * @param s the name of a vertex
         * @return the integer (between 0 and <em>V</em> - 1) associated with the vertex named <tt>s</tt>
         */
        public int index(String s) {
            return st.get(s);
        }

        /**
         * Returns the name of the vertex associated with the integer <tt>v</tt>.
         * @param v the integer corresponding to a vertex (between 0 and <em>V</em> - 1)
         * @return the name of the vertex associated with the integer <tt>v</tt>
         */
        public String name(int v) {
            return keys[v];
        }

        /**
         * Returns the graph assoicated with the symbol graph. It is the client's responsibility
         * not to mutate the graph.
         * @return the graph associated with the symbol graph
         */
        public Graph G() {
            return G;
        }
    }

    private class MyCCBFS {
        private boolean[] marked;   // marked[v] = has vertex v been marked?
        private int[] id;           // id[v] = id of connected component containing v
        private int[] size;         // size[id] = number of vertices in given component
        private int count;          // number of connected components
        private int currentId = 0;
        int[] idList;

        /**
         * Computes the connected components of the undirected graph {@code G}.
         *
         * @param G the undirected graph
         */
        private MyCCBFS(Graph G) {
            marked = new boolean[G.V()];
            id = new int[G.V()];
            size = new int[G.V()];
            idList = new int[G.V()];
            for (int v = 0; v < G.V(); v++) {
                if (!marked[v]) {
                    currentId = v;
                    idList[count] = (v);
                    bfs(G, v);
                    count++;
                }
            }
        }

        // breadth-first search for a Graph
        private void bfs(Graph G, int v) {
            Queue<Integer> q = new Queue<>();
            q.enqueue(v);
            marked[v] = true;
            while (!q.isEmpty()) {
                v = q.dequeue();
                id[v] = currentId;
                size[currentId]++;
                for (int w : G.adj(v)) {
                    if (!marked[w]) {
                        q.enqueue(w);
                        marked[w] = true;
                    }
                }
            }
        }


        /**
         * Returns the component id of the connected component containing vertex {@code v}.
         *
         * @param v the vertex
         * @return the component id of the connected component containing vertex {@code v}
         * @throws IllegalArgumentException unless {@code 0 <= v < V}
         */
        public int id(int v) {
            validateVertex(v);
            return id[v];
        }

        /**
         * Returns the number of vertices in the connected component containing vertex {@code v}.
         *
         * @param v the vertex
         * @return the number of vertices in the connected component containing vertex {@code v}
         * @throws IllegalArgumentException unless {@code 0 <= v < V}
         */
        public int size(int v) {
            validateVertex(v);
            return size[id[v]];
        }

        /**
         * Returns the number of connected components in the graph {@code G}.
         *
         * @return the number of connected components in the graph {@code G}
         */
        public int count() {
            return count;
        }

        /**
         * Returns true if vertices {@code v} and {@code w} are in the same
         * connected component.
         *
         * @param v one vertex
         * @param w the other vertex
         * @return {@code true} if vertices {@code v} and {@code w} are in the same
         * connected component; {@code false} otherwise
         * @throws IllegalArgumentException unless {@code 0 <= v < V}
         * @throws IllegalArgumentException unless {@code 0 <= w < V}
         */
        public boolean connected(int v, int w) {
            validateVertex(v);
            validateVertex(w);
            return id(v) == id(w);
        }

        // throw an IllegalArgumentException unless {@code 0 <= v < V}
        private void validateVertex(int v) {
            int V = marked.length;
            if (v < 0 || v >= V)
                throw new IllegalArgumentException("vertex " + v + " is not between 0 and " + (V - 1));
        }
    }

    private class MyHollywoodActor implements HollywoodActor {
        private String name;
        Stack movies = new Stack();
        double avgDist;
        double maxDist;
        String maxActor;
        boolean doneBFS = false;
        BreadthFirstPaths bfpInfor;

        public MyHollywoodActor (String name) {
            this.name = name;
        }
        public void performBFS () {
            bfpInfo = new BreadthFirstPaths(actorSymbolGraph.G(), actorSymbolGraph.index(this.name));
            double sumDists = 0;
            double dist = 0;
            double maxDist = dist;
            String maxActor = this.name;
            for (int vertex = 0; vertex < actorSymbolGraph.G().V(); vertex++) {
                if (myCCBFS.connected(actorSymbolGraph.index(this.name), vertex)) {
                    dist = bfpInfo.distTo(vertex);
                    if (dist > maxDist) {
                        maxDist = dist;
                        maxActor = actorSymbolGraph.name(vertex);
                    }
                    sumDists = sumDists + dist;
                }
            }
            avgDist = sumDists/myCCBFS.size(actorSymbolGraph.index(this.name));
            this.maxDist = maxDist;
            this.maxActor = maxActor;
            doneBFS = true;
        }
        public String name() {
            return name;
        }
        public Iterable<String> movies(){ // const time
            return movies;
        }
        public double distanceAverage(){ //const time
            return avgDist;
        }
        public double distanceMaximum() {
            return maxDist;
        }
        public String actorMaximum(){ // const time
            return maxActor;
        }

        public Iterable<String> actorPath(String name){ // not constant (linear to length of path)
            if (name == null || !actorSymbolGraph.contains(name)){
                throw new NoSuchElementException("Actor's name is null or does not exist in data file.");
            }
            Queue pathQueue = new Queue();
            if (bfpInfo.hasPathTo(actorSymbolGraph.index(name)) == true) {
                for (int vertex : bfpInfo.pathTo(actorSymbolGraph.index(name))) {
                    pathQueue.enqueue(actorSymbolGraph.name(vertex));
                }
                return pathQueue;
            }
            else {return null;}
        }
        public double actorPathLength(String name) {
            if (name == null || !actorSymbolGraph.contains(name)){
                throw new NoSuchElementException("Actor's name is null or does not exist in data file.");
            }
            if (bfpInfo.hasPathTo(actorSymbolGraph.index(name) )) {
                return bfpInfo.distTo(actorSymbolGraph.index(name));
            }
            else {return Double.POSITIVE_INFINITY;}
        }

        public Iterable<String> moviePath(String name) { // not const
            if (name == null || !actorSymbolGraph.contains(name)){
                throw new NoSuchElementException("Actor's name is null or does not exist in data file.");
            }
            return null;
        }
    }

    public HollywoodGraph(String filename) {
        actorSymbolGraph = new ActorSymbolGraph(filename, "/");
        myCCBFS = new MyCCBFS(actorSymbolGraph.G());
        connectedComp = new Stack();
    }

    public HollywoodActor getActorDetails(String name) {
        if (name == null || !actorSymbolGraph.contains(name)){
            throw new NoSuchElementException("Actor's name is null or does not exist in data file.");
        }
        MyHollywoodActor actor = myHollyActorArray[actorSymbolGraph.index(name)];
        actor.performBFS();
        return actor;
    }

    public Iterable<String> connectedComponents() {
        Stack repActors = new Stack();
        for (int i = 0; i < connectedComponentsCount(); i++){
            String actorName = actorSymbolGraph.name(myCCBFS.idList[i]);
            repActors.push(actorName);
        }
        return repActors;
    }

    public int connectedComponentsCount() {
        return myCCBFS.count();
    }

    public int connectedActorsCount(String name) { // ?????????????????????????????????????????????? directly or indirectly (in connected component)?
        if (name == null || !actorSymbolGraph.contains(name)){
            throw new NoSuchElementException("Actor's name is null or does not exist in data file.");
        }
        return actorSymbolGraph.G().degree(actorSymbolGraph.index(name));
    }

    public double hollywoodNumber(String name) {
        if (name == null || !actorSymbolGraph.contains(name)){
            throw new NoSuchElementException("Actor's name is null or does not exist in data file.");
        }
        return getActorDetails(name).distanceAverage();
    }

    static public void main(String[] args) {
        /* put code here to answer readme questions */
        HollywoodGraph hg = new HollywoodGraph("movies.txt");
        /*
        StdOut.println(hg.hollywoodNumber("Bacon, Kevin"));
        StdOut.println(hg.getActorDetails("Bacon, Kevin").distanceMaximum());
        StdOut.println(hg.getActorDetails("Bacon, Kevin").actorPath("Lloyd, Christopher (I)"));
        StdOut.println(hg.hollywoodNumber("Ford, Harrison (I)"));
        StdOut.println(hg.getActorDetails("Ford, Harrison (I)").distanceMaximum());
        StdOut.println(hg.getActorDetails("Ford, Harrison (I)").actorPath("Lloyd, Christopher (I)"));
        StdOut.println(hg.hollywoodNumber("Fisher, Carrie"));
        StdOut.println(hg.getActorDetails("Fisher, Carrie").distanceMaximum());
        StdOut.println(hg.getActorDetails("Fisher, Carrie").actorPath("Lloyd, Christopher (I)"));
        StdOut.println(hg.hollywoodNumber("Hamill, Mark (I)"));
        StdOut.println(hg.getActorDetails("Hamill, Mark (I)").distanceMaximum());
        StdOut.println(hg.getActorDetails("Hamill, Mark (I)").actorPath("Lloyd, Christopher (I)"));

         */
        StdOut.println(hg.hollywoodNumber("Sarandon, Susan"));

        /*
        int totalCount = 0;
        int numRuns = 5;
        for (int run = 0; run < numRuns; run++) {
            int counter = 0;
            Stopwatch stopwatch = new Stopwatch();
            while (stopwatch.elapsedTime() < 60) {
                int v = StdRandom.uniform(0, hg.actorSymbolGraph.G().V());
                hg.getActorDetails(hg.actorSymbolGraph.name(v));
                counter++;
            }
            StdOut.println(counter);
            totalCount = totalCount + counter;
        }
        StdOut.println();
        StdOut.println(totalCount/numRuns);

         */

    }
}

// Create a Graph first, and then use it to fill a 2D array where the indexes are mapped to different actors and the
// elements are the names of movies that connect the two actors that are the indices
// Or can make an array of actors, where each is linked to a symbol table where the key is the name of an actor and the value is the name of the connecting movie
// create another data file that you can compute by hand in order to check your work
