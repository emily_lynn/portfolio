import java.util.Arrays;

public class BinarySearch {
    public static int rank(int key, int[] a) { // Array must be sorted
        // returns the index position of key in a if it exists
        // else, returns -1
        int lo = 0;
        int hi = a.length - 1;
        while( lo <= hi ) { // Key is in a[lo..hi] or not present
            int mid = lo + (hi - lo) / 2;
            if      (key < a[mid]) hi = mid - 1;
            else if (key > a[mid]) lo = mid + 1;
            else    return mid;
        }
        return -1;
    }

    public static void main(String[] args) {
        // read ints from args[0] and store in whitelist
        // read ints from args[1] and store in checklist
        // print each int that is in checklist but is not in whitelist
        In in = new In(args[0]);
        In in2 = new In(args[1]);
        int[] whitelist = in.readAllInts();
        int[] checklist = in2.readAllInts();
        Arrays.sort(whitelist);

        for (int i = 0; i < checklist.length; i++) { // Reads through checklist, prints ints if not in whitelist
            int key = checklist[i];
            if (rank(key, whitelist) == -1)
                StdOut.println(key);
        }
    }
}
