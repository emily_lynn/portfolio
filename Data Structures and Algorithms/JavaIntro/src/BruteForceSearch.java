import java.util.Arrays;

public class BruteForceSearch {
    public static int rank(int key, int[] a) {
        // returns the index position of key in a if it exists
        // else, returns -1
        for (int i = 0; i < a.length; i++)
            if (a[i] == key) return i;
        return -1;
    }

    public static void main(String[] args) {
        // read ints from args[0] and store in whitelist
        // read ints from args[1] and store in checklist
        // print each int that is in checklist but is not in whitelist
        In in = new In(args[0]);
        In in2 = new In(args[1]);
        int[] whitelist = in.readAllInts();
        int[] checklist = in2.readAllInts();
        Arrays.sort(whitelist);

        for (int i = 0; i < checklist.length; i++) { // Reads through checklist, prints ints if not in whitelist
            int key = checklist[i];
            if (rank(key, whitelist) == -1)
                StdOut.println(key);
        }
    }
}
