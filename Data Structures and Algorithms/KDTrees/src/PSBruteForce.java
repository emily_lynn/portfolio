import java.util.Iterator;

/**
 * PSBruteForce is a Point collection that provides brute force
 * nearest neighbor searching using red-black tree.
 */
public class PSBruteForce<Value> implements PointSearch<Value> {
    RedBlackBST<Point, Value> rbTree;
    double minX = Double.MAX_VALUE;               // minimum X value of all points, initially set to be the maximum possible value
    double minY = Double.MAX_VALUE;               // minimum Y value of all points, initially set to be the maximum possible value
    double maxX = -Double.MAX_VALUE;              // maximum X value of all points, initially set to be the minimum possible value (maximum possible negative value)
    double maxY = -Double.MAX_VALUE;              // maximum Y value of all points, initially set to be the minimum  possible value (maximum possible negative value)


    // constructor makes empty collection
    public PSBruteForce() { rbTree = new RedBlackBST<>(); }

    // add the given Point to KDTree
    public void put(Point p, Value v) {
        if (p == null) {throw new java.lang.NullPointerException("Point argument is null"); }
        rbTree.put(p, v);
        if (p.x() > maxX) { maxX = p.x(); }
        if (p.y() > maxY) { maxY = p.y(); }
        if (p.x() < minX) { minX = p.x(); }
        if (p.y() < minY) { minY = p.y(); }
    }

    public Value get(Point p) {
        if (p == null) {throw new java.lang.NullPointerException("Point argument is null"); }
        if (rbTree.isEmpty()) { return null; }
        return rbTree.get(p);
    }

    public boolean contains(Point p) {
        if (p == null) {throw new java.lang.NullPointerException("Point argument is null"); }
        if (rbTree.isEmpty()) {return false;}
        return rbTree.contains(p);
    }

    // return an iterable of all points in collection
    public Iterable<Point> points() {
        return rbTree.keys();
    }

    // return the Point that is closest to the given Point
    public Point nearest(Point p) {
        if (p == null) {throw new java.lang.NullPointerException("Point argument is null"); }
        if (rbTree.isEmpty()) {return null;}
        int k = 1;
        MaxPQ<PointDist> pointDists= new MaxPQ();
        for (Point point : rbTree.keys()) {
            double pointDist = point.dist(p);
            pointDists.insert( new PointDist(point, pointDist) );
            if (pointDists.size() > k) {
                pointDists.delMax();
            }
        }
        return pointDists.max().p();
    } // There should be a way to do this without using a priority queue...

    // return the Value associated to the Point that is closest to the given Point
    public Value getNearest(Point p) {
        if (p == null) {throw new java.lang.NullPointerException("Point argument is null"); }
        if (rbTree.isEmpty()) {return null;}
        return this.get(nearest(p));
    }

    // return the min and max for all Points in collection.
    // The min-max pair will form a bounding box for all Points.
    // if KDTree is empty, return null.
    public Point min() {
        if (rbTree.isEmpty()) {return null;}
        return new Point(minX, minY);
    }
    public Point max() {
        if (rbTree.isEmpty()) {return null;}
        return new Point(maxX, maxY);
    }

    // return the k nearest Points to the given Point
    public Iterable<Point> nearest(Point p, int k) {
        if (p == null) {throw new java.lang.NullPointerException("Point argument is null"); }
        if (rbTree.isEmpty()) {return null;} // There are no neighbors
        if (rbTree.size() < k) { // We don't want to store more than we need, or try get too many max's from the MaxPQ
            k = rbTree.size();
        }
        MaxPQ<PointDist> pointDists = new MaxPQ();
        for (Point point : rbTree.keys()) {
            double pointDist = point.dist(p);
            pointDists.insert( new PointDist(point, pointDist) );
            if (pointDists.size() > k) {
                pointDists.delMax();
            }
        }
        Stack<Point> nearPoints = new Stack();
        for (int i = 0; i < k; i++ ) {
            nearPoints.push( (pointDists.delMax()).p() );
        }
        return nearPoints;
    }

    public Iterable<Partition> partitions() { return null; }

    // return the number of Points in KDTree
    public int size() { return rbTree.size(); }

    // return whether the KDTree is empty
    public boolean isEmpty() { return rbTree.isEmpty(); }

    // place your timing code or unit testing here
    public static void main(String[] args) {
        In in = new In(args[0]);
        Point[] pointArray = new Point[Integer.parseInt(args[1])];
        PSBruteForce bruteForce = new PSBruteForce();
        int i = 0;
        while (in.hasNextLine()) {
            Point point = new Point(in.readDouble(), in.readDouble());
            bruteForce.put(point, i);
            pointArray[i] = point;
            i++;
        }
        int totalCount = 0;
        int numRuns = 10;
        for (int run = 0; run < numRuns; run++) {
            int counter = 0;
            Stopwatch stopwatch = new Stopwatch();
            while (stopwatch.elapsedTime() < 1) {
                Point uniformPt = Point.uniform();
                bruteForce.nearest(uniformPt);
                counter++;
            }
            StdOut.println(counter);
            totalCount = totalCount + counter;
        }
        StdOut.println();
        StdOut.println(totalCount/numRuns);
    }
}
