import java.util.Iterator;

/**
 * PSKDTree is a Point collection that provides nearest neighbor searching using
 * 2d tree
 */
public class PSKDTree<Value> implements PointSearch<Value> {
    Stack<Point> points = new Stack<>();          // Iterable of the points in the tree : update each time put inserts to a null
    Stack<Partition> partitions = new Stack<>();  // Iterable of the partitions in the tree
    private Node root;                            // root of the K-d tree
    double minX = Double.MAX_VALUE;               // minimum X value of all points, initially set to be the maximum possible value
    double minY = Double.MAX_VALUE;               // minimum Y value of all points, initially set to be the maximum possible value
    double maxX = -Double.MAX_VALUE;              // maximum X value of all points, initially set to be the minimum possible value (maximum possible negative value)
    double maxY = -Double.MAX_VALUE;              // maximum Y value of all points, initially set to be the minimum  possible value (maximum possible negative value)


    private class Node {
        Point p;
        Value v;
        Node left, right;
        Partition.Direction dir; // What the NEXT layer of nodes will be divided by
        //Partition partition; //

        public Node(Point p, Value val, Partition.Direction dir) {
            if (p == null) {throw new java.lang.NullPointerException("Point argument is null"); }
            this.p = p;
            this.v = val;
            this.dir = dir;
        }
    }

    // constructor makes empty kD-tree
    public PSKDTree() {}

    private int xCompare(Point p1, Point p2) {
        if (p1.x() < p2.x()){
            return -1;
        }
        else if (p1.x() > p2.x()) {
            return 1;
        }
        else {return 0;}
    }

    private int yCompare(Point p1, Point p2) {
        if (p1.y() < p2.y()){
            return -1;
        }
        else if (p1.y() > p2.y()) {
            return 1;
        }
        else {return 0;}
    }

    // add the given Point to kD-tree
    public void put(Point p, Value v) {
        if (p == null) {throw new java.lang.NullPointerException("Point argument is null"); }
        root = put(root, null, p, v, Partition.Direction.LEFTRIGHT);
    }

    private Node put(Node x, Node prevNode, Point p, Value v, Partition.Direction d){ // d is the direction of the node x
        if (x == null) {  // we've reached the bottom of the tree where we are going to put the next node
            points.push(p);

            // Update the min and max values
            if (p.x() < minX) { minX = p.x(); }
            if (p.y() < minY) { minY = p.y(); }
            if (p.x() > maxX) { maxX = p.x(); }
            if (p.y() > maxY) { maxY = p.y(); }

            //partitioning
            if (prevNode == null) { // we are adding the first node, and there is no partition
                partitions.push(new Partition(new Point(p.x(), minY), new Point(p.x(), maxY), d));
            }
            else { // This is not the first point
                if (d == Partition.Direction.LEFTRIGHT) {
                    partitions.push(new Partition(p, new Point(p.x(), prevNode.p.y()), d));
                }
                else {
                    partitions.push(new Partition(p, new Point(prevNode.p.x(), p.y()), d));
                }
            }
            return new Node(p, v, d);
        }

        if (x.p == p) {  // The point already exists inside the tree, so we just need to update its value
            x.v = v;
        }
        else {
            int cmp;
            if (x.dir == Partition.Direction.LEFTRIGHT) {
                cmp = xCompare(p, x.p);
            } else {
                cmp = yCompare(p, x.p);
            }

            if (cmp <= 0) x.left = put(x.left, x, p, v, Partition.nextDirection(d)); // make sure that this convention of <= is in the other relevant functions
            else if (cmp > 0) x.right = put(x.right, x, p, v, Partition.nextDirection(d));
        }
        return x;
    }

    // value associated with the given key; null if no such key
    public Value get(Point p) {
        if (p == null) {throw new java.lang.NullPointerException("Point argument is null"); }
        if (isEmpty()) {return null;}
        return get(root, p);
    }

    // value associated with the given key in subtree rooted at x; null if no such key
    private Value get(Node x, Point p) {
        while (x != null) {
            if (p == x.p) {
                return x.v;
            }
            else {
                int cmp;
                if (x.dir == Partition.Direction.LEFTRIGHT) {
                    cmp = xCompare(p, x.p);
                } else {
                    cmp = yCompare(p, x.p);
                }

                if (cmp <= 0) {
                    x = x.left;
                }
                else if (cmp > 0) {
                    x = x.right;
                }
                else {
                    StdOut.println("Something is wrong in get (1)");
                }
            }
        }
        return null;
    }

    // is there a key-value pair with the given key?
    public boolean contains(Point p) {
        if (p == null) {throw new java.lang.NullPointerException("Point argument is null"); }
        if (isEmpty()) {return false;}
        return get(p) != null;
    }

    public Value getNearest(Point p) {
        if (p == null) {throw new java.lang.NullPointerException("Point argument is null"); }
        if (isEmpty()) {return null;}
        return get(nearest(p));
    }

    // return an iterable of all points in collection
    public Iterable<Point> points() { return points; }


    // return an iterable of all partitions that make up the kD-tree
    public Iterable<Partition> partitions() { return partitions; }

    // return the Point that is closest to the given Point
    public Point nearest(Point p) {
        if (p == null) {throw new java.lang.NullPointerException("Point argument is null"); }
        if (isEmpty()) {return null;}

        Iterable<Point> points = nearest(p, 1);
        Point nearestPt = null;
        for (Point point : points) {
            nearestPt = point;
        }
        return nearestPt;
    }

    // return the k nearest Points to the given Point
    public Iterable<Point> nearest(Point p, int k) {
        if (p == null) {throw new java.lang.NullPointerException("Point argument is null"); }
        if (isEmpty()) {return null;} // There are no neighbors
        if (k<1) {return null;} //
        if (size() < k) { // We don't want to store more than we need, or try get too many max's from the MaxPQ
            k = size();
        }
        MaxPQ<PointDist> pointDists = new MaxPQ();
        nearest(root, p, k, pointDists);


        Stack<Point> nearPoints = new Stack();
        for (int i = 0; i < k; i++ ) {
            nearPoints.push( (pointDists.delMax()).p() );
        }
        return nearPoints;
    }

    private void nearest(Node subroot, Point p, int k, MaxPQ<PointDist> pointDists) {
        if (subroot == null) {
            return;
        }

        int cmp;
        if (subroot.dir == Partition.Direction.LEFTRIGHT) {
            cmp = xCompare(p, subroot.p);
        } else {
            cmp = yCompare(p, subroot.p);
        }

        if (cmp <= 0) {
            nearest(subroot.left, p, k, pointDists);
        }
        else if (cmp > 0) {
            nearest(subroot.right, p, k, pointDists);
        }

        pointDists.insert( new PointDist(subroot.p, subroot.p.dist(p)) );
        if (pointDists.size() > k) {
            pointDists.delMax();
        }

        // When it comes back, we need to test whether we should go down the other branch or not
        double rootHypDist;
        if (subroot.dir == Partition.Direction.LEFTRIGHT) {
            rootHypDist = java.lang.Math.abs(subroot.p.x() - p.x());
        } else {
            rootHypDist = java.lang.Math.abs(subroot.p.y() - p.y());
        }

        double farthestNearestDist = pointDists.max().d();
        if ((rootHypDist < farthestNearestDist) || (pointDists.size() < k)) {
            if (cmp <= 0) {
                nearest(subroot.right, p, k, pointDists);
            }
            else if (cmp > 0) {
                nearest(subroot.left, p, k, pointDists);
            }
        }
    }

    // return the min and max for all Points in collection.
    // The min-max pair will form a bounding box for all Points.
    // if kD-tree is empty, return null.
    public Point min() {
        if (this.isEmpty()) { return null; }
        return new Point(minX, minY);
    }

    public Point max() {
        if (this.isEmpty()) { return null; }
        return new Point(maxX, maxY);
    }

    // return the number of Points in kD-tree
    public int size() { return points.size(); }

    // return whether the kD-tree is empty
    public boolean isEmpty() { return root == null; }

    // place your timing code or unit testing here
    public static void main(String[] args) {
        In in = new In(args[0]);
        Point[] pointArray = new Point[Integer.parseInt(args[1])];
        PSKDTree kdTree = new PSKDTree();
        int i = 0;
        while (in.hasNextLine()) {
            Point point = new Point(in.readDouble(), in.readDouble());
            kdTree.put(point, i);
            pointArray[i] = point;
            i++;
        }
        int totalCount = 0;
        int numRuns = 10;
        for (int run = 0; run < numRuns; run++) {
            int counter = 0;
            Stopwatch stopwatch = new Stopwatch();
            while (stopwatch.elapsedTime() < 1) {
                Point uniformPt = Point.uniform();
                kdTree.nearest(uniformPt);
                counter++;
            }
            StdOut.println(counter);
            totalCount = totalCount + counter;
        }
        StdOut.println();
        StdOut.println(totalCount/numRuns);
    }

}
