import java.lang.IndexOutOfBoundsException;
import java.lang.IllegalArgumentException;

/**
 * Pathfinder uses A* search to find a near optimal path
 * between to locations with given terrain.
 */

public class Pathfinder {
    private PFNode startNode = new PFNode(null, null);
    private PFNode endNode = new PFNode(null, null);
    Terrain terrain;
    private float heuristic = 0;
    boolean pathFound = false;
    float pathCost = 0;
    int searchSize = 0;
    MinPQ<PFNode> minPQ = new MinPQ();
    boolean[][] searchedArray;
    Stack<Coord> pathSolution = new Stack<>();

    /**
     * PFNode will be the key for MinPQ (used in computePath())
     */
    private class PFNode implements Comparable<PFNode> {
        Coord loc;                 // the location of the PFNode
        boolean useStatus = false; // Is true if the node has been used
        boolean validity = true;   // Is true if the node is valid
        PFNode prev;               // points to the previous PFNode in the path (towards the start)
        private float aStarCost;   // The cost to get to this node from the start, including the heuristic cost from the previous node (A* algorithm cost)
        private float dijCost;     // The cost to get to this node from the start, not including the heuristic cost from the previous node (Dijkstra's algorithm cost)


        // fromNode: how did we get here? (linked list back to start)
        public PFNode(Coord loc, PFNode fromNode) {
            this.loc = loc;
            this.prev = fromNode;
            float prevCost = 0;
            if (!(prev == null)) {
                prevCost = prev.getDijCost(); // non-heuristic accumulated cost to the previous node
                float additionalCost = terrain.computeTravelCost(this.loc, prev.loc);
                float heuristicCost = heuristic*terrain.computeDistance(this.loc, endNode.loc); // penalizes moving farther away from the end location
                this.dijCost = prevCost + additionalCost;
                this.aStarCost = prevCost + additionalCost + heuristicCost;
            }
            else { // Start node or end node
                this.aStarCost = 0;
                this.dijCost = 0;}
        }

        // compares this with that, used to find minimum cost PFNode
        // Compares based upon the A* algorithm cost, which takes into account the heuristic
        public int compareTo(PFNode that) {
            float thisCost = this.aStarCost;
            float thatCost = that.aStarCost;
            if ( thisCost < thatCost ) { return -1; }
            else if ( thisCost > thatCost ) { return 1; }
            else { return 0; } // the two costs are equal
        }

        public Coord getLoc() { return loc; }

        public void setAStarCost(float cost) {
            this.aStarCost = cost;
        }

        public void setDijCost(float cost) {
            this.dijCost = cost;
        }


        // returns the accumulated cost to travel from the start to this, without heuristic
        // via the fromNode chain
        public float getDijCost() { // Is it okay that I removed the heuristic parameter?
            return dijCost;
        }

        // returns if this PFNode is not marked invalid
        public boolean isValid() {
            return validity;
        }

        // marks the PFNode as invalid
        public void invalidate() { validity = false;}

        // returns if the PFNode is marked as used
        public boolean isUsed() {
            return useStatus;
        }

        // marks the PFNode as used
        public void use() { useStatus = true; }

        // returns an Iterable of PFNodes that surround this
        public Iterable<PFNode> neighbors() {
            int bound = terrain.getN()-1;
            Stack<PFNode> s = new Stack<>();
            int i = this.getLoc().getI();
            int j = this.getLoc().getJ();
            Coord above = new Coord(i-1, j);
            if (above.isInBounds(0,0,bound,bound) ) {
                s.push(new PFNode(above, this));
            }
            Coord right = new Coord(i, j+1);
            if (right.isInBounds(0,0,bound,bound) ) {
                s.push(new PFNode(right, this));
            }
            Coord below = new Coord(i+1, j);
            if (below.isInBounds(0,0,bound,bound) ) {
                s.push(new PFNode(below, this));
            }
            Coord left = new Coord(i, j-1);
            if (left.isInBounds(0,0,bound,bound) ) {
                s.push(new PFNode(left, this));
            }
            return s;
        }
    }

    public Pathfinder(Terrain terrain) {
        this.terrain = terrain;
        searchedArray = new boolean[terrain.getN()][terrain.getN()];
    }

    public void setPathStart(Coord loc) {  // What visually counts as in or out of bounds?
        if (loc == null) { throw new IllegalArgumentException("cannot set a null location as the path start"); }
        if (!loc.isInBounds(0,0, terrain.getN()-1 ,terrain.getN()-1)) {
            throw new IndexOutOfBoundsException("Start location is out of bounds");
        }
        startNode.loc = loc;
        startNode.prev = null;
    }

    public Coord getPathStart() { return startNode.loc; }

    public void setPathEnd(Coord loc) {
        if (loc == null) { throw new IllegalArgumentException("cannot set a null location as the path end"); }
        if (!loc.isInBounds(0,0, terrain.getN()-1 ,terrain.getN()-1)) {
            throw new IndexOutOfBoundsException("End location is out of bounds");
        }
        endNode.loc = loc;
        endNode.prev = null;
    }

    public Coord getPathEnd() { return endNode.loc; }

    public void setHeuristic(float h) { heuristic = h; }

    public float getHeuristic() { return heuristic; }

    public void resetPath() {
        pathSolution = new Stack<>();
        endNode.prev = null;
        searchSize = 0;
        pathCost = 0;
        pathFound = false;
        searchedArray = new boolean[terrain.getN()][terrain.getN()];
        minPQ = new MinPQ();
    }

    public void computePath() {
        if (startNode.loc == null || endNode.loc == null) {throw new IllegalArgumentException("Path start and end must be set before a path can be found between them"); }
        minPQ.insert(startNode);
        while (pathFound == false) {
            PFNode midNode = minPQ.delMin(); // delMin removes and returns the smallest key on the priority queue.
            if (searchedArray[midNode.getLoc().getI()][midNode.getLoc().getJ()] == true) {// we've already searched here
                continue;
            }
            searchedArray[midNode.getLoc().getI()][midNode.getLoc().getJ()] = true;
            searchSize++;
            for(PFNode node: midNode.neighbors()) {
                if ((node.loc).equals(endNode.loc)) { // we reached out goal location!
                    pathFound = true;
                    endNode = node;
                    pathCost = node.getDijCost();
                    break;
                }
                minPQ.insert(node);
            }
        }
        pathSolution = new Stack<>();
        PFNode currentNode = endNode;
        pathSolution.push(currentNode.loc);
        while(currentNode.loc != startNode.loc) {
            pathSolution.push(currentNode.prev.loc);
            currentNode = currentNode.prev;
        }
    }

    public boolean foundPath() { return pathFound; }

    public float getPathCost() { return pathCost; }

    public int getSearchSize() { return searchSize; } // should this only be for the number of unique spots that are searched??

    public Iterable<Coord> getPathSolution() {
        return pathSolution;
    }
    // Move this stuff so that getPathSolution only returns the iterable;

    public boolean wasSearched(Coord loc) {
        return searchedArray[loc.getI()][loc.getJ()];
    }  // used for changing color from white to purple
}
