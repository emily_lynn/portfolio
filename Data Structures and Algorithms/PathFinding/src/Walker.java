import java.util.Iterator;

/**
 * Walker takes an Iterable of Coords and simulates an individual
 * walking along the path over the given Terrain
 */
public class Walker {
    Terrain terrain;
    LinkedQueue<Coord> pathQueue = new LinkedQueue();
    Coord currentLoc;
    float travelCost;
    float credit = 0; // the amount of "energy" the walker has to move to the next node in the path (must be >= travelCost to advance)
    int tries = 0;    // number of times that advance is called (with or without actually taking a step)
    int steps = 0;    // number of times the current location is changed


    // terrain: the Terrain the Walker traverses
    // path: the sequence of Coords the Walker follows
    public Walker(Terrain terrain, Iterable<Coord> path) {
        this.terrain = terrain;
        for (Coord c : path) {
            pathQueue.enqueue(c);
        }
        currentLoc = pathQueue.dequeue();
    }

    // returns the Walker's current location
    public Coord getLocation() {
            return currentLoc;
    }

    // returns true if Walker has reached the end Coord (last in path)
    public boolean doneWalking() {
        if (pathQueue.size() == 0) {
            StdOut.println("tries: " + tries + " ;  steps: " + steps);
            return true;
        }
        else {return false;}
    }

    // advances the Walker along path
    // byTime: how long the Walker should traverse (may be any non-negative value)
    public void advance(float byTime) {
        if (pathQueue.size() != 0) { // There is still something to peek at to find the travelcost: we've not reached the end
            travelCost = terrain.computeTravelCost(currentLoc, pathQueue.peek());
            credit = credit + byTime;
            tries++;
            if (credit >= travelCost) {
                credit = credit - travelCost;
                currentLoc = pathQueue.dequeue();
                steps++;
            }
        }
    }

}
