This class was taken Fall 2019.

COS 265 4 hours
Data Structures and Algorithms
A survey of data structures and algorithms that operate on them, with an emphasis on
abstract data types and analysis of computational complexity.