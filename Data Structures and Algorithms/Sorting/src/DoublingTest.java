public class DoublingTest {
    /***********************************************************************
     *  main() function
     *  Place all of your unit tests here
     *  Hint: created additional functions to help organize your tests
     ***********************************************************************/

    private static Double[] shuffledArray (int N) {
        Double[] a = new Double[N];
        for (int i = 0; i < N; i++) {
            a[i] = StdRandom.uniform();
        }
        StdRandom.shuffle(a);

        return a;
    }

    private static void printDoublingInfoSelection (int numRepeats, int startN, int avgNum) {
        int N = startN;
        int count = 0; // index of info in different arrays
        int[] NArray = new int[numRepeats];
        double[] timeArray = new double[numRepeats];
        double timeToSort = 0;
        double avgTime = 0;

        while (count < numRepeats) {
            // Average results:
            double sumTime = 0;
            for (int i = 0; i < avgNum; i++) {
                Double[] a = shuffledArray(N);
                Stopwatch stopwatch = new Stopwatch(); // start Stopwatch
                Selection.sort(a);
                timeToSort = stopwatch.elapsedTime(); // record elapsed time

                sumTime = sumTime + timeToSort;
            }
            avgTime = sumTime/avgNum;

            // fill arrays with relevant information
            NArray[count] = N;
            timeArray[count] = avgTime;

            N = N*2;
            count++;
        }

        StdOut.println("Selection Sort Doubling Information");
        for (int i = 0; i < numRepeats; i++) {
            if (i == 0) {  // Don't do ratios
                StdOut.println("N : " + NArray[i] + ";         T(N) :" + timeArray[i]);
            }
            else {
                StdOut.println("N : " + NArray[i] + ";         T(N) :" + timeArray[i] + ";                          Ratio : " + timeArray[i]/timeArray[i-1] + ";         lg Ratio :" + Math.log(timeArray[i]/timeArray[i-1])/Math.log(2));
            }
        }
    }

    private static void printDoublingInfoInsertion (int numRepeats, int startN, int avgNum) {
        int N = startN;
        int count = 0; // index of info in different arrays
        int[] NArray = new int[numRepeats];
        double[] timeArray = new double[numRepeats];
        double timeToSort = 0;
        double avgTime = 0;

        while (count < numRepeats) {
            // Average results:
            double sumTime = 0;
            for (int i = 0; i < avgNum; i++) {
                Double[] a = shuffledArray(N);
                Stopwatch stopwatch = new Stopwatch(); // start Stopwatch
                Selection.sort(a);
                timeToSort = stopwatch.elapsedTime(); // record elapsed time

                sumTime = sumTime + timeToSort;
            }
            avgTime = sumTime/avgNum;

            // fill arrays with relevant information
            NArray[count] = N;
            timeArray[count] = avgTime;

            N = N*2;
            count++;
        }

        StdOut.println("Insertion Sort Doubling Information");
        for (int i = 0; i < numRepeats; i++) {
            if (i == 0) {  // Don't do ratios
                StdOut.println("N : " + NArray[i] + ";         T(N) :" + timeArray[i]);
            }
            else {
                StdOut.println("N : " + NArray[i] + ";         T(N) :" + timeArray[i] + ";                          Ratio : " + timeArray[i]/timeArray[i-1] + ";         lg Ratio :" + Math.log(timeArray[i]/timeArray[i-1])/Math.log(2));
            }
        }
    }


    private static void printDoublingInfoShell (int numRepeats, int startN, int avgNum) {
        int N = startN;
        int count = 0; // index of info in different arrays
        int[] NArray = new int[numRepeats];
        double[] timeArray = new double[numRepeats];
        double timeToSort = 0;
        double avgTime = 0;

        while (count < numRepeats) {
            // Average results:
            double sumTime = 0;
            for (int i = 0; i < avgNum; i++) {
                Double[] a = shuffledArray(N);
                Stopwatch stopwatch = new Stopwatch(); // start Stopwatch
                Shell.sort(a);
                timeToSort = stopwatch.elapsedTime(); // record elapsed time

                sumTime = sumTime + timeToSort;
            }
            avgTime = sumTime/avgNum;

            // fill arrays with relevant information
            NArray[count] = N;
            timeArray[count] = avgTime;

            N = N*2;
            count++;
        }

        StdOut.println("Shell Sort Doubling Information");
        for (int i = 0; i < numRepeats; i++) {
            if (i == 0) {  // Don't do ratios
                StdOut.println("N : " + NArray[i] + ";         T(N) :" + timeArray[i]);
            }
            else {
                StdOut.println("N : " + NArray[i] + ";         T(N) :" + timeArray[i] + ";                          Ratio : " + timeArray[i]/timeArray[i-1] + ";         lg Ratio :" + Math.log(timeArray[i]/timeArray[i-1])/Math.log(2));
            }
        }
    }

    private static void printDoublingInfoBubble (int numRepeats, int startN, int avgNum) {
        int N = startN;
        int count = 0; // index of info in different arrays
        int[] NArray = new int[numRepeats];
        double[] timeArray = new double[numRepeats];
        double timeToSort = 0;
        double avgTime = 0;

        while (count < numRepeats) {
            // Average results:
            double sumTime = 0;
            for (int i = 0; i < avgNum; i++) {
                Integer[] a = EdgeCases.ReverseArray(N);
                Stopwatch stopwatch = new Stopwatch(); // start Stopwatch
                Bubble.sort(a);
                timeToSort = stopwatch.elapsedTime(); // record elapsed time

                sumTime = sumTime + timeToSort;
            }
            avgTime = sumTime/avgNum;

            // fill arrays with relevant information
            NArray[count] = N;
            timeArray[count] = avgTime;

            N = N*2;
            count++;
        }

        StdOut.println("Bubble Sort Doubling Information");
        for (int i = 0; i < numRepeats; i++) {
            if (i == 0) {  // Don't do ratios
                StdOut.println("N : " + NArray[i] + ";         T(N) :" + timeArray[i]);
            }
            else {
                StdOut.println("N : " + NArray[i] + ";         T(N) :" + timeArray[i] + ";                          Ratio : " + timeArray[i]/timeArray[i-1] + ";         lg Ratio :" + Math.log(timeArray[i]/timeArray[i-1])/Math.log(2));
            }
        }
    }


    private static void printDoublingInfoMedian3 (int numRepeats, int startN, int avgNum) {
        int N = startN;
        int count = 0; // index of info in different arrays
        int[] NArray = new int[numRepeats];
        double[] timeArray = new double[numRepeats];
        double timeToSort = 0;
        double avgTime = 0;

        while (count < numRepeats) {
            // Average results:
            double sumTime = 0;
            for (int i = 0; i < avgNum; i++) {
                Double[] a = shuffledArray(N);
                Stopwatch stopwatch = new Stopwatch(); // start Stopwatch
                QuickSortMedian3.sort(a);
                timeToSort = stopwatch.elapsedTime(); // record elapsed time

                sumTime = sumTime + timeToSort;
            }
            avgTime = sumTime/avgNum;

            // fill arrays with relevant information
            NArray[count] = N;
            timeArray[count] = avgTime;

            N = N*2;
            count++;
        }

        StdOut.println("Median 3 Sort Doubling Information");
        for (int i = 0; i < numRepeats; i++) {
            if (i == 0) {  // Don't do ratios
                StdOut.println("N : " + NArray[i] + ";         T(N) :" + timeArray[i]);
            }
            else {
                StdOut.println("N : " + NArray[i] + ";         T(N) :" + timeArray[i] + ";                          Ratio : " + timeArray[i]/timeArray[i-1] + ";         lg Ratio :" + Math.log(timeArray[i]/timeArray[i-1])/Math.log(2));
            }
        }
    }


    private static void printDoublingInfoQuick (int numRepeats, int startN, int avgNum) {
        int N = startN;
        int count = 0; // index of info in different arrays
        int[] NArray = new int[numRepeats];
        double[] timeArray = new double[numRepeats];
        double timeToSort = 0;
        double avgTime = 0;

        while (count < numRepeats) {
            // Average results:
            double sumTime = 0;
            for (int i = 0; i < avgNum; i++) {
                Double[] a = shuffledArray(N);
                Stopwatch stopwatch = new Stopwatch(); // start Stopwatch
                Quick.sort(a);
                timeToSort = stopwatch.elapsedTime(); // record elapsed time

                sumTime = sumTime + timeToSort;
            }
            avgTime = sumTime/avgNum;

            // fill arrays with relevant information
            NArray[count] = N;
            timeArray[count] = avgTime;

            N = N*2;
            count++;
        }

        StdOut.println("Quick Sort Doubling Information");
        for (int i = 0; i < numRepeats; i++) {
            if (i == 0) {  // Don't do ratios
                StdOut.println("N : " + NArray[i] + ";         T(N) :" + timeArray[i]);
            }
            else {
                StdOut.println("N : " + NArray[i] + ";         T(N) :" + timeArray[i] + ";                          Ratio : " + timeArray[i]/timeArray[i-1] + ";         lg Ratio :" + Math.log(timeArray[i]/timeArray[i-1])/Math.log(2));
            }
        }
    }



    public static void main(String[] args) {
        //printDoublingInfoSelection(6, 1000, 5);
        //printDoublingInfoInsertion(5, 1000, 10);
        //printDoublingInfoShell(5, 1000, 10);
        //printDoublingInfoBubble(10, 1000, 2);
        //printDoublingInfoQuick(10, 1000, 2);
        //printDoublingInfoMedian3(10, 1000, 2);
        //printDoublingInfoMedian5(10, 1000, 2);

        //Selection.sort(a);  // selection sort
        //Insertion.sort(a);  // insertion sort
        //Shell.sort(a);      // Shellsort
    }
}
