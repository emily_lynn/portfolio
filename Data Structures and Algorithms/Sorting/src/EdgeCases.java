public class EdgeCases {
    /***********************************************************************
     *  main() function
     *  Place all of your unit tests here
     *  Hint: created additional functions to help organize your tests
     ***********************************************************************/

    public Integer[] OrderedArray (int N) {
        Integer[] a = new Integer[N];
        for (int i = 0; i < N; i++) {
            a[i] = i;
        }
        return a;
    }

    public static Integer[] ReverseArray (int N) {
        Integer[] a = new Integer[N];
        for (int i = 0; i < N; i++) {
            a[i] = N-i;
        }
        return a;
    }

    private Double[] Size1Array () {
        Double[] a = new Double[1];
        a[0] = StdRandom.uniform();
        return a;
    }

    public Double[] SingleValueArray (int N) {
        Double[] a = new Double[N];
        Double value = StdRandom.uniform();
        for (int i = 0; i < N; i++) {
            a[i] = value;
        }
        return a;
    }

    public Double[] DoubleAlternatingArray (int N) {
        Double[] a = new Double[N];
        Double val1 = StdRandom.uniform();
        Double val2 = StdRandom.uniform();
        int count = 0;
        for (int i = 0; i < N; i ++) {
            if (count%2 == 0) { a[i] = val1; }
            else { a[i] = val2; }
            count++;
        }
        return a;
    }


    public static void main(String[] args) {
        Double[] a = { 0.0 };

        Bubble.sort(a);     // bubble sort
        Selection.sort(a);  // selection sort
        Insertion.sort(a);  // insertion sort
        Shell.sort(a);      // Shellsort
        Quick.sort(a);      // quicksort
    }
}
