import java.util.Iterator;
import java.util.NoSuchElementException;

public class LinkedList<Item extends Comparable<Item>> implements Iterable<Item> {
    private Node first = null;
    private int count = 0;
    private int naturalRunCount = 0;
    private boolean reachedEnd = false;
    private Node beforeSortSection = null; // keeps track of where we will have to reattach the beginning of the sorted section
    private Node afterSortSection = null;  // keeps track of where we will have to reattach the end of the sorted section
    private Node beg1Run = null;
    private Node end1Run = null;
    private Node beg2Run = null;
    private Node end2Run = null;
    private Node begOfSorted = null;
    private Node endOfSorted = null;
    private Node compare1 = null;
    private Node compare2 = null;


    private class Node {
        final Item item;    // cannot change item once it is set in constructor
        Node next;          // can change next, though

        public Node(Item i, Node n) {
            item = i;
            next = n;
        }
    }

    public LinkedList() { }

    public LinkedList(Item[] fromList) {
        for(Item item : fromList) insert(item);
    }

    public void insert(Item item) {
        first = new Node(item, first);
        count++;
    }

    public Item remove() {
        Item item = first.item;
        first = first.next;
        count--;
        return item;
    }

    public boolean isEmpty() { return count == 0; }

    private Node FindNaturalRun(Node start, Iterator itr) { // start is the Node at the beginning of the potential run
        Node end = start; // "end" is the last node in the natural run (before item gets < the previous end)
        while (itr.hasNext ()) {
            if (end.next == null) {
                reachedEnd = true;
                break;
            }
            Item nextNum = end.next.item;
            itr.next(); // So that the iterator iterates even though we don't use the next();
            if (nextNum.compareTo(end.item) >= 0) { // nextNum >= end.item
                end = end.next;
            }
            if (nextNum.compareTo(end.item) < 0) {  // nextNum < end.item
                break;
            }
        }
        return end;
    }

    private void MergeRuns(){
        while (true) {
            if ( (compare2 == null) && (begOfSorted == null)) { // There is only 1 natural run, and it is already sorted
                //StdOut.println("compare2 == null && begOfSorted == null");
                begOfSorted = compare1;
                endOfSorted = end1Run;
                compare1 = null;
                compare2 = null;
                //printSortSection();
                break;
            }
            else if (compare1 == null) {
                //StdOut.println("compare1 == null");
                endOfSorted.next = compare2;
                endOfSorted = end2Run;
                compare1 = null;
                compare2 = null;
                //printSortSection();
                break;
            }
            else if (compare2 == null) {
                //StdOut.println("compare2 == null");
                endOfSorted.next = compare1;
                endOfSorted = end1Run;
                compare1 = null;
                compare2 = null;
                //printSortSection();
                break;
            }

            if ((compare1.item).compareTo(compare2.item) <= 0) { // compare1.item <= compare2.item
                //StdOut.println("compare1 <= compare2");
                if (begOfSorted == null) {
                    begOfSorted = compare1;
                    endOfSorted = compare1;
                } else {
                    endOfSorted.next = compare1;
                    endOfSorted = endOfSorted.next;
                }
                compare1 = compare1.next;
                endOfSorted.next = null; // disconnect latest addition from the rest of its original list
                //printSortSection();
            }
            else if (compare1.item.compareTo(compare2.item) > 0) { // compare1.item > compare2.item
                //StdOut.println("compare1 > compare2");
                if (begOfSorted == null) {
                    begOfSorted = compare2;
                    endOfSorted = compare2;
                } else {
                    endOfSorted.next = compare2;
                    endOfSorted = compare2;
                }
                compare2 = compare2.next;
                endOfSorted.next = null; // disconnect latest addition from the rest of its list
                //printSortSection();
            }
        }

    }

    public Iterator<Item> iterator() {
        return new Iterator<Item>() {
            Node current = first;

            @Override
            public boolean hasNext() {
                return current != null;
            }

            @Override
            public Item next() {
                Item item = current.item;
                current = current.next;
                return item;
            }
        };
    }

    // Iterates through and prints each element in the list
    private void printList() {
        Iterator<Item> itr = iterator();
        StdOut.println("****Whole List****");
        while(itr.hasNext()) {
            StdOut.println(itr.next()); // prints the current element before iterating to the next
        }
        StdOut.println("********");
        StdOut.println();
    }

    // Prints relevant variables for use before sorting
    private void printSection() {
        StdOut.println("****Section Variables****");
        StdOut.println("Before Sort: " + beforeSortSection.item);
        StdOut.println("First Run : " + beg1Run.item + " " + end1Run.item);
        if (beg2Run != null) {
            StdOut.println("Second Run: " + beg2Run.item + " " + end2Run.item);
        }
        if (afterSortSection != null) {
            StdOut.println("After Sort : " + afterSortSection.item);
        }
        else { StdOut.println("No 2nd Run"); }
        StdOut.println("********");
    }

    // Prints sorted section variables
    private void printSortSection() {
        StdOut.println("****Sort Section Variables****");
        StdOut.println("Beginning of Sorted: " + begOfSorted.item);
        StdOut.println("End of Sorted : " + endOfSorted.item);
        if (compare1 == null) { StdOut.println("compare1: null"); }
        else{ StdOut.println("compare1: " + compare1.item); }
        if (compare2 == null) { StdOut.println("compare2: null"); }
        else{ StdOut.println("compare2: " + compare2.item); }
        StdOut.println("********");
        StdOut.println();
    }

    // Prints all total function variables
    private void printAllVariables() {
        StdOut.println("****Variables****");
        if (beforeSortSection != null) {StdOut.println("beforeSortSection: " + beforeSortSection.item);}
        else {StdOut.println("beforeSortSection: null"); }
        if (begOfSorted != null) {StdOut.println("begOfSorted: " + begOfSorted.item);}
        else {StdOut.println("begOfSorted: null"); }
        if (endOfSorted != null) {StdOut.println("endOfSorted: " + endOfSorted.item);}
        else {StdOut.println("endOfSorted: null"); }
        if (compare1 != null) {StdOut.println("compare1: " + compare1.item);}
        else {StdOut.println("compare1: null"); }
        if (compare2 != null) {StdOut.println("compare2: " + compare2.item);}
        else {StdOut.println("compare2: null"); }
        if (afterSortSection != null) {StdOut.println("afterSortSection: " + afterSortSection.item);}
        else {StdOut.println("afterSortSection: null"); }
        StdOut.println("naturalRunCount: " + naturalRunCount);
        StdOut.println("reachedEnd: " + reachedEnd);
        StdOut.println("********");
        StdOut.println();
    }

    /***********************************************************************
     * Rearranges the linked list in ascending order, using the natural order
     * and mergesort.
     ***********************************************************************/

    public void sort() {
        if (this.isEmpty()) { throw new NoSuchElementException("Cannot sort an empty list");}
        while (naturalRunCount != 1) { // When there is only 1 natural run, the whole list is sorted
            Iterator<Item> itr = this.iterator(); // takes advantage of the fact that this iterator doesn't freak out if list is changed between uses to find all natural runs
            naturalRunCount = 0;
            reachedEnd = false;
            beforeSortSection = first;
            beg1Run = first;
            while (true) {
                //StdOut.println(reachedEnd);
                if (!reachedEnd) {
                    end1Run = FindNaturalRun(beg1Run, itr);
                    naturalRunCount++;
                    compare1 = beg1Run;
                    //StdOut.println(reachedEnd);
                }

                if (!reachedEnd) {
                    beg2Run = end1Run.next;
                    end2Run = FindNaturalRun(beg2Run, itr);
                    naturalRunCount++;
                    afterSortSection = end2Run.next;
                    end1Run.next = null; // safe since beg2Run is pointing to the next item
                    end2Run.next = null; // safe since afterSortSection is pointing to the rest of the list
                    compare2 = beg2Run;
                } else {
                    compare2 = null;
                    afterSortSection = null;
                }
                //StdOut.println("Before Merge...");
                //printAllVariables();
                //printSection();
                MergeRuns();
                if (beforeSortSection == first) {
                    first = begOfSorted;
                } else {
                    beforeSortSection.next = begOfSorted;
                }
                endOfSorted.next = afterSortSection;
                if (afterSortSection == null) { // The end of the last run was the end of the list and we no longer need to look for runs
                    endOfSorted = null;
                    begOfSorted = null;
                    //StdOut.println("Breaking out of while(true) loop");
                    break;
                }
                beforeSortSection = endOfSorted;
                beg1Run = afterSortSection;

                // reset variables:
                endOfSorted = null;
                begOfSorted = null;
                //StdOut.println("New semi-order:");
                //printList();
            }
            //StdOut.println("After recent Merge...");
            //printAllVariables();
            //printList();
        }
    }

    /***********************************************************************
     *  main() function
     *  Place all of your unit tests here
     *  Hint: created additional functions to help organize your tests
     ***********************************************************************/

    public static void main(String[] args) {
        //testEmpty();
        //testAlreadySorted();
        //test1Items();
        //test2Items();
        //test2ItemsOutOfOrder();
        //test2Runs();
        //test3Runs();
        //testLongListInOrder();
        //testLongListInverted();
        testRepeats();
    }


    /**************************** UNIT TESTS ******************************/

    private static void testEmpty(){
        Double[] a = {};
        LinkedList<Double> linkedlist = new LinkedList<>(a);
        StdOut.println("Before Sort...");
        linkedlist.printList();
        linkedlist.sort();
        linkedlist.printList();
    }

    private static void testAlreadySorted (){
        Double[] a = {8.0, 7.0, 6.0, 5.0, 4.0, 3.0, 2.0};
        LinkedList<Double> linkedlist = new LinkedList<>(a);
        StdOut.println("Before Sort...");
        linkedlist.printList();
        linkedlist.sort();
        linkedlist.printList();
    }

    private static void test1Items () {
        Double[] a = {8.0};
        LinkedList<Double> linkedlist = new LinkedList<>(a);
        StdOut.println("Before Sort...");
        linkedlist.printList();
        linkedlist.sort();
        linkedlist.printList();
    }

    private static void test2Items () {
        Double[] a = {9.0, 8.0};
        LinkedList<Double> linkedlist = new LinkedList<>(a);
        StdOut.println("Before Sort...");
        linkedlist.printList();
        linkedlist.sort();
        linkedlist.printList();
    }

    private static void test2ItemsOutOfOrder () {
        Double[] a = {8.0, 9.0};
        LinkedList<Double> linkedlist = new LinkedList<>(a);
        StdOut.println("Before Sort...");
        linkedlist.printList();
        linkedlist.sort();
        linkedlist.printList();
    }

    private static void test2Runs (){
        Double[] a = {7.0, 8.0, 6.0, 5.0, 4.0, 3.0, 2.0};
        LinkedList<Double> linkedlist = new LinkedList<>(a);
        StdOut.println("Before Sort...");
        linkedlist.printList();
        linkedlist.sort();
        linkedlist.printList();
    }

    private static void test3Runs () {
        Double[] a = {7.0, 8.0, 4.0, 2.0, 6.0, 5.0, 3.0};
        LinkedList<Double> linkedlist = new LinkedList<>(a);
        StdOut.println("Before Sort...");
        linkedlist.printList();
        linkedlist.sort();
        linkedlist.printList();
    }

    private static void testLongListInOrder () {
        Integer[] a = {20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0};
        LinkedList<Integer> linkedlist = new LinkedList<>(a);
        StdOut.println("Before Sort...");
        linkedlist.printList();
        linkedlist.sort();
        linkedlist.printList();
    }

    private static void testLongListInverted () {
        Integer[] a = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20};
        LinkedList<Integer> linkedlist = new LinkedList<>(a);
        StdOut.println("Before Sort...");
        linkedlist.printList();
        linkedlist.sort();
        linkedlist.printList();
    }

    private static void testRepeats() {
        Integer[] a = {0, 0, 0, 3, 3, 5, 6, 6, 8, 9, 10, 11, 11, 11, 11, 15, 16, 17, 18, 20, 20};
        LinkedList<Integer> linkedlist = new LinkedList<>(a);
        StdOut.println("Before Sort...");
        linkedlist.printList();
        linkedlist.sort();
        linkedlist.printList();
    }


}
