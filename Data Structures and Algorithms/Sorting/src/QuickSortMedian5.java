/**
 * The following code is *mostly* a copy of Quick class (quick sort) from algs4.jar
 */

public class QuickSortMedian5 extends QuickSortMedian {
    private int compareCalls = 0;

    public static class MedianOf5 extends MedianOfN {
        public MedianOf5() {
            // tell QuickSortMedian.MedianOfN we will find the median of 5 items
            super(5);
        }

        /***********************************************************
         * Determines which index in parameter indices points to
         * the median value in parameter a
         * @param a the array containing values
         * @param indices the array containing indices into a
         * @return the index of median value
         ***********************************************************/
        public int median(Comparable[] a, int[] indices) {
            // get values at specified indices
            int i0 = indices[0];
            int i1 = indices[1];
            int i2 = indices[2];
            int i3 = indices[3];
            int i4 = indices[4];
            Comparable a0 = a[i0];
            Comparable a1 = a[i1];
            Comparable a2 = a[i2];
            Comparable a3 = a[i3];
            Comparable a4 = a[i4];

            // quicksort the median array of 5
            Comparable[] medianArray = {a0, a1, a2, a3, a4};
            int lo = 0;
            int hi = medianArray.length -1;

            StdRandom.shuffle(medianArray);
            medianSort(medianArray, 0, a.length - 1);
            //StdOut.println(compareCalls);

            Comparable median = medianArray[2];
            if (median == a0) {
                return i0;
            }
            else if (median == a1) {
                return i1;
            }
            else if (median == a2) {
                return i2;
            }
            else if (median == a3) {
                return i3;
            }
            else {
                return i4;
            }
        }

    }

    // method for implementation in median
    private static int partition(Comparable[] a, int lo, int hi) {
        int i = lo, j = hi + 1;
        while(true) {
            while(a[++i].compareTo(a[lo]) < 0)   // find item on left to swap
                //compareCalls++;
                if(i == hi) break;
            while(a[lo].compareTo(a[--j]) < 0)   // find item on right to swap
                //compareCalls++;
                if(j == lo) break;
            if(i >= j) break;                    // check if pointers cross
            //exch(a, i, j);                     // swap
            Comparable oldI = a[i];
            a[i] = a[j];
            a[j] = oldI;
        }
        //exch(a, lo, j);                 // swap with partition item
        Comparable oldLo = a[lo];
        a[lo] = a[j];
        a[j] = oldLo;
        return j;       // return index of item now know to be in place
    }

    // method for implementation in median
    private static void medianSort(Comparable[] a, int lo, int hi) {
        if(hi <= lo) return;
        int j = partition(a, lo, hi);
        medianSort(a, lo, j-1);
        medianSort(a, j+1, hi);
    }

    /***********************************************************************
     * Rearranges the array in ascending order, using the natural order.
     * @param a the array to be sorted
     ***********************************************************************/
    public static void sort(Comparable[] a) {
        MedianOf5 median = new MedianOf5();
        QuickSortMedian.sort(a, median);
    }


    /***********************************************************************
     *  main() function
     *  Place all of your unit tests here
     *  Hint: created additional functions to help organize your tests
     ***********************************************************************/

    public static void main(String[] args) {
        //Double[] a = {0.0};
        //QuickSortMedian5.sort(a);
        test1();
        test2();
        test3();
        test4();
    }

    // "false" median function to test medianof5 functionality
    private static int medianTest(Comparable[] a) {
        //compareCalls = 0;
        // get values at specified indices
        int i0 = 0;
        int i1 = 1;
        int i2 = 2;
        int i3 = 3;
        int i4 = 4;
        Comparable a0 = a[i0];
        Comparable a1 = a[i1];
        Comparable a2 = a[i2];
        Comparable a3 = a[i3];
        Comparable a4 = a[i4];

        // quicksort the median array of 5
        Comparable[] medianArray = {a0, a1, a2, a3, a4};
        int lo = 0;
        int hi = medianArray.length -1;

        StdRandom.shuffle(medianArray);
        medianSort(medianArray, 0, a.length - 1);
        //StdOut.println(compareCalls);

        Comparable median = medianArray[2];
        if (median == a0) {
            return i0;
        }
        else if (median == a1) {
            return i1;
        }
        else if (median == a2) {
            return i2;
        }
        else if (median == a3) {
            return i3;
        }
        else {
            return i4;
        }
    }

    /*************************UNIT TEST*******************************/

    private static void test1() {
        Double[] a = {0.0, 1.0, 2.0, 3.0, 4.0};
        StdOut.println(a[medianTest(a)]);
    }

    private static void test2() {
        Double[] a = {4.0, 3.0, 2.0, 1.0, 0.0};
        StdOut.println(a[medianTest(a)]);
    }

    private static void test3() {
        Double[] a = {1.0, 1.0, 2.0, 3.0, 3.0};
        StdOut.println(a[medianTest(a)]);
    }

    private static void test4() {
        Double[] a = {2.0, 2.0, 2.0, 2.0, 2.0};
        StdOut.println(a[medianTest(a)]);
    }
}
