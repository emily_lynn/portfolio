import java.util.NoSuchElementException;

public class WordFind {
    private char[][] board;
    int width;
    int height;
    String[] words;

    public WordFind(int width, int height, String[] words) {
        this.width = width;
        this.height = height;
        this.words = words;
        board = new char[height][width];

        initializeBoard();

        int i = 0; // index of the word in the words array
        int boardTries = 0; // number of times the program tried to create a board and wasn't able to fit all of the words in it
        while( i < words.length && boardTries < 3 ) {
            char[] wordArray = words[i].toCharArray();
            if ( (wordArray.length > height) && (wordArray.length > width) ) {
                throw new NoSuchElementException("Word is too long for this size of board");
            }
            int orient = StdRandom.uniform(0, 3); // orient is either 0, 1, or 2
            //StdOut.println(orient);
            if (orient == 0) {
                if (!placeWordHoriz(wordArray)) {
                    //StdOut.println("Did not fit horizontally");
                    if (!placeWordDiagonal(wordArray)) {
                        //StdOut.println("Did not fit diagonally");
                        if (!placeWordVert(wordArray)) {
                            //StdOut.println("Did not fit vertically, resetting board");
                            initializeBoard(); // resets board
                            i = 0; // resets while loop
                            boardTries++;
                            continue;
                        }
                    }
                }
            }
            else if (orient == 1){
                if (!placeWordVert(wordArray)) {
                    //StdOut.println("Did not fit vertically");
                    if (!placeWordHoriz(wordArray)) {
                        //StdOut.println("Did not fit horizontally");
                        if (!placeWordDiagonal(wordArray)) {
                            //StdOut.println("Did not fit Diagonally, resetting board");
                            initializeBoard(); // resets board
                            i = 0; // resets while loop
                            boardTries++;
                            continue;
                        }
                    }
                }
            }
            else {
                if ((wordArray.length > width) || !placeWordDiagonal(wordArray)) {
                    //StdOut.println("Did not fit diagonally");
                    if (!placeWordVert(wordArray)) {
                        //StdOut.println("Did not fit vertically");
                        if (!placeWordHoriz(wordArray)) {
                            //StdOut.println("Did not fit horizontally, resetting board");
                            initializeBoard(); // resets board
                            i = 0; // resets while loop
                            boardTries++;
                            continue;
                        }
                    }
                }
            }
            i++;
        }
        if (boardTries > 2) {
            throw new NoSuchElementException("Could not find an orientation that fit all words");
        }
        //printBoard();
        fillRandom();
        printBoard();
        StdOut.println();
        StdOut.println();

        for (i = 0; i < words.length; i++) {
            StdOut.println(words[i]);
        }
    }

    private void printBoard() {
        for (int h = 0; h < height; h++) {
            for (int w = 0; w < width; w++) {
                StdOut.print(board[h][w] + " ");
            }
            StdOut.println();
        }
        StdOut.println();
    }

    private void initializeBoard() {
        for (int h = 0; h < height; h++) {
            for (int w = 0; w < width; w++) {
                board[h][w] = (char) 35; // 35 is ascii for #
                //int letterCode = StdRandom.uniform(65, 90); //Ascii codes for the uppercase letters
                //board[h][w] = (char) letterCode;
            }
        }
    }

    private boolean placeWordHoriz(char[] wordArray) {
        int tries = 0;
        while(tries < width*height) {
            int row = StdRandom.uniform(0, height);
            int col = StdRandom.uniform(0, width);
            if ((width - col) < wordArray.length) { // word will not fit in space left on the row
                tries++;
               continue;
            }
            if (fitHoriz(wordArray, row, col)) {
                for (int letter = 0; letter < wordArray.length; letter++) {
                    board[row][col+letter] = wordArray[letter];
                }
                return true;
            }
            tries++;
        }
        return false;
    }

    private boolean placeWordVert(char[] wordArray) {
        if (wordArray.length > height) { // word will never fit vertically
            return false;
        }
        int tries = 0;
        while(tries < height*width) {
            int row = StdRandom.uniform(0, height);
            int col = StdRandom.uniform(0, width);
            if ((height - row) < wordArray.length) { // word will not fit in space left on the row
                tries++;
                continue;
            }
            if (fitVert(wordArray, row, col)) {
                for (int letter = 0; letter < wordArray.length; letter++) {
                    board[row+letter][col] = wordArray[letter];
                }
                return true;
            }
            tries++;
        }
        return false;
    }

    private boolean placeWordDiagonal(char[] wordArray) {
        int tries = 0;
        while(tries < (height*width)) {
            int row = StdRandom.uniform(0, height);
            int col = StdRandom.uniform(0, width);
            if (((height-row) < wordArray.length) || ((width-col) < wordArray.length)){ // word will not fit diagonally here (the longest diagonal is as long as the width)
                tries++;
                continue;
            }
            if (fitDiag(wordArray, row, col)) {
                for (int letter = 0; letter < wordArray.length; letter++) {
                    board[row+letter][col+letter] = wordArray[letter];
                }
                return true;
            }
            tries++;
        }
        return false;
    }

    private boolean fitHoriz (char[] wordArray, int row, int col) {
        for (int i = col; i < (wordArray.length + col); i++) {
            if ( (board[row][i] != '#') && (board[row][i] != wordArray[i-col]) ) { return false; }
        }
        return true;
    }

    private boolean fitVert (char[] wordArray, int row, int col) {
        for (int i = row; i < (wordArray.length + row); i++) {
            if ( (board[i][col] != '#') && (board[i][col] != wordArray[i-row])) { return false; }
        }
        return true;
    }

    private boolean fitDiag (char[] wordArray, int row, int col) {
        for (int i = row; i < (wordArray.length + row); i++) {
            //StdOut.println("i :" + i + ";  col + i - row : " + (col+i-row) );
            if ( (board[i][col + i - row] != '#') && (board[i][col + i - row] != wordArray[i-row])) { return false; }
        }
        return true;
    }

    private void fillRandom () {
        for (int h = 0; h < height; h++) {
            for (int w = 0; w < width; w++) {
                if (board[h][w] == (char) 35){
                    int letterCode = StdRandom.uniform(65, 91); //Ascii codes for the uppercase letters
                    board[h][w] = (char) letterCode;
                }
            }
        }
    }


    public static void main(String[] args) {
        String[] words = {
                "COMPARETO",
                "COMPARABLE",
                "ALGORITHMS",
                "DATA",
                "STRUCTURES",
                "PROGRAMMING",
                "JAVA",
                "STACK",
                "QUEUE",
                "DEQUEUE",
                "LINKEDLIST",
                "ARRAY",
                "QUICKSORT",
                "MERGESORT",
                "INSERTIONSORT",
                "SELECTIONSORT",
                "SHELLSORT"
        };
        WordFind wordfind = new WordFind( 50, 50, words);
    }
}
