/**

 * @brief BattleBoi AI for battleships
 * @file BattleBoi.cpp
 * @author Emily Lynn
 * @author Elizabeth Vandegriff
 * @date November, 2017
 *
 * This Battleships AI is going to kick ass.
 */

#include <iostream>
#include <cstdio>
#include <cstdlib>

#include "conio.h"
#include "BattleBoi.h"

using namespace conio;

/**
 * @brief Constructor that initializes any inter-round data structures.
 * @param boardSize Indication of the size of the board that is in use.
 *
 * The constructor runs when the AI is instantiated (the object gets created)
 * and is responsible for initializing everything that needs to be initialized
 * before any of the rounds happen. The constructor does not get called
 * before rounds; newRound() gets called before every round.
 */
BattleBoi::BattleBoi( int boardSize )
    :PlayerV2(boardSize)
{
    functionChoice = 2;
}

/**
 * @brief Destructor placeholder.
 * If your code does anything that requires cleanup when the object is
 * destroyed, do it here in the destructor.
 */
BattleBoi::~BattleBoi( ) {}

/*
 * Private internal function that initializes a MAX_BOARD_SIZE 2D array of char to water.
 */
void BattleBoi::initializeBoard() {
    for(int row=0; row<boardSize; row++) {
	for(int col=0; col<boardSize; col++) {
	    this->board[row][col] = WATER;
	}
    }
}


void BattleBoi::makeShotArray(){
    int row = 0, col = 0, i = 0;
    for(int p=0;p<100;p++){
        this->rowShotArray[p]=0;
        this->colShotArray[p]=0;
    }
    while((row < boardSize)){
        rowShotArray[i]=row;
        colShotArray[i]=col;
        col+=3;
        if(col > boardSize-1){
            row++;
            col=row%3;
        }
        i++;
    }

    for(int s=0;s<100;s++){
        int rIndex=rand()%100;
        int tempRow=rowShotArray[s];
        int tempCol=colShotArray[s];
        rowShotArray[s]=rowShotArray[rIndex];
        colShotArray[s]=colShotArray[rIndex];
        rowShotArray[rIndex]=tempRow;
        colShotArray[rIndex]=tempCol;

    }
    

}


/**
 * @brief Specifies the AI's shot choice and returns the information to the caller.
 * @return Message The most important parts of the returned message are
 * the row and column values.
 *
 * See the Message class documentation for more information on the
 * Message constructor.
 */
Message BattleBoi::getMove() {
    int tempRow=lastRow;
    int tempCol=lastCol;
    if(shotIndex >= 100){
        shotIndex=0;
    }
    switch(board[lastRow][lastCol]){
        case SHIP:
        case WATER:
            if(hunting>3){
                for(int r=0;r<boardSize;r++){
                    for(int c=0;c<boardSize;c++){
                        if(board[r][c]==HIT){
                            hitRow=r;
                            hitCol=c;
                            hunting=0;
                            hunter();
                        }
                    }
                }
            }
            lastRow=rowShotArray[shotIndex];
            lastCol=colShotArray[shotIndex];
            while(!(board[lastRow][lastCol]==WATER || board[lastRow][lastCol]==SHIP || board[lastRow][lastCol]==HIT)){
                shotIndex++;
                if(shotIndex >= 100){
                    shotIndex=0;
                }
                lastRow=rowShotArray[shotIndex];
                lastCol=colShotArray[shotIndex];
            }
            break;
        case MISS:
            if(hunting < 4){
                hunting++;
                lastRow=hitRow;
                lastCol=hitCol;
                hunter();
            }
            else{
                for(int r=0;r<boardSize;r++){
                    for(int c=0;c<boardSize;c++){
                        if(board[r][c]==HIT){
                            hitRow=r;
                            hitCol=c;
                            hunting=0;
                            hunter();
                        }
                    }
                }
                lastRow=rowShotArray[shotIndex];
                lastCol=colShotArray[shotIndex];
                while(!(board[lastRow][lastCol]==WATER || board[lastRow][lastCol]==SHIP || board[lastRow][lastCol]==HIT)){
                    shotIndex++;
                    if(shotIndex >= 100){
                        shotIndex=0;
                    }
                    lastRow=rowShotArray[shotIndex];
                    lastCol=colShotArray[shotIndex];
                }
            }
            break;
        case HIT:
            if(hunting < 4){
                hunter();
            }
            else{
                hitRow=lastRow;
                hitCol=lastCol;
                hunting = 0;
                hunter();
                /**if(!searchSouth()){
                    hunting++;
                    if(!searchNorth()){
                        hunting++;
                        if(!searchWest()){
                            hunting++;
                            searchEast();                           
                        }
                    }
                }*/
            }
            break;
        case KILL:
            if(board[hitRow][hitCol]==HIT){
                lastRow=hitRow;
                lastCol=hitCol;
                hunting = 0;
                hunter();
            }
            else{
                hunting = 4;
                lastRow=rowShotArray[shotIndex];
                lastCol=colShotArray[shotIndex];
                while(!(board[lastRow][lastCol]==WATER || board[lastRow][lastCol]==SHIP || board[lastRow][lastCol]==HIT)){
                    shotIndex++;
                    if(shotIndex >= 100){
                        shotIndex=0;
                    }
                    lastRow=rowShotArray[shotIndex];
                    lastCol=colShotArray[shotIndex];
                }
            }
            break;
    }
    if(lastRow==tempRow && lastCol==tempCol){
        shotIndex++;
        lastRow=rowShotArray[shotIndex];
        lastCol=colShotArray[shotIndex];
    }
    Message result( SHOT, lastRow, lastCol, "Bang", None, 1 );
    return result;
}


void BattleBoi::hunter(){
    bool found = false;
    int counter = 0;
    while(!found && counter < 10){
        switch(hunting){
            case 0:
                found = searchSouth();
                if(!found){
                    hunting++;
                }
                break;
            case 1:
                found = searchNorth();
                if(!found){
                    hunting++;
                }
                break;
            case 2:
                found = searchEast();
                if(!found){
                    hunting++;
                }
                break;
            case 3:
                found = searchWest();
                if(!found){
                    hunting++;
                }
                break;
            default:
                lastRow=hitRow;
                lastCol=hitCol;
                hunting=0;
                break;
        }
        counter++;
    }

}



bool BattleBoi::searchNorth() {
    if(lastRow-1 < 0 ){
        return false;
    }
    else if(board[lastRow-1][lastCol]==WATER || board[lastRow-1][lastCol]==SHIP){
        lastRow--;
        return true;
    }
    /**while((lastRow-1) >=0 && board[lastRow-1][lastCol]==HIT){
        lastRow--;
        if(board[lastRow][lastCol]==WATER || board[lastRow][lastCol]==SHIP){
            return true;
        }
    }*/
    return false;
}

bool BattleBoi::searchSouth() {
    if(lastRow+1 > boardSize-1 ){
        return false;
    }
    else if(board[lastRow+1][lastCol]==WATER || board[lastRow+1][lastCol]==SHIP){
        lastRow++;
        return true;
    }
    /**while((lastRow+1) <(boardSize-1) && board[lastRow+1][lastCol]==HIT){
        lastRow++;
        if(board[lastRow][lastCol]==WATER || board[lastRow][lastCol]==SHIP){
            return true;
        }
    }*/
    return false;
}


bool BattleBoi::searchWest() {
    if(lastCol-1 < 0){
        return false;
    }
    else if(board[lastRow][lastCol-1]==WATER || board[lastRow][lastCol-1]==SHIP){
        lastCol--;
        return true;
    }
    /**while((lastCol-1) >=0 && board[lastRow][lastCol-1]==HIT){
        lastCol--;
        if(board[lastRow][lastCol]==WATER || board[lastRow][lastCol]==SHIP){
            return true;
        }
    }*/
    return false;
}

bool BattleBoi::searchEast() {
    if(lastCol+1 > boardSize-1){
        return false;
    }
    else if(board[lastRow][lastCol+1]==WATER || board[lastRow][lastCol+1]==SHIP){
        lastCol++;
        return true;
    }
    /**while((lastCol+1) <(boardSize-1) && board[lastRow][lastCol+1]==HIT){
        lastCol++;
        if(board[lastRow][lastCol]==WATER || board[lastRow][lastCol]==SHIP){
            return true;
        }
    }*/
    return false;
}




/**
 * @brief Tells the AI that a new round is beginning.
 * The AI show reinitialize any intra-round data structures.
 */
void BattleBoi::newRound() {
    /* DumbPlayer is too simple to do any inter-round learning. Smarter players
     * reinitialize any round-specific data structures here.
     */
    this->lastRow = 0;
    this->lastCol = 0;
    this->hitRow = 0;
    this->hitCol = 0;
    this->hunting = 4;
    this->shotIndex = 0;
    this->numShipsPlaced = 0;
    this->functionChoice = 2;
    this->initializeBoard();
    this->makeShotArray();
}

/**
 * @brief Gets the AI's ship placement choice. This is then returned to the caller.
 * @param length The length of the ship to be placed.
 * @return Message The most important parts of the returned message are
 * the direction, row, and column values.
 *
 * The parameters returned via the message are:
 * 1. the operation: must be PLACE_SHIP
 * 2. ship top row value
 * 3. ship top col value
 * 4. a string for the ship name
 * 5. direction Horizontal/Vertical (see defines.h)
 * 6. ship length (should match the length passed to placeShip)
 */
Message BattleBoi::placeShip(int length) {
    char shipName[10];
    // Create ship names each time called: Ship0, Ship1, Ship2, ...
    snprintf(shipName, sizeof shipName, "Ship%d", numShipsPlaced);

    int dirChooser = rand()%2;
    if (dirChooser == 0) {
      this->dir = Horizontal;
    }
    else {
      this->dir = Vertical;
    }

    int row = 0;
    int col = 0;

    int tryCounter = 0;

    int EdgeArray[4];
    EdgeArray[0] = 0;
    EdgeArray[1] = boardSize-1;
    EdgeArray[2] = boardSize-2;
    EdgeArray[3] = 1;

    dir = Vertical; /////////////////////////////

    if (functionChoice == 2) {
	while (tryCounter <= 25) {
	    if (dir == Horizontal) {
			row = ( rand() % 2 ) + (boardSize-2);
			//col = rand()%((boardSize-length) + 1);
			col = rand()%2;
	    }

	    if (dir == Vertical) {
		col = ( rand() % 2 ) + (boardSize-2);
		//row = rand()%((boardSize-length) + 1);
		row = boardSize - length;
	    }

	    if (isSafeMove(row, col, length, dir)) {
		markShip(row, col, length, dir);
		Message response( PLACE_SHIP, row, col, shipName, dir, length );
		return response;
	    }
	    tryCounter++;
	}

	tryCounter = 0;
	if (dir == Horizontal) {
	    dir = Vertical;
	}
	else {
	    dir = Horizontal;
	}

	while (tryCounter <= 25) {
	    if (dir == Horizontal) {
			row = ( rand() % 2 ) + (boardSize-2);
			//col = rand()%((boardSize-length) + 1);
			col = rand()%2;
	    }

	    if (dir == Vertical) {
		col = ( rand() % 2 ) + (boardSize-2);
		//row = rand()%((boardSize-length) + 1);
		row = boardSize - length;
	    }

	    if (isSafeMove(row, col, length, dir)) {
		markShip(row, col, length, dir);
		Message response( PLACE_SHIP, row, col, shipName, dir, length );
		return response;
	    }
	    tryCounter++;
	}

	tryCounter = 0;

	while (tryCounter <= 25) {
	    if (dir == Horizontal) {
			row = ( rand() % 3 ) + (boardSize-3);
			//col = rand()%((boardSize-length) + 1);
			col = (boardSize-3);
	    }

	    if (dir == Vertical) {
		//col = ( rand() % 3 ) + (boardSize-3);
		//row = rand()%((boardSize-length) + 1);
		col = boardSize-3;
		//row = rand()%((boardSize-length) + 1);
		row = boardSize - length;
	    }

	    if (isSafeMove(row, col, length, dir)) {
		markShip(row, col, length, dir);
		Message response( PLACE_SHIP, row, col, shipName, dir, length );
		return response;
	    }
	    tryCounter++;
	}

	tryCounter = 0;
	if (dir == Horizontal) {
	    dir = Vertical;
	}
	else {
	    dir = Horizontal;
	}

	while (tryCounter <= 25) {
	    if (dir == Horizontal) {
			row = ( rand() % 3) + (boardSize-3);
			col = rand()%((boardSize-length) + 1);
	    }

	    if (dir == Vertical) {
		//col = ( rand() % 3 ) + (boardSize-3);
		col = boardSize - 3;
		//row = rand()%((boardSize-length) + 1);
		row = boardSize - length;
	    } 

	    if (isSafeMove(row, col, length, dir)) {
		markShip(row, col, length, dir);
		Message response( PLACE_SHIP, row, col, shipName, dir, length );
		return response;
	    }
	    tryCounter++;
	}

	tryCounter = 0;

	while (tryCounter <= 25) {
	    if (dir == Horizontal) {
			row = rand()%boardSize;
			col = rand()%((boardSize-length) + 1);
	    }

	    if (dir == Vertical) {
		col = rand()%boardSize;
		row = rand()%((boardSize-length) + 1);
	    }

	    if (isSafeMove(row, col, length, dir)) {
		markShip(row, col, length, dir);
		Message response( PLACE_SHIP, row, col, shipName, dir, length );
		return response;
	    }
	    tryCounter++;
	}
	tryCounter = 0;
	Message response( PLACE_SHIP, 0, 0, shipName, dir, length );
    }
	
    else if (functionChoice == 0) {
	while(tryCounter <= 25) {
	    int arrayPosition = rand()%4;
	    if (dir == Horizontal) {
		row = EdgeArray[arrayPosition];
		col = rand()%((boardSize-length) + 1);
	    }
	    else {
		col = EdgeArray[arrayPosition];
		row = rand()%((boardSize-length) + 1);
	    }
	    if (isSafeMove(row, col, length, dir)) {
		markShip(row, col, length, dir);
		Message response( PLACE_SHIP, row, col, shipName, dir, length );
		return response;
	    }
	    tryCounter++;
	}

	tryCounter = 0;
	if (dir == Horizontal) {
	    dir = Vertical;
	}
	else {
	    dir = Horizontal;
	}

	while(tryCounter <= 25) {
	    int arrayPosition = rand()%4;
	    if (dir == Horizontal) {
		row = EdgeArray[arrayPosition];
		col = rand()%((boardSize-length) + 1);
	    }
	    else {
		col = EdgeArray[arrayPosition];
		row = rand()%((boardSize-length) + 1);
	    }
	    if (isSafeMove(row, col, length, dir)) {
		markShip(row, col, length, dir);
		Message response( PLACE_SHIP, row, col, shipName, dir, length );
		return response;
	    }
	    tryCounter++;
	}

	tryCounter = 0;

	while (tryCounter <= 25) {
	    if (dir == Horizontal) {
			row = rand()%boardSize;
			col = rand()%((boardSize-length) + 1);
	    }

	    if (dir == Vertical) {
		col = rand()%boardSize;
		row = rand()%((boardSize-length) + 1);
	    }

	    if (isSafeMove(row, col, length, dir)) {
		markShip(row, col, length, dir);
		Message response( PLACE_SHIP, row, col, shipName, dir, length );
		return response;
	    }
	    tryCounter++;
	}
	tryCounter = 0;
	Message response( PLACE_SHIP, 0, 0, shipName, dir, length );

    }


    // parameters = mesg type (PLACE_SHIP), row, col, a string, direction (Horizontal/Vertical)
    while (tryCounter <= 25) {
      	if (dir == Horizontal) {
          	    row = rand()%boardSize;
          	    col = rand()%((boardSize-length) + 1);
      	}

      	if (dir == Vertical) {
      	    col = rand()%boardSize;
      	    row = rand()%((boardSize-length) + 1);
      	}

      	if (isSafeMove(row, col, length, dir)) {
      	    markShip(row, col, length, dir);
      	    Message response( PLACE_SHIP, row, col, shipName, dir, length );
      	    return response;
        }
	tryCounter++;
    }
    tryCounter = 0;
    if (dir == Horizontal) {
	dir = Vertical;
    }
    else {
	dir = Horizontal;
    }
    while (tryCounter <= 25) {
      	if (dir == Horizontal) {
          	    row = rand()%boardSize;
          	    col = rand()%((boardSize-length) + 1);
      	}

      	if (dir == Vertical) {
      	    col = rand()%boardSize;
      	    row = rand()%((boardSize-length) + 1);
      	}

      	if (isSafeMove(row, col, length, dir)) {
      	    markShip(row, col, length, dir);
      	    Message response( PLACE_SHIP, row, col, shipName, dir, length );
      	    return response;
        }
	tryCounter++;
    }
    
    Message response( PLACE_SHIP, 0, 0, shipName, dir, length );
    
}

bool BattleBoi::isSafeMove(int row, int col, int length, Direction dir) {
    if (dir == Horizontal) {
        for (int c = col; c < (col + length); ++c){
            if (board[row][c]==SHIP){
                return false;
            }

        }
    }
    else if (dir == Vertical) {
        for(int r = row; r < (row + length) ;r++){
            if(board[r][col]==SHIP){
                return false;
            }

        }
    }
    return true;
}

void BattleBoi::markShip(int row, int col, int length, Direction dir) {
    if (dir == Horizontal) {
	for (int c = col; c <= (boardSize - 1); c++) {
	    board[row][c] = SHIP;
	}
    }
    else {
	for (int r = row; r <= (boardSize - 1); r++) {
	    board[r][col] = SHIP;
	}
    }
}

/**
 * @brief Updates the AI with the results of its shots and where the opponent is shooting.
 * @param msg Message specifying what happened + row/col as appropriate.
 */
void BattleBoi::update(Message msg) {
    switch(msg.getMessageType()) {
	case HIT:
	case KILL:
	case MISS:
	    board[msg.getRow()][msg.getCol()] = msg.getMessageType();
	    break;
	case WIN:
	    break;
	case LOSE:
	    break;
	case TIE:
	    break;
	case OPPONENT_SHOT:
	    // TODO: get rid of the cout, but replace in your AI with code that does something
	    // useful with the information about where the opponent is shooting.
	    //cout << gotoRowCol(20, 30) << "BattleBoi: opponent shot at "<< msg.getRow() << ", " << msg.getCol() << flush;
	    break;
    }
}
