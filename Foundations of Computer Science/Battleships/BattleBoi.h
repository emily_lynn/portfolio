/**
 * @author Stefan Brandle, Jonathan Geisler
 * @date September, 2004
 *
 * Please type in your name[s] here:
 *Emily Lynn and Elizabeth Vandergriff
 */

#ifndef BattleBoi_H		// Double inclusion protection
#define BattleBoi_H

using namespace std;

#include "PlayerV2.h"
#include "Message.h"
#include "defines.h"

// DumbPlayer inherits from/extends PlayerV2

class BattleBoi: public PlayerV2 {
    public:
	BattleBoi( int boardSize );
	~BattleBoi();
	void newRound();
	Message placeShip(int length);
	Message getMove();
	void update(Message msg);
	bool isSafeMove( int row, int col, int length, Direction dir );
	void markShip( int row, int col, int length, Direction dir );
    bool searchNorth();
    bool searchSouth();
    bool searchEast();
    bool searchWest();
    void hunter();
    void makeShotArray();

    private:
	void initializeBoard();
        int lastRow;
        int lastCol;
        int hitRow;
        int hitCol;
        int hunting;
        int shotIndex;
        int numShipsPlaced;
        char board[MAX_BOARD_SIZE][MAX_BOARD_SIZE];
        int rowShotArray[100];
        int colShotArray[100];
        int functionChoice; //for randomly choosing between functions to dodge learning
	Direction dir;
};

#endif
