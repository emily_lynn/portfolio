/**
 * @file Board.cpp
 * @brief Board class implementation (also called 'definition') file.
 * @author Stefan Brandle
 * @author Emily Lynn
 * @author Elizabeth Vandergriff
 * @date November 2017
 */

#include <iostream>
#include <cstdlib>
#include <unistd.h>

// When writing a class implementation file, you must "#include" the class
// declaration file.
#include "Board.h"


// We also use the conio namespace contents, so must "#include" the conio declarations.
#include "conio.h"

/**
 * @brief The Board class constructor, responsible for intializing a Board object.
 * The Board constructor is responsible for initializing all the Board object variables.
 *
 * @param rows The number of rows to make the board.
 * @param cols The number of columns to make the board.
 * @param numberOfHumans The number of humans to place on the board.
 */
Board::Board(int rows, int cols, int numberOfHumans) {
    numHumans = numberOfHumans;
    numRows = rows;
    numCols = cols;
    currentTime = 0;
    uSleepTime = 250000;
    numInfected=0;
    numPoisoned=0;
    numApples = 7;
}

/**
 * @brief The Board class destructor.
 * The Board destructor is responsible for any last-minute cleaning
 * up that a Board object needs to do before being destroyed. In this case,
 * it needs to return all the memory borrowed for creating the Human objects.
 */
Board::~Board() {
    for(int pos=0; pos<numHumans; ++pos) {
	delete humans[pos];
    }
    for(int pos=0; pos<numApples; ++pos) {
	delete apples[pos];
    }
}

/**
 * @brief function that runs the simulation
 * Creates human objects, infects one human, then runs simulation until all are infected.
 */
void Board::run() {
    int row, col;

    // Creates 'Human' objects and sets the array pointers to point at them.
    for(int pos=0; pos<numHumans; ++pos) {

	row = pos%numRows;       // row will be in range(0, numRows-1)
	col = random()%numCols;  // col will be in range(0, numCols-1)
	// Create and initialize another Human.
	// Parameters are  row on board, col on board, initialy infected,
	// and a pointer to this board object ('this').
	humans[pos] = new Human(row, col, false, this);
    }
    // Check position of human and infect a random human in the range 0 to numHumans-1 that if farther than 4 spaces from a poison apple
    int randHuman=random()%numHumans;
    while(closeToApple(humans[randHuman])){
        randHuman=random()%numHumans;
    }
    humans[randHuman]->setInfected();

    placeApples(); //Place the zombie killer apples

    for(currentTime=0; allInfected() == false; ++currentTime) {
	// Clear screen before every new time unit
	cout << conio::clrscr() << flush;

	// Tell each human to try moving
	for(int pos=0; pos<numHumans; ++pos) {
	    humans[pos]->move(humans[pos]);
	}

	// Deal with infection propagation.
	processInfection();

	// Tell each human to draw itself on board with updated infection status
	for(int pos=0; pos<numHumans; ++pos) {
	    humans[pos]->draw();
	}

	// Tell each apple to draw itself on board
  	for(int pos=0; pos<numApples; ++pos) {
      	    apples[pos]->draw();
  	}


	// Print statistics.
	cout << conio::gotoRowCol(numRows+3, 1)
	     << "Time=" << currentTime
	     << " Total humans=" << numHumans
	     << " Total infected=" << numInfected
             << " Zombies dead=" << numPoisoned << flush;

	// Sleep specified microseconds
	usleep(uSleepTime);
    }

    // Position the cursor so prompt shows up on its own line
    cout << endl;
}

/**
 * @brief Determines whether or not all humans are infected.
 * @return If even one human is uninfected, returns false. Otherwise, returns true.
 */
bool Board::allInfected() {
    for(int i=0; i<numHumans; ++i) {
	if(humans[i]->isInfected() == false) return false;
    }

    return true;
}

/**
 * @brief The function that handles one infection cycle to determine what new infections
 *        are present.
 * For each pair of adjacent humans in the simulation, processInfection() makes sure that if
 * one is infected, the other becomes infected as well.
 */
void Board::processInfection() {
    for( int i=0; i<numHumans; ++i ) {
        for( int j=i+1; j<numHumans; ++j ) {
	    if( isNextTo(humans[i], humans[j]) ){
	        if( humans[i]->isInfected() && humans[j]->isInfected()==false && humans[i]->isPoisoned()==false) {
		    humans[j]->setInfected();
		} else if ( humans[j]->isInfected() && humans[i]->isInfected()==false && humans[j]->isPoisoned()==false) {
		    humans[i]->setInfected();
		}
	    }
	}
    }

    // Reset the board 'numInfected' count and recount how many are infected.
    numInfected = 0;
    for( int i=0; i<numHumans; ++i ) {
        if( humans[i]->isInfected() ) numInfected++;
    }

    //Determine whether an infected human is on an apple and set it to poisoned if it is
    for( int i=0; i<numHumans; ++i ) {
        for( int j=0; j<numApples; ++j ) {
       	    if(touchesApple(humans[i], apples[j])) {
                if( humans[i]->isInfected() ){
            	    humans[i]->setPoisoned();
                }
            }
      	}
    }
    // Reset the board 'numPoisoned' count and recount how many are poisoned.
    numPoisoned=0;
    for( int i=0; i<numHumans; ++i ) {
        if( humans[i]->isPoisoned() ) numPoisoned++;
    }
}

/**
 * @brief The function that determines whether a particular move can happen.
 *        If the move would go off the board, or land on the same position as another
 *        human, the function returns false (do not move). Otherwise, it returns true (ok to proceed).
 * @param[in] row the row the human wishes to move to.
 * @param[in] col the col the human wishes to move to.
 * @return Whether the human calling this function may move to the specified row and column.
 */
bool Board::tryMove(int row, int col) {
    int tryRow, tryCol=-1;

    // If off board, the move is not permitted
    if( row<0 || row>=numRows || col<0 || col>=numCols ) return false;

    // Else if another human is on the same location, the move is not permitted
    for(int i=0; i<numHumans; ++i) {
        humans[i]->getLocation(tryRow, tryCol);
	if( row==tryRow && col==tryCol ) return false;
    }

    // No problems, so the move is permitted
    return true;
}


/**
 * @brief The function that determines whether a particular move can happen.
 *        If the move would go off the board, land on the same position as another
 *        human, or land on an apple, the function returns false (do not move). Otherwise, it returns true (ok to proceed).
 * @param[in] row the row the human wishes to move to.
 * @param[in] col the col the human wishes to move to.
 * @param[in] h1 pointer to a the human object.
 * @return Whether the human calling this function may move to the specified row and column.
 */
bool Board::tryMove(int row, int col, Human* h1) {
    int tryRow, tryCol=-1;

    // If off board, the move is not permitted
    if( row<0 || row>=numRows || col<0 || col>=numCols ) return false;

    // Else if another human is on the same location, the move is not permitted
    for(int i=0; i<numHumans; ++i) {
        humans[i]->getLocation(tryRow, tryCol);
	if( row==tryRow && col==tryCol ) return false;
    }

    // Else if an apple is on that location, the move is not permitted
    for (int i=0; i<numApples; ++i) {
	apples[i]->getLocation(tryRow, tryCol);
	if( row==tryRow && col==tryCol ) return false;
    }

    // Else if the human is poisoned, the move is not permitted
    if( h1->isPoisoned() ) return false;

    // No problems, so the move is permitted
    else {
      return true;
    }
}


/**
 * @brief The function that determines whether two humans are on adjacent squares.
 * @param[in] h1 pointer to first human object.
 * @param[in] h2 pointer to second human object.
 * @return Whether or not h1 and h2 are on adjacent squares.
 */
bool Board::isNextTo(Human *h1, Human* h2) {
    // Get human location information
    int h1Row, h1Col;
    h1->getLocation(h1Row, h1Col);
    int h2Row, h2Col;
    h2->getLocation(h2Row, h2Col);

    // Return whether h1 and h2 are on adjacent squares in any direction
    // (horizontally, vertically, diagonally).
    return abs(h1Row-h2Row)<=1 && abs(h1Col-h2Col)<=1;
}

/**
 * @brief The function that determines whether a human is adjacent to an apple.
 * @param[in] h1 pointer to a human object.
 * @param[in] a1 pointer to poison apple object.
 * @return Whether or not h1 and a1 are on adjacent squares.
 */
bool Board::touchesApple(Human* h1, PoisonApple* a1) {
    int h1Row, h1Col;
    h1->getLocation(h1Row, h1Col);
    int a1Row, a1Col;
    a1->getLocation(a1Row, a1Col);
    
    //Return whether h1 and a1 are on adjacent squares in any direction
    //(horizontally, vertically, diagonally).
    return abs(h1Row-a1Row)<=1 && abs(h1Col-a1Col)<=1;
}



/**
 * @brief The function that determines whether a human is closer than four spots away from an apple.
 * @param[in] h1 pointer to a human object.
 * @return Whether or not h1 and a1 are within four spaces of each other so that the original infected human does not immediately get poisoned
 */
bool Board::closeToApple(Human* h1) {
    bool check=false;
    int h1Row, h1Col;
    h1->getLocation(h1Row, h1Col);
    for(int i;i<numApples;i++){
        PoisonApple* a1=apples[i];
        int a1Row, a1Col;
        a1->getLocation(a1Row, a1Col);
        check = abs(h1Row-a1Row)<=4 && abs(h1Col-a1Col)<=4;
    }

    //returns whether or not h1 and a1 are within four spaces of each other in any direction.
    //(horizontally, vertically, diagonally)
    return check;
}

/**
 * @brief The function that generates zombie killer apples and randomly places them.
 */
void Board::placeApples(){
    int row, col;
    for(int pos=0; pos<numApples; ++pos) {
       bool isPlaced = false;
       while (not isPlaced ) {
         row = pos%numRows+2;       // row will be in range(0, numRows-1)
         col = random()%numCols+2;  // col will be in range(0, numCols-1)
         if(tryMove(row,col)){
             apples[pos] = new PoisonApple(row, col, false, this);
             isPlaced = true;
         }
       }
     }
}
