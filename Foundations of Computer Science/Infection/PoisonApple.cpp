/**
 * @file PoisonApple.cpp
 * @brief Poison Apple class implementation
 * @author Emily Lynn
 * @author Elizabeth Vandegriff
 * @date November 2017 
 */

#include <iostream>
#include <string>
#include "conio.h"
#include "PoisonApple.h"

using namespace std;

/**
 * @brief The Poison Apple class constructor.
 * This function initializes the row, col, infected, and board variables for the apple.
 *
 * @param initRow the poison apple row location.
 * @param initCol the poison apple column location.
 * @param initInfected left over from human class.
 * @param theBoard a pointer to the board (used to ask board whether a proposed move is ok).
 */
PoisonApple::PoisonApple(int initRow, int initCol, bool initInfected, Board* theBoard) : Human(initRow, initCol, initInfected, theBoard) {
    row = initRow;
    col = initCol;
    infected = initInfected;
    board = theBoard;
}
/**
 * @brief Draws the poison apple.
 * Draws the poison apple at the current row/col location on the screen.
 */
void PoisonApple::draw() {
    cout << conio::gotoRowCol(row+1,col+1);
    cout << conio::bgColor(conio::LIGHT_GREEN);
    cout << '*' << conio::resetAll() << flush;
}
