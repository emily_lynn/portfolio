/**
 * @file PoisonApple.cpp
 * @brief Declaration of the PoisonApple class, inherited from the Human class.
 *
 * @author Emily Lynn
 * @author Elizabeth Vandegriff
 * @date November 2017
 */

#include <iostream>
#include <string>

#include "Human.h"
#include "Board.h"

#ifndef POISONAPPLE_H
#define POISONAPPLE_H

using namespace std;

/**
 * @class PoisonApple
 * @brief The PoisonApple class declaration
 */
class PoisonApple : public Human {
	public:
    		PoisonApple(int initRow, int initCol, bool initInfected, Board* thisBoard);
    		void GetEaten();
    		void draw();

	protected:
    		int numApples; // number of apples that are initiated at the beginning
    
	private:
};

#endif // POISONAPPLE_H
