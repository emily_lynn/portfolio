// Your name(s): Emily Lynn and Elizabeth Vandergriff

#ifndef LISTARRAY_CPP
#define LISTARRAY_CPP

#include <iostream>
#include "ListArray.h"

using namespace std;

//template <class DataType>
template <typename DataType> //must put this statement in front of every method 
List<DataType>::List() //must specify placeholder type when initializing object
// Initialize the object's member variables
: maxSize(defaultMaxSize), size(0), cursor(-1) //initializing certain variables in header
{
    // Make the array to put the items in and point dataItems there.
    dataItems = new DataType[maxSize]; //placing pointer to the array address
}

template <typename DataType>
List<DataType>::List( int maxItems ) 
: maxSize(maxItems), size(0), cursor(-1)
{
    dataItems = new DataType[maxSize];
}

template <typename DataType>
List<DataType>::List( const List<DataType>& other ){ 
    // You do: create dataItems array and copy all valid data from the other list
    maxSize = other.maxSize;
    size= other.size;
    cursor= other.cursor;
    dataItems = new DataType[maxSize];
    for (int i = 0; i < other.size; i++) { //use less than size because we only want to copy significant items, not the trash that could be in the rest of the array 
        dataItems[i] = other.dataItems[i];
    }
}

template <typename DataType>
List<DataType>::~List()
{
    // You do: return dynamically allocated memory (destructor)
    delete[] dataItems;
}

template <typename DataType>
List<DataType>& List<DataType>::operator=( const List<DataType>& other ) { //because otherwise it does a shallow copy
    // Handle being asked to assign yourself to yourself
    // Do that by comparing address of other list to your address.
    if (this == &other) return *this; // & gives address

    // You do: delete your dataItems array
    delete[] dataItems; 

    // You do: make a new dataItems array of the correct size
    maxSize = other.maxSize;
    size = other.size;
    cursor = other.cursor;
    dataItems = new DataType[maxSize];

    for (int i = 0; i < size; ++i) {
        dataItems[i] = other.dataItems[i];
    }

    // To get chained assignment working, return a reference to yourself
    // by doing following. Chained assignment is "a = b = c".
    return *this;
}

// Iterators

template <typename DataType>
bool List<DataType>::gotoNext() { //moves cursor to next position and returns True, or sees that next spot is empty and returns false
    if ((cursor == (size-1))||(size == 0)) return false; //size counts characters instead of giving the last index (so is always one ahead of cursor)
    cursor++;
    return true;
}

template <typename DataType>
bool List<DataType>::gotoPrior() {
    if ((cursor == 0) | (cursor == -1)) return false;
    cursor--;
    return true;
}

template <typename DataType>
void List<DataType>::gotoBeginning() { //moves curosr to first index position
    if (cursor == -1) return; //if it is empty
    cursor = 0;
  //if (!isEmpty()) cursor = 0; is another option
}

template <typename DataType>
void List<DataType>::gotoEnd() {
    if (size == 0){
        return;
    }
    cursor = size-1;
}

// Observers

template <typename DataType>
bool List<DataType>::isEmpty() const {
    //or could do "return size == 0;"
    if(size == 0) return true;
    else return false;
}

template <typename DataType>
bool List<DataType>::isFull() const {
    if (size == maxSize) {
        return true;
    }
    else{
        return false;
    }
}

template <typename DataType>
int List<DataType>::getCursorPosition() const { //getter function
    return cursor;
}

template <typename DataType>
int List<DataType>::getSize() const {
    return size;
}

template <typename DataType>
DataType List<DataType>::getCursor() const throw (logic_error) {
    if(isEmpty()) throw logic_error("List is Empty!");
    return dataItems[cursor];
}


template <typename DataType>
DataType List<DataType>::operator[]( int position ) const throw (logic_error) {
    if (isEmpty()) throw logic_error("List is Empty!");
    if ((position < 0)|(position > (size))) throw logic_error("Invalid Postion");
    return dataItems[position];
}

template <typename DataType>
void List<DataType>::showStructure() const                      // Debugging function
// Outputs the data items in a list. if the list is empty, outputs
// "empty list". this operation is intended for testing/debugging
// purposes only.

{
    int j;   // loop counter

    if ( size == 0 )
       cout << "empty list" << endl;
    else
    {
       cout << "size = " << size
            <<  "   cursor = " << cursor << endl;
       for ( j = 0 ; j < maxSize ; j++ )
           cout << j << "\t";
       cout << endl;
       for ( j = 0 ; j < size ; j++ ) {
           if( j == cursor ) {
              cout << "[";
              cout << dataItems[j];
              cout << "]";
              cout << "\t";
           }
           else
              cout << dataItems[j] << "\t";
       }
       cout << endl;
    }
}

// Transformers/mutators
template <typename DataType>
bool List<DataType>::search( DataType value ) {
    for (int i = 0; i < size; i++) {
        if (dataItems[i] == value) {
            return true;
        }
    }
    return false;
}

template <typename DataType>
void List<DataType>::add( const DataType& newItem ) throw (logic_error) {
    if(size == maxSize) throw logic_error("List is Full!");
    for (int i = (size-1); i > cursor; --i) {
        dataItems[i+1] = dataItems[i];
    }
    dataItems[cursor+1] = newItem;
    cursor++;
    size++;
    dataItems[size] = '\0';
}

template <typename DataType>
void List<DataType>::add( const List<DataType>& otherList ) throw (logic_error) {
    if (size == maxSize) throw logic_error("List is Full!");
    if (maxSize < (size + otherList.size)) throw logic_error("List is too Small!");
    for (int i = (size-1); i > cursor; --i) {
        dataItems[i + otherList.size] = dataItems[i];
    }
    for (int i = 1; i < otherList.size; ++i) {
        dataItems[cursor + i] = otherList.dataItems[i-1];
    }
    cursor = cursor + otherList.size;
    size = size + otherList.size;
    dataItems[size] = '\0';
}

template <typename DataType>
void List<DataType>::remove( ) throw ( logic_error) {
    if (size == 0) throw logic_error("Nothing to Remove");
    if (cursor == (size-1)) {
        dataItems[size-1] = '\0';
        cursor = 0;
    }
    else{
        for (int i = cursor; i < size; i++) {
        dataItems[i] = dataItems[i+1];
        }
    }
    size--;
}

template <typename DataType>
void List<DataType>::remove( const DataType& delItem ) {
    for (int i = 0; i < size; i++) {
        if (dataItems[i] == delItem) {
            cursor = i;
            remove();
            return;
        }
   }
}

template <typename DataType>
void List<DataType>::replace( const DataType& replaceItem ) throw (logic_error) {
    if (size == 0) throw logic_error("List is Empty!");
    dataItems[cursor] = replaceItem;
}

template <typename DataType>
void List<DataType>::clear() {
    // Don't throw away the array. Just declare it to be empty.
    cursor = -1;
    size = 0;
}

#endif     // #ifndef LIST_CPP
