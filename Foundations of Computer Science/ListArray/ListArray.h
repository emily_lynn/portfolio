#ifndef LISTARRAY_H
#define LISTARRAY_H

#include <stdexcept>

using namespace std;

template <typename DataType> //name of the type of data it holds(so can hold any of several types)
class List {
  public:
    static const int defaultMaxSize = 8; //way to make a const in a class

    // Constructors, destructor, and related
    List();
    List(int maxItems);
    List(const List& other); //given existing List, make yourself look like that list
    virtual ~List(); //means may be overriden in a derived class
    List& operator=(const List& other);

    // Iterators
    bool gotoNext();
    bool gotoPrior();
    void gotoBeginning();
    void gotoEnd();

    // Observers
    bool isEmpty() const; //const meanst that it does not change the object
    bool isFull() const;
    int getCursorPosition() const;
    int getSize() const;
    DataType getCursor() const throw (logic_error); //asking for item at the cursor. Also may throw an error (such as if you ask this when the list is empty
    DataType operator[]( int position ) const throw (logic_error);
    void showStructure() const;		// Debugging function

    // Transformers/mutators
    bool search(DataType value);
    void add( const DataType& newItem ) throw (logic_error);
    void add( const List& otherList ) throw (logic_error);
    void remove() throw (logic_error);
    void remove( const DataType& delItem );
    void replace( const DataType& replaceItem ) throw (logic_error);
    void clear();

  protected:
    // Data members
    int maxSize,		// Array size => maximum items in list
        size,			// Current number of items in list
	cursor;			// Current location of cursor
    DataType *dataItems;	// Pointer to array holding list items
};

#endif
