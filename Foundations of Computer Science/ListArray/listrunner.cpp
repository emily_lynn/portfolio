/* Generated file, do not edit */

#ifndef CXXTEST_RUNNING
#define CXXTEST_RUNNING
#endif

#define _CXXTEST_HAVE_STD
#define _CXXTEST_HAVE_EH
#include <cxxtest/TestListener.h>
#include <cxxtest/TestTracker.h>
#include <cxxtest/TestRunner.h>
#include <cxxtest/RealDescriptions.h>
#include <cxxtest/ErrorPrinter.h>

int main() {
 return CxxTest::ErrorPrinter().run();
}
#include "__instructor_listarray_test_full.h"

static textTest suite_textTest;

static CxxTest::List Tests_textTest = { 0, 0 };
CxxTest::StaticSuiteDescription suiteDescription_textTest( "__instructor_listarray_test_full.h", 11, "textTest", suite_textTest, Tests_textTest );

static class TestDescription_textTest_testDefaultConstructor1 : public CxxTest::RealTestDescription {
public:
 TestDescription_textTest_testDefaultConstructor1() : CxxTest::RealTestDescription( Tests_textTest, suiteDescription_textTest, 15, "testDefaultConstructor1" ) {}
 void runTest() { suite_textTest.testDefaultConstructor1(); }
} testDescription_textTest_testDefaultConstructor1;

static class TestDescription_textTest_testParameterizedConstructor1 : public CxxTest::RealTestDescription {
public:
 TestDescription_textTest_testParameterizedConstructor1() : CxxTest::RealTestDescription( Tests_textTest, suiteDescription_textTest, 27, "testParameterizedConstructor1" ) {}
 void runTest() { suite_textTest.testParameterizedConstructor1(); }
} testDescription_textTest_testParameterizedConstructor1;

static class TestDescription_textTest_testParameterizedConstructor2 : public CxxTest::RealTestDescription {
public:
 TestDescription_textTest_testParameterizedConstructor2() : CxxTest::RealTestDescription( Tests_textTest, suiteDescription_textTest, 37, "testParameterizedConstructor2" ) {}
 void runTest() { suite_textTest.testParameterizedConstructor2(); }
} testDescription_textTest_testParameterizedConstructor2;

static class TestDescription_textTest_testCopyConstructor1 : public CxxTest::RealTestDescription {
public:
 TestDescription_textTest_testCopyConstructor1() : CxxTest::RealTestDescription( Tests_textTest, suiteDescription_textTest, 47, "testCopyConstructor1" ) {}
 void runTest() { suite_textTest.testCopyConstructor1(); }
} testDescription_textTest_testCopyConstructor1;

static class TestDescription_textTest_testAssignmentOperator : public CxxTest::RealTestDescription {
public:
 TestDescription_textTest_testAssignmentOperator() : CxxTest::RealTestDescription( Tests_textTest, suiteDescription_textTest, 70, "testAssignmentOperator" ) {}
 void runTest() { suite_textTest.testAssignmentOperator(); }
} testDescription_textTest_testAssignmentOperator;

static class TestDescription_textTest_testaddOneItem : public CxxTest::RealTestDescription {
public:
 TestDescription_textTest_testaddOneItem() : CxxTest::RealTestDescription( Tests_textTest, suiteDescription_textTest, 93, "testaddOneItem" ) {}
 void runTest() { suite_textTest.testaddOneItem(); }
} testDescription_textTest_testaddOneItem;

static class TestDescription_textTest_testaddThreeItems : public CxxTest::RealTestDescription {
public:
 TestDescription_textTest_testaddThreeItems() : CxxTest::RealTestDescription( Tests_textTest, suiteDescription_textTest, 110, "testaddThreeItems" ) {}
 void runTest() { suite_textTest.testaddThreeItems(); }
} testDescription_textTest_testaddThreeItems;

static class TestDescription_textTest_testaddTooManyItems : public CxxTest::RealTestDescription {
public:
 TestDescription_textTest_testaddTooManyItems() : CxxTest::RealTestDescription( Tests_textTest, suiteDescription_textTest, 131, "testaddTooManyItems" ) {}
 void runTest() { suite_textTest.testaddTooManyItems(); }
} testDescription_textTest_testaddTooManyItems;

static class TestDescription_textTest_testRemoveFirstItem : public CxxTest::RealTestDescription {
public:
 TestDescription_textTest_testRemoveFirstItem() : CxxTest::RealTestDescription( Tests_textTest, suiteDescription_textTest, 157, "testRemoveFirstItem" ) {}
 void runTest() { suite_textTest.testRemoveFirstItem(); }
} testDescription_textTest_testRemoveFirstItem;

static class TestDescription_textTest_testRemoveMiddleItem : public CxxTest::RealTestDescription {
public:
 TestDescription_textTest_testRemoveMiddleItem() : CxxTest::RealTestDescription( Tests_textTest, suiteDescription_textTest, 179, "testRemoveMiddleItem" ) {}
 void runTest() { suite_textTest.testRemoveMiddleItem(); }
} testDescription_textTest_testRemoveMiddleItem;

static class TestDescription_textTest_testRemoveLastItem : public CxxTest::RealTestDescription {
public:
 TestDescription_textTest_testRemoveLastItem() : CxxTest::RealTestDescription( Tests_textTest, suiteDescription_textTest, 202, "testRemoveLastItem" ) {}
 void runTest() { suite_textTest.testRemoveLastItem(); }
} testDescription_textTest_testRemoveLastItem;

static class TestDescription_textTest_testRemoveFromEmptyList : public CxxTest::RealTestDescription {
public:
 TestDescription_textTest_testRemoveFromEmptyList() : CxxTest::RealTestDescription( Tests_textTest, suiteDescription_textTest, 223, "testRemoveFromEmptyList" ) {}
 void runTest() { suite_textTest.testRemoveFromEmptyList(); }
} testDescription_textTest_testRemoveFromEmptyList;

static class TestDescription_textTest_testReplaceFirstItem : public CxxTest::RealTestDescription {
public:
 TestDescription_textTest_testReplaceFirstItem() : CxxTest::RealTestDescription( Tests_textTest, suiteDescription_textTest, 237, "testReplaceFirstItem" ) {}
 void runTest() { suite_textTest.testReplaceFirstItem(); }
} testDescription_textTest_testReplaceFirstItem;

static class TestDescription_textTest_testReplaceMiddleItem : public CxxTest::RealTestDescription {
public:
 TestDescription_textTest_testReplaceMiddleItem() : CxxTest::RealTestDescription( Tests_textTest, suiteDescription_textTest, 260, "testReplaceMiddleItem" ) {}
 void runTest() { suite_textTest.testReplaceMiddleItem(); }
} testDescription_textTest_testReplaceMiddleItem;

static class TestDescription_textTest_testReplaceLastItem : public CxxTest::RealTestDescription {
public:
 TestDescription_textTest_testReplaceLastItem() : CxxTest::RealTestDescription( Tests_textTest, suiteDescription_textTest, 284, "testReplaceLastItem" ) {}
 void runTest() { suite_textTest.testReplaceLastItem(); }
} testDescription_textTest_testReplaceLastItem;

static class TestDescription_textTest_testReplaceFromEmptyList : public CxxTest::RealTestDescription {
public:
 TestDescription_textTest_testReplaceFromEmptyList() : CxxTest::RealTestDescription( Tests_textTest, suiteDescription_textTest, 306, "testReplaceFromEmptyList" ) {}
 void runTest() { suite_textTest.testReplaceFromEmptyList(); }
} testDescription_textTest_testReplaceFromEmptyList;

static class TestDescription_textTest_testClearEmptyList : public CxxTest::RealTestDescription {
public:
 TestDescription_textTest_testClearEmptyList() : CxxTest::RealTestDescription( Tests_textTest, suiteDescription_textTest, 320, "testClearEmptyList" ) {}
 void runTest() { suite_textTest.testClearEmptyList(); }
} testDescription_textTest_testClearEmptyList;

static class TestDescription_textTest_testClearPartlyFullList : public CxxTest::RealTestDescription {
public:
 TestDescription_textTest_testClearPartlyFullList() : CxxTest::RealTestDescription( Tests_textTest, suiteDescription_textTest, 334, "testClearPartlyFullList" ) {}
 void runTest() { suite_textTest.testClearPartlyFullList(); }
} testDescription_textTest_testClearPartlyFullList;

static class TestDescription_textTest_testClearFullList : public CxxTest::RealTestDescription {
public:
 TestDescription_textTest_testClearFullList() : CxxTest::RealTestDescription( Tests_textTest, suiteDescription_textTest, 350, "testClearFullList" ) {}
 void runTest() { suite_textTest.testClearFullList(); }
} testDescription_textTest_testClearFullList;

static class TestDescription_textTest_testIsEmptyInEmptyList : public CxxTest::RealTestDescription {
public:
 TestDescription_textTest_testIsEmptyInEmptyList() : CxxTest::RealTestDescription( Tests_textTest, suiteDescription_textTest, 366, "testIsEmptyInEmptyList" ) {}
 void runTest() { suite_textTest.testIsEmptyInEmptyList(); }
} testDescription_textTest_testIsEmptyInEmptyList;

static class TestDescription_textTest_testIsEmptyInNonEmptyList : public CxxTest::RealTestDescription {
public:
 TestDescription_textTest_testIsEmptyInNonEmptyList() : CxxTest::RealTestDescription( Tests_textTest, suiteDescription_textTest, 374, "testIsEmptyInNonEmptyList" ) {}
 void runTest() { suite_textTest.testIsEmptyInNonEmptyList(); }
} testDescription_textTest_testIsEmptyInNonEmptyList;

static class TestDescription_textTest_testIsEmptyInFullList : public CxxTest::RealTestDescription {
public:
 TestDescription_textTest_testIsEmptyInFullList() : CxxTest::RealTestDescription( Tests_textTest, suiteDescription_textTest, 383, "testIsEmptyInFullList" ) {}
 void runTest() { suite_textTest.testIsEmptyInFullList(); }
} testDescription_textTest_testIsEmptyInFullList;

static class TestDescription_textTest_testIsFullInEmptyList : public CxxTest::RealTestDescription {
public:
 TestDescription_textTest_testIsFullInEmptyList() : CxxTest::RealTestDescription( Tests_textTest, suiteDescription_textTest, 393, "testIsFullInEmptyList" ) {}
 void runTest() { suite_textTest.testIsFullInEmptyList(); }
} testDescription_textTest_testIsFullInEmptyList;

static class TestDescription_textTest_testIsFullInNonEmptyList : public CxxTest::RealTestDescription {
public:
 TestDescription_textTest_testIsFullInNonEmptyList() : CxxTest::RealTestDescription( Tests_textTest, suiteDescription_textTest, 401, "testIsFullInNonEmptyList" ) {}
 void runTest() { suite_textTest.testIsFullInNonEmptyList(); }
} testDescription_textTest_testIsFullInNonEmptyList;

static class TestDescription_textTest_testIsFullInFullList : public CxxTest::RealTestDescription {
public:
 TestDescription_textTest_testIsFullInFullList() : CxxTest::RealTestDescription( Tests_textTest, suiteDescription_textTest, 410, "testIsFullInFullList" ) {}
 void runTest() { suite_textTest.testIsFullInFullList(); }
} testDescription_textTest_testIsFullInFullList;

static class TestDescription_textTest_testGoToBeginningInEmptyList : public CxxTest::RealTestDescription {
public:
 TestDescription_textTest_testGoToBeginningInEmptyList() : CxxTest::RealTestDescription( Tests_textTest, suiteDescription_textTest, 420, "testGoToBeginningInEmptyList" ) {}
 void runTest() { suite_textTest.testGoToBeginningInEmptyList(); }
} testDescription_textTest_testGoToBeginningInEmptyList;

static class TestDescription_textTest_testGoToBeginningInOneItemList : public CxxTest::RealTestDescription {
public:
 TestDescription_textTest_testGoToBeginningInOneItemList() : CxxTest::RealTestDescription( Tests_textTest, suiteDescription_textTest, 435, "testGoToBeginningInOneItemList" ) {}
 void runTest() { suite_textTest.testGoToBeginningInOneItemList(); }
} testDescription_textTest_testGoToBeginningInOneItemList;

static class TestDescription_textTest_testGoToBeginningInMultiItemList : public CxxTest::RealTestDescription {
public:
 TestDescription_textTest_testGoToBeginningInMultiItemList() : CxxTest::RealTestDescription( Tests_textTest, suiteDescription_textTest, 451, "testGoToBeginningInMultiItemList" ) {}
 void runTest() { suite_textTest.testGoToBeginningInMultiItemList(); }
} testDescription_textTest_testGoToBeginningInMultiItemList;

static class TestDescription_textTest_testGoToEndInEmptyList : public CxxTest::RealTestDescription {
public:
 TestDescription_textTest_testGoToEndInEmptyList() : CxxTest::RealTestDescription( Tests_textTest, suiteDescription_textTest, 469, "testGoToEndInEmptyList" ) {}
 void runTest() { suite_textTest.testGoToEndInEmptyList(); }
} testDescription_textTest_testGoToEndInEmptyList;

static class TestDescription_textTest_testGoToNextInEmptyList : public CxxTest::RealTestDescription {
public:
 TestDescription_textTest_testGoToNextInEmptyList() : CxxTest::RealTestDescription( Tests_textTest, suiteDescription_textTest, 519, "testGoToNextInEmptyList" ) {}
 void runTest() { suite_textTest.testGoToNextInEmptyList(); }
} testDescription_textTest_testGoToNextInEmptyList;

static class TestDescription_textTest_testGoToPriorInEmptyList : public CxxTest::RealTestDescription {
public:
 TestDescription_textTest_testGoToPriorInEmptyList() : CxxTest::RealTestDescription( Tests_textTest, suiteDescription_textTest, 579, "testGoToPriorInEmptyList" ) {}
 void runTest() { suite_textTest.testGoToPriorInEmptyList(); }
} testDescription_textTest_testGoToPriorInEmptyList;

static class TestDescription_textTest_testgetCursorInEmptyList : public CxxTest::RealTestDescription {
public:
 TestDescription_textTest_testgetCursorInEmptyList() : CxxTest::RealTestDescription( Tests_textTest, suiteDescription_textTest, 638, "testgetCursorInEmptyList" ) {}
 void runTest() { suite_textTest.testgetCursorInEmptyList(); }
} testDescription_textTest_testgetCursorInEmptyList;

#include <cxxtest/Root.cpp>
