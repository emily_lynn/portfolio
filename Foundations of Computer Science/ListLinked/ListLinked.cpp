//Emily Lynn and Elizabeth Vandegriff



#ifndef LISTLINKED_CPP
#define LISTLINKED_CPP

#include <iostream>
#include <stdexcept>

#include "ListLinked.h"

using namespace std;

template <typename DataType>
List<DataType>::ListNode::ListNode( DataType newData, ListNode* next ) { //outer class, then inner class
    dataItem = newData;
    this -> next = next; //can't just do next=next because sets parameter next equal to itself
    //means the next in this object equals the next parameter
}


template <typename DataType>
List<DataType>::List() { //initializing a new object List
    head = 0;
    cursor = 0; //cursor is a pointer so it can't equal -1 (remember can't reference a pointer to 0)
}

template <typename DataType>
List<DataType>::List(int /* maxItems */) { //means that you know you are receiving an int that you don't care about so don't even give it a name. We just do this to make the linked list public interface the same as the List Array public interface. We don't need to make the size prematurely because we build the list as we go with linked lists
    head = 0;
    cursor = 0;
}

template <typename DataType>
List<DataType>::List(const List& other) {
    head = 0;
    cursor = 0;
    operator=(other);
}

template <typename DataType>
List<DataType>::~List() { //destructor
    clear(); //within object have a function called clear, and call it. 

}
//done

template <typename DataType>
List<DataType>& List<DataType>::operator=(const List& other) {
    if(this==&other) return *this;
    
    clear();
    
    ListNode *temp = other.head;
    ListNode *thumb = other.head;
    while(temp != 0){
        add(temp -> dataItem);
        if(temp == other.cursor){
            cursor = thumb;
        }
        temp = temp -> next;
    }


    return *this;
}


// Iterators
template <typename DataType>
bool List<DataType>::gotoNext() {
    if(isEmpty()){
        return false;
    }
    if(cursor == 0){
        return false;
    }
    while(cursor -> next != 0){
        cursor = cursor -> next;
        return true;
    }
}

template <typename DataType>
bool List<DataType>::gotoPrior() {
    if(isEmpty()){
        return false;
    }
    //fix
    if(cursor == head){
        return false;
    }
    ListNode *save = head;
    while(save -> next != cursor && cursor != head){
        save = save -> next;
    }
    cursor = save;
    return true;


}

template <typename DataType>
void List<DataType>::gotoBeginning() {
    if(isEmpty()){
        return;
    }
    cursor = head;

}

template <typename DataType>
void List<DataType>::gotoEnd() {
    if (isEmpty()) {
        return;
    }
    while(cursor -> next != 0) {
        cursor = cursor -> next;
    }
    return;

}


// Observers
template <typename DataType>
bool List<DataType>::isEmpty() const {

    return head==0;
}

template <typename DataType>
bool List<DataType>::isFull() const {

    return false;
}

template <typename DataType>
int List<DataType>::getCursorPosition() const {
    if(isEmpty()){
        return -1;
    }
    if(cursor == head){
        return 0;
    }
    ListNode *save = head;
    int index = 0;
    while(save != cursor){
        save = save -> next;
        index++;
    }
    delete save;
    return index;

}

template <typename DataType>
int List<DataType>::getSize() const {
    if(isEmpty()){
        return 0;
    }
    ListNode *save = head;
    int index = 0;
    while(save -> next != 0){
        save = save -> next;
        index++;
    }
    return index+1;
    //return index+1;

}

template <typename DataType>
DataType List<DataType>::getCursor() const throw (logic_error) {
    if(isFull()){
        throw logic_error("List is Full!");
    }
    return cursor -> dataItem;
}

template <typename DataType>
DataType List<DataType>::operator[]( int position ) const throw (logic_error) {

}

template <typename DataType>
void List<DataType>::showStructure() const {

// Outputs the items in a list. If the list is empty, outputs
// "Empty list". This operation is intended for testing and
// debugging purposes only.

    if ( isEmpty() )
    {
       cout << "Empty list" << endl;
    } 
    else
    {
	for (ListNode* temp = head; temp != 0; temp = temp->next) {
	    if (temp == cursor) {
		cout << "[";
	    }

	    // Assumes that dataItem can be printed via << because
	    // is is either primitive or operator<< is overloaded.
	    cout << temp->dataItem;	

	    if (temp == cursor) {
		cout << "]";
	    }
	    cout << " ";
	}
	cout << endl;
    }
}
		// Debugging function

// Transformers/mutators
template <typename DataType>
bool List<DataType>::search(DataType value) {
//ToDo:
    if(isEmpty()){
        return false;
    }
    while(cursor -> next != 0){
        cursor = cursor -> next;

    }
    return true;

}

template <typename DataType>
void List<DataType>::add( const DataType& newItem ) throw (logic_error) {
//ToDo:
    if(isFull()){
        throw logic_error("List is Full!");
    }
    if(isEmpty()){
        ListNode *temp = new ListNode(newItem, 0);
        head = temp;
        cursor = head;
    }
    else{
        ListNode *temp = new ListNode(newItem, cursor -> next);
        cursor -> next = temp;
        cursor = cursor -> next;
    }
    return;

}

template <typename DataType>
void List<DataType>::add( const List& otherList ) throw (logic_error) {
    
}

template <typename DataType>
void List<DataType>::remove() throw (logic_error) {

    if(isEmpty()){
        throw logic_error("List is Empty!");
    }
    else if(head == cursor){
        head = head -> next;
        delete cursor;
        cursor = head;
    }
    else if(cursor -> next == 0){
        gotoPrior();
        delete cursor -> next;
        cursor -> next = 0;
        cursor = head;
    }
    else{
        ListNode *killMe = cursor -> next;
        cursor -> dataItem = killMe -> dataItem;
        cursor -> next = killMe -> next;
        delete killMe;
    }
}

template <typename DataType>
void List<DataType>::remove( const DataType& delItem ) {
    if(head == 0){
        return;
    }
    else if(head == cursor){
        head = head -> next;
        delete cursor;
        cursor = head;
    }
    else if(cursor -> next == 0){
        gotoPrior();
        delete cursor -> next;
        cursor -> next = 0;
        cursor = head;
    }
    else{
        ListNode *killMe = cursor -> next;
        cursor -> dataItem = killMe -> dataItem;
        cursor -> next = killMe -> next;
        delete killMe;
    }
}

template <typename DataType>
void List<DataType>::replace( const DataType& replaceItem ) throw (logic_error) {
    if(isEmpty()) throw logic_error("List is Empty!");
    cursor -> dataItem = replaceItem;

}

template <typename DataType>
void List<DataType>::clear() {
    if(isEmpty()){
        return;
    }
    ListNode *currentNode = head;
    cursor = head;
    while(cursor != 0){
         currentNode = cursor;
         cursor = cursor -> next;
         delete currentNode;
    }
    head = 0;
    cursor = 0;   
}



#endif
