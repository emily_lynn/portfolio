//------------------------------------------------------------------------
// Pseudo-code for the maze solver problem. You have permission to use any
// of the code below, or to totally ignore it. -sb
//---------------------------------------------------------------------


#include <iostream>
#include <string>
#include <fstream>
#include <unistd.h>

#include "conio.h" 
#include "conio.cpp"

using namespace std;
using namespace conio;

void loadMatrix(char masterMatrix[10][10]);
void printMaze(char masterMatrix[10][10]);
void getStart(int& startRow, int& startCol);
bool canEscape(int row, int col, char masterMatrix[10][10]);

const char BEEN_THERE = '-';
string mazeText;

void loadMatrix(char masterMatrix[10][10], string mazeText) {
    int l = 0;
    int c = 0;
    char nextChar;
    string mazeTextString;
    ifstream mazeFile;
    mazeFile.open(mazeText.c_str());
    if (mazeFile.is_open()){
         for (l = 0; l < 10; ++l) {
             for (c = 0; c < 10; ++c) {
                 mazeFile >> nextChar;
                 masterMatrix[l][c] = nextChar;
             }
         }
    }
    printMaze(masterMatrix);
}

void printMaze(char masterMatrix[10][10]) {
    cout << clrscr() << flush;
    int l = 0;
    int c = 0;
    cout << gotoRowCol(1,1) << flush;
    cout << "  0123456789" << endl << endl;
    for (l = 0; l < 10; ++l) {
        cout << l << "  ";
        for (c = 0; c < 10; ++c) {
            cout << conio::gotoRowCol(l+3,c+3) << masterMatrix[l][c];
        }
        cout << endl;
    }
    cout << endl;
}


void getStart(int& startRow, int& startCol) {
    cout << "Starting Row? --> ";
    cin >> startRow;
    cout << "Starting Column? --> ";
    cin >> startCol;
}

bool canEscape(int row, int col, char masterMatrix[10][10]){
    if ( (row > 9) || (col > 9) || (row < 0) || (col < 0)) {
        return false;
    }
    else if (masterMatrix[row][col] == '#') {
        return false;
    } 
    else if (masterMatrix[row][col] == 'E') {
        cout << bgColor(GREEN) << gotoRowCol(row+3, col+3) << masterMatrix[row][col] << '\b' << flush;
        usleep(150000);
        return true;
    }
    else if (masterMatrix[row][col] == BEEN_THERE){
        return false;
    }
    
    masterMatrix[row][col] = BEEN_THERE;
    cout << bgColor(YELLOW) << conio::gotoRowCol(row+3, col+3) << masterMatrix[row][col] << '\b' << flush;  //flush means forces to send whatever is in the output buffer
    usleep(250000);
//fgColor( Color c); color can be RED, GREEN, YELLOW

    if (canEscape(row, col + 1, masterMatrix) || canEscape(row + 1, col, masterMatrix) ||
        canEscape(row, col - 1, masterMatrix) || canEscape(row - 1, col, masterMatrix)) 
    {
        cout << bgColor(GREEN) << gotoRowCol(row+3, col+3) << masterMatrix[row][col] << bgColor(BLACK) << '\b' << flush;
        usleep(150000);
        return true;
    }
    else {
        cout << bgColor(RED) << gotoRowCol(row+3, col+3) << masterMatrix[row][col] << bgColor(BLACK) << '\b' <<flush;
        usleep(250000);
        return false; //all directions don't work
    }

}

int main()
{
     int startRow, startCol;

     char masterMatrix[10][10];
     char matrix[10][10];

     cout << bgColor(BLACK) << fgColor(WHITE) << flush;
     cout << "Maze Text File? --> " << flush;
     cin >> mazeText;
     loadMatrix(masterMatrix, mazeText);

     getStart(startRow, startCol);
     while (cin) {
         if (canEscape(startRow, startCol, masterMatrix)) {
             cout << bgColor(BLACK) << fgColor(WHITE) << gotoRowCol(19, 1) << "You can escape!!" << endl;
         }
         else {
             cout << bgColor(BLACK) << fgColor(WHITE) << gotoRowCol(19,1) <<  "You're Trapped!!" << endl;
         }
         usleep(3000000);
         cout << fgColor(WHITE) << flush;
         loadMatrix(masterMatrix, mazeText);
         getStart(startRow, startCol);
     }

     return 0;
}

