/*Nqueens.cpp -- a program that receives a certain number of queens 
 *and places board of the same square size in such a way as that no queen
 *is threatening another.
 *@authors Elizabeth Vandegriff and Emily Lynn
 */

//---------------------------------------------------------------------
// Includes
//---------------------------------------------------------------------
#include <iostream>
#include <cstdlib>

//#include "queens.h"

using namespace std;

void loadQueenMatrix(char QueenMatrix[35][35], int numQueens);
void printBoard(char QueenMatrix[35][35], int numQueens);
bool findSolution(char QueenMatrix[35][35], int numQueens, int row, int col);
bool goodSpot(char QueenMatrix[35][35], int numQueens, int row, int col);

int placedQueens = 0;

int main(int argc, char *argv[], char *envp[]) {

    int numQueens = atoi(argv[1]);
   // cout << "Number of queens = " << numQueens << endl;
    
    char QueenMatrix[35][35];
    loadQueenMatrix (QueenMatrix, numQueens);
    int row = 1;
    int col = 1;
    findSolution(QueenMatrix, numQueens, row, col);
    printBoard(QueenMatrix, numQueens);
    return 0;
}

void loadQueenMatrix(char QueenMatrix[35][35], int numQueens) {
    QueenMatrix[0][0] = '+';
    for (int c = 1; c <= numQueens; ++c) {
        QueenMatrix[0][c] = '-';
    }
    QueenMatrix[0][numQueens+1] = '+';
    for (int r = 1; r <= numQueens; ++r) {
        QueenMatrix[r][0] = '|';
        for (int c = 1; c <= numQueens; ++c) {
            QueenMatrix[r][c] = '-';
        }
        QueenMatrix[r][numQueens +1] = '|';
    }
    QueenMatrix[numQueens+1][0] = '+';
    for(int c = 1; c <= numQueens; ++c) {
        QueenMatrix[numQueens +1][c] = '-';
    }
    QueenMatrix[numQueens+1][numQueens+1] = '+';
}

void printBoard(char QueenMatrix[35][35], int numQueens) {
    for (int r = 0; r <= numQueens+1; ++r) {
        for (int c = 0; c <= numQueens+1; ++c) {
            cout << QueenMatrix[r][c];
        }
        cout << endl;
    }
    cout << endl;
}



bool findSolution(char QueenMatrix[35][35], int numQueens, int row, int col){
    if(placedQueens==numQueens){
        return true;
    }
    for (int i = 1; i <= numQueens; i++) {
        if (goodSpot(QueenMatrix, numQueens, row, i)) {
            QueenMatrix[row][i] = 'Q';
            placedQueens++;
            if (findSolution(QueenMatrix, numQueens, row+1, 1)) {
                 return true;
            }
            else {
                QueenMatrix[row][i] = '-';
                placedQueens--;
            }   
        }    
    }
    return false;
}




bool goodSpot(char QueenMatrix[35][35], int numQueens, int row, int col){
    for(int h=1;h<row+1;h++){
        if(QueenMatrix[h][col]=='Q'){
            return false;
        }
    }
    for(int r=1;(col+r)<(numQueens+1);r++){
        if((row-r)>0){
            if(QueenMatrix[row-r][col+r] == 'Q'){
                return false;
            }
            
        }
    }

    for(int r=1;(col-r)>0;r++){
        if((row-r)>0){
            if(QueenMatrix[row-r][col-r] == 'Q'){
                return false;
            }
            
        }
    }
    return true;
}


