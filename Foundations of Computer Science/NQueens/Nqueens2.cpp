/*Nqueens.cpp -- a program that receives a certain number of queens 
 *and places board of the same square size in such a way as that no queen
 *is threatening another.
 *@authors Elizabeth Vandegriff and Emily Lynn
 */

//---------------------------------------------------------------------
// Includes
//---------------------------------------------------------------------
#include <iostream>
#include <cstdlib>

#include "queens.h"

using namespace std;

void loadQueenMatrix(char QueenMatrix[30][30], int numQueens);
void printBoard(char QueenMatrix[30][30], int numQueens);
bool findSolution(char QueenMatrix[30][30], int numQueens);
bool goodSpot(char QueenMatrix[30][30], int numQueens, int row, int col);
//bool checkDiagonal(char Queenmatrix[30][30], int numQueens);

int placedQueens = 0;

int main(int argc, char *argv[], char *envp[]) {

    int numQueens = atoi(argv[1]);
    cout << "Number of queens = " << numQueens << endl;
    
    char QueenMatrix[30][30];
    loadQueenMatrix (QueenMatrix, numQueens);
    //recursive function
    printBoard(QueenMatrix, numQueens);
    findSolution(QueenMatrix, numQueens);
    return 0;
}

void loadQueenMatrix(char QueenMatrix[30][30], int numQueens) {
    QueenMatrix[0][0] = '+';
    for (int c = 1; c <= numQueens; ++c) {
        QueenMatrix[0][c] = EMPTY;
    }
    QueenMatrix[0][numQueens+1] = '+';
    for (int r = 1; r <= numQueens; ++r) {
        QueenMatrix[r][0] = '|';
        for (int c = 1; c <= numQueens; ++c) {
            QueenMatrix[r][c] = EMPTY;
        }
        QueenMatrix[r][numQueens +1] = '|';
    }
    QueenMatrix[numQueens+1][0] = '+';
    for(int c = 1; c <= numQueens; ++c) {
        QueenMatrix[numQueens +1][c] = EMPTY;
    }
    QueenMatrix[numQueens+1][numQueens+1] = '+';
}

void printBoard(char QueenMatrix[30][30], int numQueens) {
    for (int r = 0; r <= numQueens+1; ++r) {
        for (int c = 0; c <= numQueens+1; ++c) {
            cout << QueenMatrix[r][c];
        }
        cout << endl;
    }
    cout << endl;
}



bool findSolution(char QueenMatrix[30][30], int numQueens, int row, int col){
    if(placedQueens==numQueens){
        return true;
    }
    if(goodSpot(QueenMatrix, numQueens, row, col)==false){
        if (Matrix[row][col+1] == "|") {
            return false;
        }
        else {
            FindSolution(
    }
    return false;
}




bool goodSpot(char QueenMatrix[30][30], int numQueens, int row, int col){
    int n = 1;
    int p = 1;
    while(row>0 && row< numQueens){
        while(col>0 && col<numQueens){
            if(QueenMatrix[row+n][col+p]=='Q'){
                return false;
            }
            if(QueenMatrix[row+n][col]=='Q'){
                return false;
            }
            p++;
        }
        n++;
    }
    return true;
}


