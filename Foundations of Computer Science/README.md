This class was taken Fall 2017.

COS 121 4 hours
Foundations of Computer Science
This course builds on COS 120 by emphasizing object-oriented programming and
including concepts of computer science such as computational complexity
simulation and recursion. The use and implementation of data structures such as
lists, stacks, queues, and trees are introduced as they are needed in developing
algorithms for problems studied. Additional topics include the Linux operating
system and tools, source code versioning, unit testing, and code refactoring. Three
hours of lecture and two hours of lab per week.