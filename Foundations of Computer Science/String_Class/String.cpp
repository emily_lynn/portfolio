#ifndef STRING_CPP
#define STRING_CPP

#include <cstring>
#include "String.h"
#include <iostream>

using namespace std;


//Function for making a String: "String s1"
String::String() {
    bufferSize = 1;
    buffer = new char[bufferSize];
    buffer[0] = char(0);
}

//Function to make a new String that is equal to another String object: String s2(s1)
String::String( const String &other) {
    bufferSize = other.bufferSize;
    buffer = new char[bufferSize];
    strcpy(buffer, other.buffer);
}

//Function to set String object equal to a cString (array of chars): "String s3("hi")
String::String (const char *charSeq) {
    bufferSize = strlen(charSeq) + 1;
    buffer = new char[bufferSize];
    strcpy(buffer, charSeq);
}

//Function to clean up and destroy object and free up taken memory
String::~String() {
    delete[] buffer;
}

//Reinitializes the object using the specified string oject as a model
void String::set(const string &other) {
    delete[] buffer;
    bufferSize = other.length();
    buffer = new char[bufferSize];
    for (int i=0; i<=bufferSize; ++i) {
        buffer[i] = other.at(i); 
    }
    

}

//reinitializes the object using the specified C-String as a model
void String::set( char* charSeq ) {  //Why can't I call the above functions?
    delete[] buffer;
    bufferSize = strlen(charSeq) + 1;
    buffer = new char[bufferSize];
    strcpy(buffer, charSeq);

}

//reinitializes the object using the specified String object as a model
void String::set (const String &other) {
    delete[] buffer;
    bufferSize = other.bufferSize;
    buffer = new char[bufferSize];
    strcpy(buffer, other.buffer);

}

//sets the object to be an empty string
void String::clear() {
    bufferSize = 1;
    buffer = new char[bufferSize];
    buffer[0] = char(0);

}

//a replica of the C++ string.c_str() method that return a pointer to the internal C-string buffer
const char * String::c_str() const {
    return buffer;
}

//Equivalent to the C++ string .length method that returns a count of the number of letters in the string.
int String::getLength() const {
    return bufferSize - 1;
}

//overloaded assignment operator that returns a reference to this object. Makes an already made String object have the contents of another String object: "s1=s2"
String& String::operator= (const String& other) {
    if (this == &other) {
        return *this;
    }
    if (bufferSize < other.bufferSize) {
        delete[] buffer;
        bufferSize = other.bufferSize;
        buffer = new char[bufferSize];
    }
    strcpy(buffer, other.buffer);
    return *this;
}


//Overloaded equality operator. return whether or not the two string are equivalent
bool String::operator== (const String& other) const {
    if (this == &other) {
        return true;
    }
    if (bufferSize == other.bufferSize) {
        if (strcmp(this->buffer, other.buffer)== 0) {
            return true;
        }
        else {
            return false;
        }
    }
    else {
        return false;
    }
}

//overloaded subscript operator that returns the character at position n. If the specified position is not valid, return the null byte
char String::operator[] (int n) const {
    if ((n > bufferSize) || (n < 0)){
        return char(0);
    }
    else {
        return buffer[n];
    }
}

#endif 
