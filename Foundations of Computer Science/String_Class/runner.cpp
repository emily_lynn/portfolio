/* Generated file, do not edit */

#ifndef CXXTEST_RUNNING
#define CXXTEST_RUNNING
#endif

#define _CXXTEST_HAVE_STD
#include <cxxtest/TestListener.h>
#include <cxxtest/TestTracker.h>
#include <cxxtest/TestRunner.h>
#include <cxxtest/RealDescriptions.h>
#include <cxxtest/ErrorPrinter.h>

int main() {
 return CxxTest::ErrorPrinter().run();
}
#include "String_test.h"

static textTest suite_textTest;

static CxxTest::List Tests_textTest = { 0, 0 };
CxxTest::StaticSuiteDescription suiteDescription_textTest( "String_test.h", 11, "textTest", suite_textTest, Tests_textTest );

static class TestDescription_textTest_testDefaultConstructor : public CxxTest::RealTestDescription {
public:
 TestDescription_textTest_testDefaultConstructor() : CxxTest::RealTestDescription( Tests_textTest, suiteDescription_textTest, 14, "testDefaultConstructor" ) {}
 void runTest() { suite_textTest.testDefaultConstructor(); }
} testDescription_textTest_testDefaultConstructor;

static class TestDescription_textTest_testCopyConstructor : public CxxTest::RealTestDescription {
public:
 TestDescription_textTest_testCopyConstructor() : CxxTest::RealTestDescription( Tests_textTest, suiteDescription_textTest, 26, "testCopyConstructor" ) {}
 void runTest() { suite_textTest.testCopyConstructor(); }
} testDescription_textTest_testCopyConstructor;

static class TestDescription_textTest_testConversionConstructor : public CxxTest::RealTestDescription {
public:
 TestDescription_textTest_testConversionConstructor() : CxxTest::RealTestDescription( Tests_textTest, suiteDescription_textTest, 41, "testConversionConstructor" ) {}
 void runTest() { suite_textTest.testConversionConstructor(); }
} testDescription_textTest_testConversionConstructor;

static class TestDescription_textTest_testAssignmentOperator : public CxxTest::RealTestDescription {
public:
 TestDescription_textTest_testAssignmentOperator() : CxxTest::RealTestDescription( Tests_textTest, suiteDescription_textTest, 53, "testAssignmentOperator" ) {}
 void runTest() { suite_textTest.testAssignmentOperator(); }
} testDescription_textTest_testAssignmentOperator;

static class TestDescription_textTest_testSetString : public CxxTest::RealTestDescription {
public:
 TestDescription_textTest_testSetString() : CxxTest::RealTestDescription( Tests_textTest, suiteDescription_textTest, 69, "testSetString" ) {}
 void runTest() { suite_textTest.testSetString(); }
} testDescription_textTest_testSetString;

static class TestDescription_textTest_testSetCPPString : public CxxTest::RealTestDescription {
public:
 TestDescription_textTest_testSetCPPString() : CxxTest::RealTestDescription( Tests_textTest, suiteDescription_textTest, 85, "testSetCPPString" ) {}
 void runTest() { suite_textTest.testSetCPPString(); }
} testDescription_textTest_testSetCPPString;

static class TestDescription_textTest_testSetCString : public CxxTest::RealTestDescription {
public:
 TestDescription_textTest_testSetCString() : CxxTest::RealTestDescription( Tests_textTest, suiteDescription_textTest, 101, "testSetCString" ) {}
 void runTest() { suite_textTest.testSetCString(); }
} testDescription_textTest_testSetCString;

static class TestDescription_textTest_testClear : public CxxTest::RealTestDescription {
public:
 TestDescription_textTest_testClear() : CxxTest::RealTestDescription( Tests_textTest, suiteDescription_textTest, 115, "testClear" ) {}
 void runTest() { suite_textTest.testClear(); }
} testDescription_textTest_testClear;

static class TestDescription_textTest_test_c_str : public CxxTest::RealTestDescription {
public:
 TestDescription_textTest_test_c_str() : CxxTest::RealTestDescription( Tests_textTest, suiteDescription_textTest, 125, "test_c_str" ) {}
 void runTest() { suite_textTest.test_c_str(); }
} testDescription_textTest_test_c_str;

static class TestDescription_textTest_testLength : public CxxTest::RealTestDescription {
public:
 TestDescription_textTest_testLength() : CxxTest::RealTestDescription( Tests_textTest, suiteDescription_textTest, 139, "testLength" ) {}
 void runTest() { suite_textTest.testLength(); }
} testDescription_textTest_testLength;

static class TestDescription_textTest_testIndexOperator : public CxxTest::RealTestDescription {
public:
 TestDescription_textTest_testIndexOperator() : CxxTest::RealTestDescription( Tests_textTest, suiteDescription_textTest, 148, "testIndexOperator" ) {}
 void runTest() { suite_textTest.testIndexOperator(); }
} testDescription_textTest_testIndexOperator;

static class TestDescription_textTest_testOperatorEquals5 : public CxxTest::RealTestDescription {
public:
 TestDescription_textTest_testOperatorEquals5() : CxxTest::RealTestDescription( Tests_textTest, suiteDescription_textTest, 200, "testOperatorEquals5" ) {}
 void runTest() { suite_textTest.testOperatorEquals5(); }
} testDescription_textTest_testOperatorEquals5;

#include <cxxtest/Root.cpp>
