/* Generated file, do not edit */

#ifndef CXXTEST_RUNNING
#define CXXTEST_RUNNING
#endif

#define _CXXTEST_HAVE_STD
#include <cxxtest/TestListener.h>
#include <cxxtest/TestTracker.h>
#include <cxxtest/TestRunner.h>
#include <cxxtest/RealDescriptions.h>
#include <cxxtest/ErrorPrinter.h>

int main() {
 return CxxTest::ErrorPrinter().run();
}
#include "Calculator_test.h"

static Instructor__test suite_Instructor__test;

static CxxTest::List Tests_Instructor__test = { 0, 0 };
CxxTest::StaticSuiteDescription suiteDescription_Instructor__test( "Calculator_test.h", 18, "Instructor__test", suite_Instructor__test, Tests_Instructor__test );

static class TestDescription_Instructor__test_testInitial : public CxxTest::RealTestDescription {
public:
 TestDescription_Instructor__test_testInitial() : CxxTest::RealTestDescription( Tests_Instructor__test, suiteDescription_Instructor__test, 24, "testInitial" ) {}
 void runTest() { suite_Instructor__test.testInitial(); }
} testDescription_Instructor__test_testInitial;

static class TestDescription_Instructor__test_testSetValue1 : public CxxTest::RealTestDescription {
public:
 TestDescription_Instructor__test_testSetValue1() : CxxTest::RealTestDescription( Tests_Instructor__test, suiteDescription_Instructor__test, 34, "testSetValue1" ) {}
 void runTest() { suite_Instructor__test.testSetValue1(); }
} testDescription_Instructor__test_testSetValue1;

static class TestDescription_Instructor__test_testSetValue2 : public CxxTest::RealTestDescription {
public:
 TestDescription_Instructor__test_testSetValue2() : CxxTest::RealTestDescription( Tests_Instructor__test, suiteDescription_Instructor__test, 45, "testSetValue2" ) {}
 void runTest() { suite_Instructor__test.testSetValue2(); }
} testDescription_Instructor__test_testSetValue2;

static class TestDescription_Instructor__test_testClear : public CxxTest::RealTestDescription {
public:
 TestDescription_Instructor__test_testClear() : CxxTest::RealTestDescription( Tests_Instructor__test, suiteDescription_Instructor__test, 58, "testClear" ) {}
 void runTest() { suite_Instructor__test.testClear(); }
} testDescription_Instructor__test_testClear;

static class TestDescription_Instructor__test_testAdd1 : public CxxTest::RealTestDescription {
public:
 TestDescription_Instructor__test_testAdd1() : CxxTest::RealTestDescription( Tests_Instructor__test, suiteDescription_Instructor__test, 70, "testAdd1" ) {}
 void runTest() { suite_Instructor__test.testAdd1(); }
} testDescription_Instructor__test_testAdd1;

static class TestDescription_Instructor__test_testAdd2 : public CxxTest::RealTestDescription {
public:
 TestDescription_Instructor__test_testAdd2() : CxxTest::RealTestDescription( Tests_Instructor__test, suiteDescription_Instructor__test, 81, "testAdd2" ) {}
 void runTest() { suite_Instructor__test.testAdd2(); }
} testDescription_Instructor__test_testAdd2;

static class TestDescription_Instructor__test_testSubtract1 : public CxxTest::RealTestDescription {
public:
 TestDescription_Instructor__test_testSubtract1() : CxxTest::RealTestDescription( Tests_Instructor__test, suiteDescription_Instructor__test, 94, "testSubtract1" ) {}
 void runTest() { suite_Instructor__test.testSubtract1(); }
} testDescription_Instructor__test_testSubtract1;

static class TestDescription_Instructor__test_testSubtract2 : public CxxTest::RealTestDescription {
public:
 TestDescription_Instructor__test_testSubtract2() : CxxTest::RealTestDescription( Tests_Instructor__test, suiteDescription_Instructor__test, 105, "testSubtract2" ) {}
 void runTest() { suite_Instructor__test.testSubtract2(); }
} testDescription_Instructor__test_testSubtract2;

static class TestDescription_Instructor__test_testMultiplyBy1 : public CxxTest::RealTestDescription {
public:
 TestDescription_Instructor__test_testMultiplyBy1() : CxxTest::RealTestDescription( Tests_Instructor__test, suiteDescription_Instructor__test, 118, "testMultiplyBy1" ) {}
 void runTest() { suite_Instructor__test.testMultiplyBy1(); }
} testDescription_Instructor__test_testMultiplyBy1;

static class TestDescription_Instructor__test_testMultiplyBy2 : public CxxTest::RealTestDescription {
public:
 TestDescription_Instructor__test_testMultiplyBy2() : CxxTest::RealTestDescription( Tests_Instructor__test, suiteDescription_Instructor__test, 130, "testMultiplyBy2" ) {}
 void runTest() { suite_Instructor__test.testMultiplyBy2(); }
} testDescription_Instructor__test_testMultiplyBy2;

static class TestDescription_Instructor__test_testDivideBy1 : public CxxTest::RealTestDescription {
public:
 TestDescription_Instructor__test_testDivideBy1() : CxxTest::RealTestDescription( Tests_Instructor__test, suiteDescription_Instructor__test, 144, "testDivideBy1" ) {}
 void runTest() { suite_Instructor__test.testDivideBy1(); }
} testDescription_Instructor__test_testDivideBy1;

static class TestDescription_Instructor__test_testDivideBy2 : public CxxTest::RealTestDescription {
public:
 TestDescription_Instructor__test_testDivideBy2() : CxxTest::RealTestDescription( Tests_Instructor__test, suiteDescription_Instructor__test, 156, "testDivideBy2" ) {}
 void runTest() { suite_Instructor__test.testDivideBy2(); }
} testDescription_Instructor__test_testDivideBy2;

#include <cxxtest/Root.cpp>
