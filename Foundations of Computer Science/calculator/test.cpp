// Emily Lynn 

#include <iostream>

// #include the Calculator header file below
#include "Calculator.h"

using namespace std;

int main() {
    Calculator test;
    cout << test.getValue() << endl; //0
    test.setValue(9);
    cout << test.getValue() << endl; //9
    test.clear();
    cout << test.getValue() << endl; //0
    test.add(5);
    cout << test.getValue() << endl; //5
    test.subtract(5);
    cout << test.getValue() << endl; //0
    test.add(2);
    test.multiplyBy(4);
    cout << test.getValue() << endl; //8
    test.divideBy(4); 
    cout << test.getValue() << endl; //2



    return 0;
}
