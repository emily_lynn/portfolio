/*checkDelimiters.cpp -- a program that checks for properly balanced
 * delimiters and marks the problems using the caret ('^') symbol.
 * @author Emily Lynn
 * @date September 21 2017
 */

//-----------------------------------------------------------------
// Includes
//-----------------------------------------------------------------
#include <iostream>
#include <string>
#include <cstring>
#include <stack>


//-----------------------------------------------------------------
// Prototypes. If you write other functions, put the prototypes
// (function signatures) here.
//-----------------------------------------------------------------
void checkDelimiters( char line[] );



//-----------------------------------------------------------------
// Defined constants
//-----------------------------------------------------------------
const int LINESIZE = 1024;


using namespace std;
//-----------------------------------------------------------------
// Global variables
//-----------------------------------------------------------------
bool verbose = false;
bool veryVerbose = false;
stack<char> delimiters;
stack<int>delimiterPosition;
int lenStack = 0;

char topDel;
bool errorStatus = false;


int main( int argc, char *argv[] ) {
   char line[LINESIZE];

   if( argc == 2 && strcmp( argv[1], "-v" ) == 0) {
      verbose = true;
   }
   else if( argc == 2 && strcmp( argv[1], "-V" ) == 0) {
      veryVerbose = true;
   }

   if( verbose ) {
      cout << "This program checks a line of characters for balanced delimiter." << endl
  	   << "{ }, [ ], < >, ( )" << endl
	   << "Enter a line (<ENTER> to quit): ";
   }

   // Loop, processing lines one at a time, until reach EOF or empty string.
   cin.getline(line, sizeof line, '\n');

   while( cin && strlen(line) > 0 ) {
      checkDelimiters( line );
      if( verbose ) {
	 cout << "Enter a line (<ENTER> to quit): ";
      }
      cin.getline(line, sizeof line, '\n');
   }

   return 0;
}


//-----------------------------------------------------------------

/******************************************************************
 * Your code below. If you want extra functions, great. Write the code
 * below and add the function prototypes (signatures) with the other
 * function prototype for 'checkDelimiters' at the top of this file.
 ******************************************************************/
void checkDelimiters( char line[] ) {
	delimiters = stack<char>();
  delimiterPosition = stack<int>(); //position of stil unmatched open delimiter
	lenStack = 0;
	errorStatus = false;
	for(int i = 0; line[i]!=char(0); i++){
		switch (line[i]){
			case '{':
			case '<':
			case '[':
			case '(':
				delimiters.push(line[i]);
        delimiterPosition.push(i);
				lenStack = lenStack + 1;
				break;
			case '}':
				if (! delimiters.empty()) { //if there are still open delimiters unmatched
					topDel = delimiters.top();
					if (topDel == '{'){ //If it matches the last open delimiter
						delimiters.pop();
            delimiterPosition.pop();
						lenStack = lenStack - 1;
					}
					else { //doesn't match last open delimiter: unmatched pair
						cout << "Error" << endl;
						cout << line << endl;
						for (int b = 0; b < delimiterPosition.top(); b++) { //print spaces until unmatched opening delimiter position
							cout << " ";
						}
						cout << "^";
						for (int c = delimiterPosition.top() + 1; c < i; c++) { //print spaces until unmatched closing delimiter position
							cout << " ";
						}
						cout << "^" << endl << endl;
						return;
					}
				}
				else { //No remaining open delimiters: unmatched closing
					cout<< "Error" << endl;
					cout << line << endl;
					for (int a = 0; a < i; a++) { //print spaces until reach closing delimiter location
						cout << " ";
					}
					cout << "^" << endl << endl;
					return;
				}
				break;
			case '>':
				if (! delimiters.empty()) {
					topDel = delimiters.top();
					if (topDel == '<'){
						delimiters.pop();
            delimiterPosition.pop();
						lenStack = lenStack - 1;
					}
					else { //unmatched pair
						cout << "Error" << endl;
						cout << line << endl;
						for (int b = 0; b < delimiterPosition.top(); b++) {
							cout << " ";
						}
						cout << "^";
						for (int c = delimiterPosition.top() + 1; c < i; c++) {
							cout << " ";
						}
						cout << "^" << endl << endl;
						return;
					}
				}
				else { //unmatched closing
					cout<< "Error" << endl;
					cout << line << endl;
					for (int a = 0; a < i; a++) {
						cout << " ";
					}
					cout << "^" << endl << endl;
					return;
				}
				break;
			case ']':
				if (! delimiters.empty()) {
					topDel = delimiters.top();
					if (topDel == '['){
						delimiters.pop();
            delimiterPosition.pop();
						lenStack = lenStack - 1;
					}
					else { //unmatched pair
						cout << "Error" << endl;
						cout << line << endl;
						for (int b = 0; b < delimiterPosition.top(); b++) {
							cout << " ";
						}
						cout << "^";
						for (int c = delimiterPosition.top() + 1; c < i; c++) {
							cout << " ";
						}
						cout << "^" << endl << endl;
						return;
					}
				}
				else { //unmatched closing
					cout<< "Error" << endl;
					cout << line << endl;
					for (int a = 0; a < i; a++) {
						cout << " ";
					}
					cout << "^" << endl << endl;
					return;
				}
				break;
			case ')':
				if (! delimiters.empty()) {
					topDel = delimiters.top();
					if (topDel == '('){
						delimiters.pop();
            delimiterPosition.pop();
						lenStack = lenStack - 1;
					}
					else { //unmatched pair
						cout << "Error" << endl;
						cout << line << endl;
						for (int b = 0; b < delimiterPosition.top(); b++) {
							cout << " ";
						}
						cout << "^";
						for (int c = delimiterPosition.top() + 1; c < i; c++) {
							cout << " ";
						}
						cout << "^" << endl << endl;
						return;
					}
				}
				else { //unmatched closing
					cout<< "Error" << endl;
					cout << line << endl;
					for (int a = 0; a < i; a++) {
						cout << " ";
					}
					cout << "^" << endl << endl;
					return;
				}
				break;
		}
	}
	if (lenStack >= 1) { //unmatched opening
		cout << "Error" << endl;
		cout << line << endl;
    for (int d = 0; d < delimiterPosition.top(); d++) {
      cout << " ";
    }
		cout << "^" << endl << endl;
	}
	else if (errorStatus == false){
		cout << "Valid" << endl << endl;
	}
}
