/**
* Draw Diamond Program Starter.
* by Stefan Brandle
* February 7, 2011
*/
#include <iostream>
#include <iomanip>

using namespace std;

int main() {

    int width;
    cout << "How wide is the diamond (use an odd number)? ";
    cin >> width;

    cout << "The diamond is " << width << " wide."<< endl;

 // Print top half of diamond (growing)
    for (int len=1; len<=width; len+=2) {
        int spaces=(width-len)/2;
        cout << setw(spaces) << "";
        for (int i=0; i< len; i++) {
            cout << '*';
        }
        cout << endl;
    }
//Print bottom half of diamond (shrinking)
    for (int len = width-2; len >= 1; len-=2) {
	int spaces = (width-len)/2;
	cout << setw(spaces)<< "";
	for (int i=len; i>=1; i--) {
		cout << '*';
	}
	cout << endl;
	}
    return 0;
}
