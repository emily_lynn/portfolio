import math

DEBUGDefine = False
DEBUGForward = False
DEBUGolError = False
DEBUGhlError = False
DEBUGValueUpdates = False
DEBUGMain = False

class Node:
    def __init__(self, status, bias, weights):
        self.bias = bias
        self.weights = weights
        self.status = status  #input, hidden, or output
        self.netInput = 0
        self.output = 0
        self.err = 0

    def printNode(self):
        print("Node attributes")
        print("status: ", self.status)
        print("bias: ", self.bias)
        print("weights: ", self.weights)
        print("Netinput: ", self.netInput)
        print("output: ", self.output)
        print("error:", self.err)
        print("")

#class Layer would be a good idea for generalization

#inputNodes = None
#hiddenNodes = None
#outputNodes = None

def DefineNet(inputs):
    inputNodes = (Node("input", 1, (.2, -.3)), Node("input", 1, (.4, .3)))
    #inputNodes[0].output = inputs[0]
    #inputNodes[1].output = inputs[1]
    if DEBUGDefine:print(inputNodes[0].printNode())
    if DEBUGDefine:print(inputNodes[1].printNode())
    hiddenNodes = (Node("hidden", .1, (.3, -.2)), Node("hidden", -.1, (.5, -.4)))
    if DEBUGDefine:print(hiddenNodes[0].printNode())
    if DEBUGDefine:print(hiddenNodes[1].printNode())
    outputNodes = (Node("output", -.2, ()), Node("output", .3, ()))
    if DEBUGDefine:print(outputNodes[0].printNode())
    if DEBUGDefine:print(outputNodes[1].printNode())

    return inputNodes, hiddenNodes, outputNodes

def ForwardProp(inputNodes, hiddenNodes, outputNodes):
    if DEBUGForward:print(inputNodes[0].printNode())
    if DEBUGForward:print(inputNodes[1].printNode())
    for i in range(len(hiddenNodes)):
        hiddenNodes[i].netInput = (inputNodes[0].weights[i] * inputNodes[0].output) + (inputNodes[1].weights[i] * inputNodes[1].output) + hiddenNodes[i].bias
        hiddenNodes[i].output = 1/(1 + math.exp(-hiddenNodes[i].netInput))
    if DEBUGForward:print(hiddenNodes[0].printNode())
    if DEBUGForward:print(hiddenNodes[1].printNode())
    for i in range(len(outputNodes)):
        outputNodes[i].netInput = (hiddenNodes[0].weights[i] * hiddenNodes[0].output) + (hiddenNodes[1].weights[i] * hiddenNodes[1].output) + outputNodes[i].bias
        outputNodes[i].output = 1/(1 + math.exp(-outputNodes[i].netInput))
    if DEBUGForward:print(outputNodes[0].printNode())
    if DEBUGForward:print(outputNodes[1].printNode())

    return inputNodes, hiddenNodes, outputNodes

def OutputLayerErr(inputNodes, hiddenNodes, outputNodes, correctOutput):
    for i in range (len(outputNodes)):
        outputNodes[i].err = outputNodes[i].output * (1 - outputNodes[i].output) * (correctOutput[i] - outputNodes[i].output)
        if DEBUGolError:print(outputNodes[i].printNode())

def HiddenLayerErr(inputNodes, hiddenNodes, outputNodes):
    for i in range (len(hiddenNodes)):
        hiddenNodes[i].err = hiddenNodes[i].output * (1 - hiddenNodes[i].output) * ((outputNodes[0].err*hiddenNodes[i].weights[0]) + (outputNodes[1].err*hiddenNodes[i].weights[1]))
        if DEBUGhlError:print(hiddenNodes[i].printNode())


def ValueUpdates(inputNodes, hiddenNodes, outputNodes, lrate):
    for i in range(len(outputNodes)):
        #update for output layer
        outDeltaBias = lrate*outputNodes[i].err
        outputNodes[i].bias = outputNodes[i].bias + outDeltaBias
        if DEBUGValueUpdates:print(outputNodes[i].printNode())

        #update for hidden layer
        hiddenDeltaBias = lrate*hiddenNodes[i].err
        hiddenDeltaWeights = [(lrate)*outputNodes[0].err*hiddenNodes[i].output, (lrate)*outputNodes[1].err*hiddenNodes[i].output]
        hiddenNodes[i].bias = hiddenNodes[i].bias + hiddenDeltaBias
        hiddenNodes[i].weights = [hiddenNodes[i].weights[0] + hiddenDeltaWeights[0], hiddenNodes[i].weights[1] + hiddenDeltaWeights[1]]
        if DEBUGValueUpdates:print(hiddenNodes[i].printNode())

        #update for input layer
        inputDeltaWeights = [(lrate)*hiddenNodes[0].err*inputNodes[i].output, (lrate)*hiddenNodes[1].err*inputNodes[i].output]
        inputNodes[i].weights = [inputNodes[i].weights[0] + inputDeltaWeights[0], inputNodes[i].weights[1] + inputDeltaWeights[1]]
        if DEBUGValueUpdates:print(inputNodes[i].printNode())

def BackProp(inputNodes, hiddenNodes, outputNodes, correctOutput):
    OutputLayerErr(inputNodes, hiddenNodes, outputNodes, correctOutput)
    HiddenLayerErr(inputNodes, hiddenNodes, outputNodes)
    ValueUpdates(inputNodes, hiddenNodes, outputNodes, 1)

def main():
    inputOptions = [[1,1], [1,0], [0,1], [0,0]]
    correctOutputs = [[0,1], [1,0], [1,0], [0,1]]
    inputNodes, hiddenNodes, outputNodes = DefineNet(inputOptions[0])

    #Training the Neural Net
    print("Training...")
    print("")
    for i in range(4000):
        inputChoice = i%4
        correctOutput = correctOutputs[i%4]
        inputs = inputOptions[inputChoice]
        inputNodes[0].output = inputs[0]
        inputNodes[0].bias = inputs[0]
        inputNodes[1].output = inputs[1]
        inputNodes[1].bias = inputs[1]
        #if DEBUGMain:print(inputNodes[0].printNode())
        #if DEBUGMain:print(inputNodes[1].printNode())
        ForwardProp(inputNodes, hiddenNodes, outputNodes)
        BackProp(inputNodes, hiddenNodes, outputNodes, correctOutput)
    if DEBUGMain:print(inputNodes[0].printNode())
    if DEBUGMain:print(inputNodes[1].printNode())
    if DEBUGMain:print(outputNodes[0].printNode())
    if DEBUGMain:print(outputNodes[1].printNode())

    #Testing
    print("Testing...")
    print("")
    correctClassifications = 0
    for i in range(4):
        inputChoice= i
        inputs = inputOptions[inputChoice]
        inputNodes[0].output = inputs[0]
        inputNodes[0].bias = inputs[0]
        inputNodes[1].output = inputs[1]
        inputNodes[1].bias = inputs[1]
        ForwardProp(inputNodes, hiddenNodes, outputNodes)
        hypothesisOutput = [round(outputNodes[0].output, 0), round(outputNodes[1].output, 0)]
        if (hypothesisOutput == correctOutputs[i]):
            correctClassifications = correctClassifications + 1
    percentCorrect = (correctClassifications/4)*100
    print("Performance: ", percentCorrect, "% correctly classified")
    print("")

    #Interact
    interactAnswer = input("Would you like to test a set of input values (Y/N) ? ")
    if (interactAnswer == "Y" or interactAnswer == "y"):
        tryAgain = True
        while (tryAgain):
            userInput = [0,0]
            userInput[0] = int(input("Please enter the first input value (0 or 1): "))
            userInput[1] = int(input("Please enter the second input value (0 or 1): "))
            inputNodes[0].output = userInput[0]
            inputNodes[0].bias = userInput[0]
            inputNodes[1].output = userInput[1]
            inputNodes[1].bias = userInput[1]
            ForwardProp(inputNodes, hiddenNodes, outputNodes)
            hypothesisOutput = [round(outputNodes[0].output, 0), round(outputNodes[1].output, 0)]
            if (hypothesisOutput == [0,1]):
                print("The result of an XOR gate with your given input is False")
                print("")
            elif (hypothesisOutput == [1,0]):
                print("The result of an XOR gate with your given input is True")
                print("")
            else:
                print("Something broke")
            cont = input("Would you like to test another input (Y/N) ? ")
            if (cont == "Y" or cont == "y"):
                tryAgain = True
            elif (cont == "N" or cont == "n"):
                tryAgain = False
            else:
                exit("Invalid Input. Exiting")
        print("")
        exit("Thank you for using this Neural Net.")
    elif(interactAnswer == "N" or interactAnswer == "n"):
        print("")
        exit("Thank you for using this Neural Net.")
    else:
        exit("Invalid Input. Exiting.")



main()


#An epoch is running once through all of the possible patters (4 for XOR, 150 for Iris Data)
#Need to go through about 1000 epochs (4000 individual)


