
def readInData(priceFile, sqFeetFile):
    """Stores the price and square feet data in a format that can be used
    for the rest of the function"""
    pass

def computeHypothesis(price_y, squareFeet_x):
    """Somehow computes and equation that attempts to fit the data
    trend in some way"""
    pass
    #return the hypothesis function h that seems to fit the data

def hypothesisFunctionH(squareFeet_x):
    """The equation that computes the predicted price"""
    pass
    #return the predicted price at this given squareFeet_x

def costFunction(price_y, squareFeet_x, m):
    """uses the hypothesis function to compute the predicted values for y
    and then uses the difference between the actual and predicted y's
    to compute the cost function for that hypothesis. Will call hypothesisFunction"""
    pass
    #return J

def gradientDescent(theta_0, theta_1, alpha, J):
    """uses the cost function equation and the step size alpha
    to iteratively compute and update new values for theta_0 and theta_1
    that converge at a local minimum
    (requires the ability to compute a derivative of the function J"""
    pass
    #return the theta_0 and theta_1 coordinates of the minimum location

def linearGradientDescent(startTheta_0, startTheta_1, alpha, price_y, squareFeet_x, m):
    """ like the gradientDescent function, except the derivative of a linear
    regression cost function is known and can be explicitely programmed in terms
    of the hypothesis function h
    """
    pass
    #return the theta_0 and theta_1 coordinates of the minimum location


def main():
    readInData(0,0)
    computeHypothesis(0, 0)
    hypothesisFunctionH(0)
    costFunction(0, 0, 0)
    gradientDescent(0, 0, 0, 0)
    linearGradientDescent(0, 0, 0, 0, 0, 0)

main()
print("done")
