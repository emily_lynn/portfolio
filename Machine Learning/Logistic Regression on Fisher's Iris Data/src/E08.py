# Need to display J Cost decreasing
# And the final coefficients
# Perhaps run back on data and see how many cases this model fails for
# don't forget to shuffle the data for randomness
# make X a list of lists where each of those inner lists has 1 as the first entry
# Y is a list of Y values
# X and Y need to be parallel lists
# can either do it iteratively or import a linear library
# Need to stop grad descent if JCost starts incr or is past the max number of iterations\
# ThetaList as an imput list ThetaList = [0,0,0,0,0] (4 features and what's the first one?)
# input file should be the Normalized Data from the Scale and Normalize function
import linecache
import numpy as np
import math

DEBUG1 = False
DEBUG2 = False
DEBUG3 = False
DEBUG4 = False

def InputAndStructureData(inputFileName, outputFileName):
    #May want to just make all of this a hardcoded jump to the 7th line...
    inputFile = open(inputFileName, "r")
    outputFile = open(outputFileName, "w+")
    dataName = linecache.getline(inputFileName, 1)
    lineBuffer = linecache.getline(inputFileName, 2)
    numFeatures = linecache.getline(inputFileName, 3)
    numPredictedVals = linecache.getline(inputFileName, 4)
    colNames = linecache.getline(inputFileName, 5)
    documentation = linecache.getline(inputFileName, 6)
    #Need to iterate through and turn the data into two sets instead of three, and then use those two sets to make X and Y
    #Y is the predicted identification
    #X is the features (with the leading 1)
    counter = 0;
    XFeatures = []
    YPredictVal = []
    DataLOL = []
    for line in inputFile:
        counter = counter + 1
        if counter > 6: # this is where the actual data starts...
            lineList = (line.strip()).split(" ")
            DataLOL.append(lineList)
    for i in range(len(DataLOL)):
        XFeatures.append(DataLOL[i])
        XFeatures[i][0] = 1;
        YPredictVal.append(XFeatures[i].pop())
    for i in range(len(YPredictVal)):
        if (int(float(YPredictVal[i])) == 0): #Allows for different testing with distinguishing between different species
            YPredictVal[i] = 0
        elif (int(float(YPredictVal[i])) == 1): #Merges the versicolor category with the setosa category
            YPredictVal[i] = 0
        elif (int(float(YPredictVal[i])) == 2): #Allows for different testing with distinguishing between different species
            YPredictVal[i] = 1
    ThetaList = [0,0,0,0,0]
    X = np.array(XFeatures, dtype=float)
    Y = np.array(YPredictVal, dtype=int)
    Theta = np.array(ThetaList, dtype=float)
    #if DEBUG1:print(X)
    #if DEBUG1:print(X.shape)
    #if DEBUG1:print(Y)
    #if DEBUG1:print(Y.shape)
    #if DEBUG1:print(Theta)
    #if DEBUG1:print(Theta.shape)
    #h = CalculateHypothesis(X, Y, Theta)
    #cost = CalculateCost(X, Y, Theta, h)
    #Theta = CalculateTheta(X, Y, Theta, h, cost)
    Theta = gradientDescent(X, Y, Theta, alpha=.1,threshold=.0000000001,maxIters=10000000 )
    print(Theta)
    h = CalculateHypothesis(X, Y, Theta)
    for i in range(len(h)):
        h[i] = int(round(h[i]))
    if DEBUG1:print(Y)
    if DEBUG1:print(h)
    if DEBUG1:print(Y - h)
    for i in range(len(h)):
        if int(h[i]) != Y[i]:
            print("incorrectly classified case: hypothesis was ", int(h[i]), " but Y was ", Y[i])


def CalculateHypothesis(X, Y, Theta):
    if DEBUG2:print(X)
    z = (Theta.transpose()).dot(X.transpose())
    if DEBUG2:print("z dimensions:", z.shape)
    g = 1/(1 + np.exp(-z))
    if DEBUG2:print("g dimensions:", g.shape)
    if DEBUG2:print("z is :", z, " and g is :", g)
    return g

def CalculateCost(X, Y, Theta, h):
    m = 150;
    if DEBUG3:print("h is ", h)
    if DEBUG3:print("h dimensions:", h.shape)
    #if DEBUG3:print(1 - h)
    #if DEBUG3:print(np.log(np.identity(150) - h))
    J = (1/m) * (-Y.transpose().dot(np.log(h)) - ((1-Y).transpose()).dot(np.log(1-h)))
    if DEBUG3:print(J)
    return J

def CalculateTheta(X, Y, Theta, h, alpha):
    m = 150
    if DEBUG4:print("X dim is:", X.shape, " and Y dim is:", Y.shape, " and h dim is:", h.shape, " and X^T dim is:", (X.transpose()).shape)
    if DEBUG4:print("dim of computation is:", ((X.transpose()).dot((h - Y))).shape )
    newTheta = Theta - ((alpha/m)*(X.transpose()).dot((h - Y)))
    if DEBUG4:print("newTheta is :", newTheta)
    return newTheta

def gradientDescent(X, Y, Theta, alpha, threshold, maxIters):
    Theta = Theta
    countIters = 1
    currentCost = CalculateCost(X, Y, Theta, CalculateHypothesis(X, Y, Theta))
    prevCost=currentCost+1
    h = CalculateHypothesis(X, Y, Theta)
    while countIters<maxIters and abs(prevCost-currentCost) > threshold:
        prevCost = currentCost
        Theta = CalculateTheta(X, Y, Theta, h, alpha)
        h = CalculateHypothesis(X, Y, Theta)
        currentCost = CalculateCost(X, Y, Theta, h)
        if currentCost>prevCost:
            print("JCost is increasing - why?")
            exit()
        if countIters%20000==0:
            print("Theta is: ", Theta, " and cost is:", currentCost)
        countIters+=1
        if (abs(prevCost-currentCost) < threshold):
            print("Threshold reached after ", countIters, " iterations")
        if (countIters>maxIters):
            print("Max iterations reached without reaching threshold")
    return Theta




def main():
    inputFileName = "NormalizedData.txt"
    outputFileName = "OutputFile.txt"
    InputAndStructureData(inputFileName, outputFileName)


main()

