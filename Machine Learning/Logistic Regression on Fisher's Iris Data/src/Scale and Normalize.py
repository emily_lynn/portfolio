import linecache
import numpy as np
import math

DEBUG = False
DEBUG2 = False

def ScaleAndNormalize(inputFileName, outputFileName, delimiter, decimalPrecision):
    inputFile = open(inputFileName, "r")
    outputFile = open(outputFileName, "w+")
    dataName = linecache.getline(inputFileName, 1)
    lineBuffer = linecache.getline(inputFileName, 2)
    numFeatures = linecache.getline(inputFileName, 3)
    numPredictedVals = linecache.getline(inputFileName, 4)
    colNames = linecache.getline(inputFileName, 5)
    documentation = linecache.getline(inputFileName, 6)
    linesToWrite = [dataName, lineBuffer, numFeatures, numPredictedVals, colNames, documentation]
    outputFile.writelines(linesToWrite)
    #Should test to make sure that these sum to the number of items in the data
    #dataTestLine = linecache.getline(fileName, 5)
    #if ()

    counter = 0
    firstPass = True
    if DEBUG:print(int(numFeatures))
    minMaxArray = np.zeros( (int(numFeatures)+1,2) ) # index i is column i, and the first item in the array at that index is the min and the second item is the max
    if DEBUG:print(minMaxArray)
    sumArray = np.zeros(int(numFeatures)+1) # index i is column i, and the item is the sum of all of the numbers in that column
    if DEBUG:print(sumArray)

    for line in inputFile:
        counter = counter + 1
        if DEBUG:print(counter)
        if counter > 6:
            lineList = (line.strip()).split(delimiter)
            if DEBUG:print(lineList)
            for i in range(1, int(numFeatures) + 1):
                if DEBUG:print(lineList[i])
                if firstPass == True:
                    minMaxArray[i] = [lineList[i], lineList[i]]
                else:
                    if float(lineList[i]) < minMaxArray[i, 0]:
                        minMaxArray[i,0] = lineList[i]
                    if float(lineList[i]) > minMaxArray[i, 1]:
                        minMaxArray[i,1] = lineList[i]
                sumArray[i] = sumArray[i] + float(lineList[i])
            firstPass = False
        #if DEBUG:print(minMaxArray)
        #if DEBUG:print(sumArray)
    avgArray = np.zeros(int(numFeatures)+1)
    for i in range(len(sumArray)):
        avgArray[i] = sumArray[i]/(counter - 6) # should actually be div by num added together
    if DEBUG:print(avgArray)

    counter = 0
    inputFile.seek(0)  # puts cursor back at the beginning of the file
    for line in inputFile:
        counter = counter + 1
        if DEBUG2:print(counter)
        writeLine = ""
        if counter > 6:
            lineList = (line.strip()).split(delimiter)
            if DEBUG2:print(lineList)
            for i in range(1, len(lineList)-1):
                lineList[i] = (float(lineList[i]) - avgArray[i]) / (minMaxArray[i, 1] - minMaxArray[i, 0])
            if DEBUG2:print(lineList)
            for i in range(len(lineList)):
                if i == 0:
                    writeLine = writeLine + " " + str(math.trunc(float(lineList[i]))) # could have used ((decimalPrecision+1) * " ") instead of " " for readability, but causes problems for general splitting later
                else:
                    writeLine = writeLine + " " + str(round(float(lineList[i]), decimalPrecision))
            if DEBUG2:print(writeLine)
            outputFile.write(writeLine + "\n")




    inputFile.close()
    outputFile.close()


def main():
    inputFile = "Fishers Iris Data.txt"
    outputFile = "NormalizedData.txt"
    ScaleAndNormalize(inputFile, outputFile, ",", 7)

main()