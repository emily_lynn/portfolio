Class taken Spring 2020.
All of the code in these Machine Learning projects is my own. 

SYS 411 3 hours
Machine Learning
Classification learning systems of various types are explored. These include
statistical pattern recognition, neural networks, genetic algorithms, and methods
for inducing decision trees and production rules induction. Existing systems are
reviewed. Group term projects allow development of and experimentation with a
system of interest.