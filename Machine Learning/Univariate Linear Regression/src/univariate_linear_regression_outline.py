""" The following code will generate a univariate
(one feature) linear regression line given a set of
data in two input files (x and y) one item per line"""
import time
import matplotlib.pyplot as plt
import numpy as np
global DEBUG
DEBUG=False

#Code the data reading function to produce parallel lists
#xList(our single feature) and yList(our actual y value when
#the parallel x value in xList was observed).  It should be
#noted that the same x value in another case may result in a
#different observed #value in our training data set.  We expect
#to read in m x values and m y values from our files.
#Input: the file names
#Outputs: xList, parallel yList, m (the number of #training cases)
def readAndSetUpTrainingValues(fileNameX,fileNameY):
    xList=[]
    yList=[]
    inFile=open(fileNameX,"r")
    mx=0
    for line in inFile:
        xList.append(float(line))
        mx+=1
    inFile.close()
    inFile=open(fileNameY,"r")
    my=0
    for line in inFile:
        yList.append(float(line))
        my+=1
    inFile.close()
    if my != mx:
        print("Count mismatch between feature and dependent variable!  -- Terminating")
        exit()        
    return xList,yList,mx


#Code the hypothesis function
#Inputs: x value, theta0 and theta1
#Output: predicted value (predicted y)
def hOfX(x,theta0,theta1):
    h_theta = theta0 + (theta1*x)
    return h_theta  #return the predicted value


#Code the cost function.
#Not used directly for linear regression (the derivative
#is used to adjust #thetas in our gradient descent algorithm)
#but can be output as an indication that the training process
#is moving in the right direction, or used to terminate
#gradient descent loop
#Invokes hOfX
#Inputs: m, theta0, theta1, xList, yList
#Output: cost value
def JCost(m,theta0,theta1,xList,yList):
    sum = 0;
    for i in range(m):
        sum = sum + ( (hOfX(xList[i], theta0, theta1) - yList[i])**2 )
    J = sum * (1/(2*m))
    return J # return the cost


#Code the partial derivative of JCost (given in notes) for use
#in gradient descent process
#Inputs: m, theta0, theta1, xList, yList
#Outputs: totalDiffs for Theta0 and Theta1
def JCostDerivForGradientDescent(m,theta0,theta1,xList,yList):
    sum0 = 0
    sum1 = 0
    for i in range(m):
        sum0 = sum0 + ( (hOfX(xList[i], theta0, theta1) - yList[i]) )
        sum1 = sum1 + ( (hOfX(xList[i], theta0, theta1) - yList[i])*xList[i] )
    return sum0,sum1  # return the total diffs for theta0 and theta1

#Code the gradient descent process loop for convergence on
#the global minimum (for univariate linear regression should
#always be able to find the only minimum that exists for JCost
#if alpha is correctly chosen)
#Inputs: m, xList,yList,alpha,threshold,maxIters
#Outputs: theta0, theta1, (doc purposes: alpha, countIters, threshold)
def gradientDescent(m,xList,yList,alpha,threshold,maxIters):
    countIters=1
    theta0=0
    theta1=0
    currentJCost=JCost(m,theta0,theta1,xList,yList)
    prevJCost=currentJCost+1
    if DEBUG:print(currentJCost)
    while countIters<maxIters and abs(prevJCost-currentJCost) > threshold:
        tDiffsTheta0,tDiffsTheta1 = JCostDerivForGradientDescent(m,theta0,theta1,xList,yList)
        temp0=theta0 - alpha * ((1/m) * (tDiffsTheta0))
        temp1=theta1 - alpha * ((1/m) * (tDiffsTheta1))
        theta0=temp0
        theta1=temp1
        prevJCost=currentJCost
        #plt.subplot(211)
        x = np.linspace(2, 8, 10)
        if (countIters%1000 == 0):
            plt.plot(x, theta0 + (theta1*x), '-r')
            plt.draw()
            plt.pause(0.001)
        currentJCost=JCost(m,theta0,theta1,xList,yList)
        if currentJCost>prevJCost:
            print("JCost is increasing - why?")
            exit()
        if countIters%10000==0:
            #print(prevJCost,currentJCost,format(prevJCost-currentJCost,".17f"))
            print('{:7d} prevJCost= {:01.17f}  currentJCost= {:01.17f}   diff= {:01.17f}'.format(countIters, prevJCost, currentJCost, prevJCost-currentJCost))
        countIters+=1
        if DEBUG:print(currentJCost)
    if DEBUG:print("Gradient Descent iterations",countIters)
    return theta0, theta1, alpha, countIters, threshold

#global variables
#def defineGlobals(xList, yList):
    #global dataplot
    #dataplot = plt.plot(xList, yList, 'ko')
    #global lineplot
    #lineplot = plt.

def plotData(xList, yList):
    #global dataplot
    plt.ion()
    plt.subplot(211)
    plt.plot(xList, yList, 'ko')
    #plt.show()
    #plt.draw()
    plt.pause(0.001)


#def plotRegressionLine():


def main():
    xList,yList,m=readAndSetUpTrainingValues("xData.txt","yData.txt")
    if DEBUG:print(xList)
    if DEBUG:print(yList)
    if DEBUG:print(m)
    #defineGlobals(xList, yList)
    plotData(xList, yList)
    start=time.time()
    theta0,theta1,alpha,iters,threshold=gradientDescent(m,xList,yList,alpha=.001,threshold=.000000000001,maxIters=10000000)
    stop=time.time()
    print('Theta0= {:01.17f}   Theta1= {:01.17f}'.format(theta0,theta1))
    print("For alpha",format(alpha,'.6f'),"with",iters,"iterations","and threshold",format(threshold,'.17f'),"took",format(stop-start,'.12f'),"seconds")
    
main()

'''
When alpha = .001:
Theta0= 0.74981212910412887   Theta1= 0.06394263860956333
For alpha 0.001000 with 74677 iterations and threshold 0.00000000000100000 took 6.866756677628 seconds

When alpha = .01:
Theta0= 0.75001642277709568   Theta1= 0.06390547045859614
For alpha 0.010000 with 8571 iterations and threshold 0.00000000000100000 took 0.536561727524 seconds

When alpha = .1:
JCost is increasing - why?

Alpha is the step size, and so the smaller the step size the more steps it will take to get to the minimum, which is why
there are more iterations for the smaller alpha. Once alpha is too large, it will overshoot the minimum and cause the JCost to increase, 
whihc is why at the largest alpha that I tried I did not actually get a result below the required threshold. 
'''

#matplotlib for plotting this

#to plot, may need to install pip for installation purposes