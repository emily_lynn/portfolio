
using namespace std;
#include <cmath>
#include <iostream>
#include <vector>

double Func(double x); 
void Romberg();
double Trap(double n, double a, double b);

double Func(double x) {
    double value = pow(x, 0.1)*(1.2 - x)*(1 - exp(20*(x-1))); 
    return value;
}

double Trap(double n, double a, double b) {
    double h = (b - a)/n;
    double x = a;
    double sum = Func(x);
    for (int i = 1; i < n; ++i) {
	x = x + h;
	sum = sum + (2*Func(x));
    }
    sum = sum + Func(b);
    double IntEst = (b - a)*sum/(2*n);
    return IntEst;
}

double Romberg(double a, double b, double maxit, double es) {
    vector< vector<double> > I(100, vector<double>(100, 1));
    double n = 1;
    I[1][1] = Trap(n, a, b);
    double iter = 0;
    double ea = 100;
    while ((iter < maxit) && (ea > es)){
	iter = iter + 1;
	n = pow(2.0, iter);
	I[iter+1][1] = Trap(n, a, b);
	for (int k = 2; k < (iter +2); ++k) {
	    double j = 2 + iter - k;
	    I[j][k] = ((pow(4.0, k-1)*I[j+1][k-1]) - I[j][k-1]) / (pow(4.0, k-1) -1);
	}
	ea = abs((I[1][iter + 1] - I[2][iter])/I[1][iter+1]) * 100;
    }
    double IntEst = I[1][iter+1];
    cout << "The Integral Estimate is : " << IntEst 
	 << "\nWith an approx. relative error of : " << ea << " %" 
	 << "\nFound after " << iter << " iterations" << endl;
}


int main() {
    Romberg(0, 1, 20, 0.05);
    return(0);
}
