
using namespace std;
#include <iostream>
#include <cmath>

void DivAvg(double a);

/*Code to use the Divide and Average Method of Finding Roots
  Variables:
    a = any positive number
    tol = tolerance level for the error
    sqrt = final answer
    y = new value for x
    x = current value for the sqrt
*/
void DivAvg(double a){
    double x = 0;
    double y = 0;
    double sqrt;
    double tol = pow(10, -5);
    double e = 100;
    if (a > 0){ //If a is a valid positive number
	x = a/2;
	while ( e > tol){ //while the approx. error is greater than the tolerance errror, keep trying
	   y = (x + (a/x))/2;
	   e = abs( (y-x)/y );
	   x = y;
        }
	sqrt = x;
    }
    else {
	sqrt = 0;
    }
    cout << "The Square Root of " << a << " is : " << sqrt <<endl;
}

int main(){
    DivAvg(4);
    DivAvg(5);
    DivAvg(-3);
    return(0);
}


	
