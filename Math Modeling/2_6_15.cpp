
using namespace std;
#include <iostream>
#include <cmath>

double Fixpt(double x0, double y0, double es, double imax);

double NewtRaph(double x0, double y0, double es, double imax);

//Part (a): Fixed Point Iteration
double Fixpt(double x0, double y0, double es, double imax) {
    double xr = x0;
    double yr = y0;
    double iter = 0;
    double eax = 100;
    double eay = 100;
    while ( eax > es || eay > es ){
	double xrold = xr;
	double yrold = yr;
	xr = sqrt(xrold - yrold + 0.75);
	yr = ( (xr*xr) - yrold )/(5*xr);
	iter = iter + 1;
	if (xr != 0 && yr != 0) {
	    eax = abs( (xr - xrold)/xr ) * 100;
	    eay = abs( (yr - yrold)/yr ) * 100;
        }
    }
    double fixpt = xr;
    cout << "The root is : (" << xr << ", " << yr << ")" 
	 << "\n eax = " << eax 
	 << "\n eay = " << eay 
	 << "\n iterations : " << iter << endl;
}

double NewtRaph(double x0, double y0, double es, double imax) {
    double xr = x0;
    double yr = y0;
    double iter = 0;
    double eax = 100;
    double eay = 100;
    double u = 0;
    double v = 0;
    double du_x = 0;
    double du_y = 0;
    double dv_x = 0;
    double dv_y = 0;
    while ( (eax > es || eay > es) ) {
	double xrold = xr;
	double yrold = yr;
	u = yrold + (xrold*xrold) - xrold - 0.75;
	v = yrold + (5*xrold*yrold) - (xrold*xrold);
	du_x = (2*xrold) -1;
	du_y = 1.0;
	dv_x = (5*yrold) - (2*xrold);
	dv_y = (5*xrold) + 1.0;
	xr = xrold - ( ((u * dv_y)-(v*du_y)) / ((du_x * dv_y) - (du_y * dv_x)) );
	yr = yrold - ( ((v * du_x)-(u*dv_x)) / ((du_x * dv_y) - (du_y * dv_x)) );
	iter = iter + 1;
	if (xr != 0 && yr != 0) {
	    eax = abs( (xr - xrold)/xr ) * 100;
	    eay = abs( (yr - yrold)/yr ) * 100;
	}
    }
    cout << "The root is : (" << xr << ", " << yr << ")"
	 << "\n eax = " << eax
	 << "\n eay = " << eay
	 << "\n iterations : " << iter << endl;
}

int main(){
    Fixpt(1.2, 1.2, 0.05, 50);
    //NewtRaph(1.2, 1.2, 0.05, 50);
    return(0);
}



