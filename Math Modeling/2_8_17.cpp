
using namespace std;

#include <iostream>
#include <cmath>

void NewtRaph(double x0, double tol);

void NewtRaph(double x0, double tol){
    double xr = x0;
    double xrold;
    double ea = 100;
    while( ea > tol ){
	xrold = xr;
	double f = ( (xrold/24)*(exp(600/xrold)+exp(-600/xrold)) ) -(xrold/12) -9;
    	double fprime =( (1/24)*( exp(600/xrold)+exp(-600/xrold)) ) + ( (600/(12*xrold))*(-exp(600/xrold)+exp(-600/xrold)) ) - (1/12);
        xr = xrold - (f/fprime);
	if (xr != 0){
	    ea = abs( (xr-xrold)/xr ) * 100;
	}
    }
    cout << "The root is : " << xr 
	 << "\n ea is " << ea << endl;
}
int main(){
    NewtRaph(1500, 0.05);
    return(0);
}
