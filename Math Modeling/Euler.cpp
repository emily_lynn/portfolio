
using namespace std;

#include <cmath>
#include <iostream>

double PrimeFunction(double y, double x);
double Function(double x);
void Euler(double xi, double xf, double yi, double dx);


double PrimeFunction(double y, double x){
    double yprime = (1.0+(2.0*x))*sqrt(y);
    return yprime;
}

double Function(double x){
    double y = (1.0/4.0)*pow((-2.0 + x + pow(x,2)), 2);
    return y;
}

void Euler(double xi, double xf, double yi, double dx){
    double x = xi;
    double y = yi;
    double nc = (xf-xi)/dx;
    double relError = 100;
    cout << "The initial values are x = " << x << " and y = " << y << endl;
    for (int i = 1; i < (nc+1); i++){
	double dydx = PrimeFunction(y, x);
	y = y + (dydx*dx);
	x = x+dx;
	relError = abs((y - Function(x))/Function(x));
	cout << "x = " << x 
	     << "   y = " << y 
	     << "   Relative Error: " << relError 
	     << endl;
    }
}

int main(){
    Euler(0, 1, 1, 0.01);
    return(0);
}


    
