
using namespace std;

#include <iostream>
#include <cmath>

void GSeid(double tol);

void RelaxGSeid(double tol, double lamda);

void TryGSeid(double tol);

void TryGSeid3(double tol);

void GSeid(double tol){
    double T11 = (0 + 0 + 275)/4;
    double T12 = (T11 + 0 + 225)/4;
    double T21 = (T11 + 0 + 75)/4;
    double T22 = (T12 + T21 + 25)/4;
    double eT11 = 100;
    double eT12 = 100;
    double eT21 = 100;
    double eT22 = 100;
    double iter = 1;
    while ((eT11 > tol) || (eT12 > tol) || (eT21 > tol) || (eT22 > tol)){
	double T11old = T11;
	double T12old = T12;
	double T21old = T21;
	double T22old = T22;
	T11 = (T12old + T21old + 275)/4;
	T12 = (T11 + T22old + 225)/4;
	T21 = (T11 + T22old + 75)/4;
	T22 = (T12 + T21 + 25)/4;
	eT11 = abs((T11-T11old)/T11) * 100;
	eT12 = abs((T12-T12old)/T12) * 100;
	eT21 = abs((T21-T21old)/T21) * 100;
	eT22 = abs((T22-T22old)/T22) * 100;
	iter = iter + 1;
    }
    cout << "T11 is " << T11 << " with an approx. error of " << eT11 << "%"  
	 << "\nT12 is " << T12 << " with an approx. error of " << eT12 << "%"
	 << "\nT21 is " << T21 << " with an approx. error of " << eT21 << "%"
	 << "\nT22 is " << T22 << " with an approx. error of " << eT22 << "%" 
	 << "Result found after " << iter << " iterations" << endl;
}

void RelaxGSeid(double tol, double lamda){
    double x1 = (3 + 0 + 0)/6;
    double x2 = ((6*x1) + 0 - 40)/-9;
    double x3 = (50 + (3*x1) - x2)/12;
    double ex1 = 100;
    double ex2 = 100;
    double ex3 = 100;
    double iter = 1;
    while ((ex1 > tol) || (ex2 > tol) || (ex3 > tol)) {
	double x1old = x1;
	double x2old = x2;
	double x3old = x3;
	x1 = (3 + x2old + x3old)/6;
	if (lamda != 0){
	    x1 = (lamda * x1) + ((1 - lamda)*x1old);
	}
	x2 = ((6*x1) + x3old - 40)/-9;
	if (lamda != 0){
	    x2 = (lamda * x2) + ((1 - lamda)*x2old);
	}
	x3 = (50 + (3*x1) - x2)/12;
	if (lamda != 0){
	    x3 = (lamda * x3) + ((1 - lamda)*x3old);
	}
	ex1 = abs((x1-x1old)/x1) * 100;
	ex2 = abs((x2-x2old)/x2) * 100;
	ex3 = abs((x3-x3old)/x3) * 100;
	iter = iter + 1;
    }
    cout << "x1 = " << x1 << " with an approx error of " << ex1 << "%"
	 << "\nx2 = " << x2 << " with an approx error of " << ex2 << "%"
	 << "\nx3 = " << x3 << " with an approx error of " << ex3 << "%"
	 << "\nResult found after " << iter << " iterations" << endl;
}

void TryGSeid(double tol){
    double x = 8;
    double y = (5 - x)/5;
    double z = ((4*x) + (2*y) -4)/2;
    double ex = 100;
    double ey = 100;
    double ez = 100;
    int iter = 1;
    while (((ex>tol) || (ey > tol) || (ez > tol)) && (iter < 100)){
	double xold = x;
	double yold = y;
	double zold = z;
	x = 8 - yold - (6*zold);
	y = (5 + zold - x)/5;
	z = ((4*x) + (2*y) - 4)/2;
	ex = abs((x-xold)/x) * 100;
	ey = abs((y-yold)/y) * 100;
	ez = abs((z-zold)/z) * 100;
	iter = iter + 1;
    }
    if (iter = 100){
	cout << "Would not Converge within 100 iterations" << endl;
	cout << "x = " << x << " and ex = " << ex << "%" 
	     << "\ny = " << y << " and ey = " << ey << "%" 
	     << "\nz = " << z << " and ez = " << ez << "%" << endl;
    }
    else {
	cout << "Converged to have an error less than " << tol << "%" << endl;
	cout << "x = " << x << " with an approx error of " << ex << "%" 
	     << "\ny = " << y << " with an approx error of " << ey << "%"
	     << "\nz = " << z << " with an approx error of " << ez << "%" 
	     << "\nResult found after " << iter << " iterations" << endl;
    }
}

void TryGSeid3(double tol){
    double x = 6/-3;
    double y = (-3 + (2*x))/2;
    double z = ((2*y)-1);
    double ex = 100;
    double ey = 100;
    double ez = 100;
    int iter = 1;
    while (((ex>tol) || (ey>tol) || (ez>tol)) && (iter < 100)){
	double xold = x;
	double yold = y;
	double zold = z;
	x = (6 - (4*yold) - (5*zold))/-3;
	y = (-3 + (2*x) + (4*zold))/2;
	z = ((2*y)-1);
	ex = abs((x-xold)/x) * 100;
	ey = abs((y-yold)/y) * 100;
	ez = abs((z-zold)/z) * 100;
	iter = iter + 1;
    }
    if (iter = 100){
	cout << "Would not converge to within tolerance within 100 iterations" << endl;
	cout << "x = " << x << " and ex = " << ex << "%" 
	     << "\ny = " << y << " and ey = " << ey << "%" 
	     << "\nz = " << z << " and ez = " << ez << "%" << endl;

    }
    else {
	cout << "Converged to have an error less than " << tol << "%" << endl;
    }
}

int main(){
    //GSeid(0.05);
    //RelaxGSeid(5.0, 0);
    //RelaxGSeid(5.0, 0.95);
    //TryGSeid(0.05);
    TryGSeid3(0.05);
    return 0;
}
