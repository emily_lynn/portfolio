using namespace std;

#include <cmath>
#include <iostream>

void NewtRaph(double x0, double tol);

void NewtRaph(double x0, double tol){
    double xr = x0;
    double xrold;
    double ea = 100;
    while( ea > tol ){
	xrold = xr;
	double f = (-9*pow(xrold,5)) - (8*pow(xrold,3)) + 12;
	double fprime = (-45*pow(xrold,4)) - (24*pow(xrold,2));
	xr = xrold - (f/fprime);
	if (xr != 0){
	    ea = abs( (xr-xrold)/xr ) * 100;
	}
    }
    double fMax = (-1.5*pow(xr,6)) - (2*pow(xr,4)) + (12*xr);
    cout << "The root (and therefore the maximum) is at x = " << xr
	 << "\n With a maximum function value of " << fMax
	 << "\n ea is " << ea << "%" << endl;
}
int main(){
    NewtRaph(0.5, 0.05);
    return(0);
}

