using namespace std;
#include <cmath>
#include <iostream>

void GoldenSearch(double xlow, double xhigh, double es);

double Function(double x);

double Function(double x){
    double funcVal = (-1.5*pow(x,6)) - (2*pow(x,4)) + (12*x);
    return(funcVal);
}

void GoldenSearch(double xlow, double xhigh, double es){
    double R = (pow(5,0.5) - 1)/2;
    double d = (xhigh - xlow)*R;
    double x1 = xlow + d;
    double x2 = xhigh - d;
    double iter = 0;
    double f1 = Function(x1);
    double f2 = Function(x2);
    double xopt = 0;
    double ea = 100;
    double fMax = 0;
    while (iter < 3){
	d = R*d;
    	if (f1 > f2){
		xopt = x1;
		fMax = f1;
		xlow = x2;
		x2 = x1;
		x1 = xlow + d;
		f2 = f1;
		f1 = Function(x1);
    	}
    	else if (f1 < f2){
		xopt = x2;
		fMax = f2;
		xhigh = x1;
		x1 = x2;
		x2 = xhigh - d;
		f1 = f2;
		f2 = Function(x2);
    	}
	else {
	    cout << "Something is wrong" << endl;
	}
   	iter = iter + 1;
	if (xopt != 0){
	    ea = (1-R)*abs((xhigh - xlow)/xopt)*100;
	}
    }
    cout << "The estimated maximum is y =  " << fMax
	 << "\nFound at x value x = " << xopt
	 << "\nwith an estimated relative error of : " << ea << "%" 
	 << "\nnumber of iterations : " << iter << endl;

}

int main(){
    GoldenSearch(0, 2, 0.05);
    return(0);
}
