using namespace std;

#include <cmath>
#include <iostream>

void Newton(double x0);

void Newton(double x0){
    double xr = x0;
    double xrold;
    double ea = 100;
    double iter = 0;
    while( iter < 3 ){
	xrold = xr;
	double fprime = (-9*pow(xrold,5)) - (8*pow(xrold,3)) + 12;
	double fDoublePrime = (-45*pow(xrold,4)) - (24*pow(xrold,2));
	xr = xrold - (fprime/fDoublePrime);
	if (xr != 0){
	    ea = abs( (xr-xrold)/xr ) * 100;
	}
	iter = iter + 1;
    }
    double fMax = (-1.5*pow(xr,6)) - (2*pow(xr,4)) + (12*xr);
    cout << "The root (and therefore the maximum) is at x = " << xr
	 << "\n With a maximum function value of " << fMax
	 << "\n ea is " << ea << "%" << endl;
}
int main(){
    Newton(2.0);
    return(0);
}
