using namespace std;

#include <cmath>
#include <iostream>
#include <cstdlib>

double Function(double x, double y);

double Function2(double x, double y);

void Random(double x, double y, int n);

double Function(double x, double y){
    double fxy = (3.5*x) + (2*y) + (x*x) - pow(x,4) - (2*x*y) - (y*y);
    return(fxy);
}

double Function2(double x, double y){
    double fxy = (2.25*x*y) + (1.75*y) - (1.5*x*x) - (2*y*y);
    return(fxy);
}

void Random(int n){
    srand(static_cast<unsigned>(time(0)));
    double x = 0;
    double y = 0;
    double fMax = -100000000;
    int iter = 0;
    double xMax = 0;
    double yMax = 0;
    while (iter < n){
	x = (1.0*(static_cast<float>(rand())/static_cast<float>(RAND_MAX)));
	y = (1.0*(static_cast<float>(rand())/static_cast<float>(RAND_MAX)));
	double fn = Function2(x, y);
	if (fn > fMax){
	    fMax = fn;
	    xMax = x;
	    yMax = y;
	}
	iter = iter + 1;
    }
    cout << "The maximum of the function is f(x,y) = " << fMax
	 << "\nWith xMax = " << xMax << " and yMax = " <<yMax
	 << "\nFound after " << n << " random guesses" << endl;
}

int main(){
    Random(10);
    return(0);
}
