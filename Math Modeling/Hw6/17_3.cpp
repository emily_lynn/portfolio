using namespace std;

#include <cmath>
#include <iostream>
#include <math.h>

void LinearReg(double x[], double y[], int n);

void LinearReg(double x[], double y[], int n){
    double sumx = 0;
    double sumxy = 0;
    double st = 0;
    double sumy = 0;
    double sumx2 = 0;
    double sr = 0;
    for (int i=0; i<n; i++){ //Because computers start counting at 0
	sumx = sumx + x[i];
	sumy = sumy + y[i];
	sumxy = sumxy + (x[i]*y[i]);
	sumx2 = sumx2 + (x[i]*x[i]);
    }
    double xm = sumx/n;
    double ym = sumy/n;
    double a1 = ((n*sumxy) - (sumx*sumy))/((n*sumx2) - (sumx*sumx));
    double a0 = ym - (a1*xm);
    for (int i=0; i<n; i++){
	st = st + pow((y[i] - ym), 2);
	//cout << (y[i]-ym) << endl;
	sr = sr + pow((y[i] - (a1*x[i]) -a0), 2);
    }
    double syx = pow((sr/(n-2)), 0.5);
    double r2 = (st - sr)/st;
    double r = pow(r2, 0.5);
    cout << "a1 = " << a1 
	 << "\na0 = " << a0
	 << "\nsyx = " << syx
	 << "\nr = " << r << endl;
}

int main(){
    double xarray[] = {log10(5.0),log10(10.0),log10(15.0),log10(20.0),log10(25.0),log10(30.0),log10(35.0),log10(40.0),log10(45.0),log10(50.0)};
    double yarray[] =  {log10(17.0),log10(24.0),log10(31.0),log10(33.0),log10(37.0),log10(37.0),log10(40.0),log10(40.0),log10(42.0),log10(41.0)};
    LinearReg(xarray, yarray, 10); 
    return(0);
}
