This class was taken Fall 2018.
All of the code in these Math Modeling Projects is my own.

MAT 310 3 hours
Mathematical Modeling with Numerical Analysis
An introduction to modeling and the methods, techniques, and pitfalls in scientific
computing and numerical analysis. The course will emphasize projects, writing,
technology, and applications. Topics include iterative and algorithmic processes, error
analysis, numerical integration and differentiation, curve fitting, and numerical solutions
to different equations. Required for mathematics majors with a concentration in computer
science and for computer science majors with a concentration in scientific computing. 
