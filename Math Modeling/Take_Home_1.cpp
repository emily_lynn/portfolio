
using namespace std;
#include <cmath>
#include <iostream>

void Recursive(float x0, float tol);

void Recursive2(double x0, double tol);

void Recursive(float x0, float tol){
    float x = x0;
    float iter = 1;
    float ea = 100;
    float xold;
    while ( (ea > tol) && (iter < 100) ) {
	xold = x;
	x = pow(2.0, iter)*(sqrt(1 + (pow(2.0, (1-iter))*xold)) - 1);
	iter = iter + 1;
	if (x != 0){
            ea = (abs(x-xold)/x) * 100;
	}
    }
    cout << "With x0 = " 
	 << x0
	 << " \nthe recursive function has reached a limit of : " 
	 << x
	 << " with an approx. relative error of : "
	 << ea 
	 << "%" 
	 << "\n iterations: " << iter 
	 << "\n" << endl;
}


void Recursive2(double x0, double tol){
    double x = x0;
    double iter = 1;
    double ea = 100;
    double xold;
    while ( (ea > tol) && (iter < 100) ) {
	xold = x;
	x = (pow(2,iter/2.0)*sqrt(pow(2,iter)+(2*xold))) - pow(2,iter);
	iter = iter + 1;
	if (x != 0){
            ea = (abs(x-xold)/x) * 100;
	}
    }
    cout << "With x0 = " 
	 << x0
	 << " \nthe recursive function has reached a limit of : " 
	 << x
	 << " with an approx. relative error of : "
	 << ea 
	 << "%" 
	 << "\n iterations: " << iter 
	 << "\n" << endl;
}


int main() {
    Recursive(1, .00001);
    Recursive(5, .00001);
    Recursive(10, .00001);
    Recursive(100, .00001);
    Recursive(0, .00001);
    return(0);
}
