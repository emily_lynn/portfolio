# Portfolio

These are my projects for my Computer Science classes from my time as an undergraduate student at Taylor University.
Not all of the code is mine. Professors often gave supporting frameworks (.h files,
supporting visualization programs, function stubs, etc.) from 
which we worked to implement the project requirements. 

(Not included are projects from my Introduction to Computational Problem Solving class)